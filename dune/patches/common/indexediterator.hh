#ifndef DUNE_PATCHES_COMMON_INDEXEDITERATOR_HH
#define DUNE_PATCHES_COMMON_INDEXEDITERATOR_HH

#include <type_traits>
#include <utility>

#include <dune/common/iteratorfacades.hh>

namespace Dune {
  namespace Patches {

    namespace IndexedIteratorImpl {
      template<class Item>
      struct Index : private Item
      {
        auto iteratorIndex() const -> decltype(this->Item::iteratorIndex());
      };
      template<class Item>
      using IndexT = typename std::decay<
        decltype(std::declval<const Index<Item> >().iteratorIndex())>::type;
    }

    //! Generic iterator to iterate over indexed items
    /*
     * This is a generic iterator to iterate over items with an index.
     * Specifically, the item must have a public or protected member index_.
     * Advancing the iterator is done by advancing index_, and equality is
     * determined by equality of index_.  The iterator derives privately from
     * item to gain access to index_.  The type of the index_ should be signed
     * so it can be used as the difference_type.
     *
     * We need to be able to assign the index to one passed the last valid
     * index; this serves to implement the end iterator.
     */
    template<class Item>
    class IndexedIterator :
      private Item,
      public RandomAccessIteratorFacade<IndexedIterator<Item>,
                            const Item, const Item,
                            IndexedIteratorImpl::IndexT<Item> >
    {
      typedef IndexedIteratorImpl::IndexT<Item> Index;
      // does not work because bool operator!=(IteratorFacade, IteratorFacade)
      // is a free function, and there are many more
      // friend RandomAccessIteratorFacade;

    public:
      IndexedIterator() = default;
      template<class... Args>
      IndexedIterator(Args &&... args) :
        Item(std::forward<Args>(args)...)
      { }

      const Item &dereference() const { return *this; }
      const Item elementAt(Index n) const
      {
        return *(*this + n);
      }
      bool equals(const IndexedIterator &other) const
      {
        return this->iteratorIndex() == other.iteratorIndex();
      }
      void increment() { ++this->iteratorIndex(); }
      void decrement() { --this->iteratorIndex(); }
      void advance(Index n) { this->iteratorIndex() += n; }
      Index distanceTo(const IndexedIterator &other) const
      {
        return other.iteratorIndex() - this->iteratorIndex();
      }
    };

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_COMMON_INDEXEDITERATOR_HH
