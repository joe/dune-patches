#ifndef DUNE_PATCHES_COMMON_UTILITY_HH
#define DUNE_PATCHES_COMMON_UTILITY_HH

#include <algorithm>
#include <cassert>
#include <map>
#include <utility>
#include <vector>

#include <dune/patches/utility/range.hh>

/**
 * @file Common utilities used for the implementation of patches
 */

namespace Dune {
  namespace Patches {

    //! Comparison functor for std::map / std::set
    /**
     * This is a comparision functor for std::map / std::set where the keys
     * are standard containers (like std::vector<int>).  It uses
     * std::lexicographical_compare to compare the keys.
     */
    struct lexicographic_less {
      template<class T>
      bool operator()(const T &first, const T & second)
      {
        return std::lexicographical_compare(first.begin(), first.end(),
                                            second.begin(), second.end());
      }
    };

    //! compute intersections given a connectivity.
    /**
     * \param[in] refelem         Reference-element for the codim-0 entities
     *                            in the mesh.
     * \param[in] elementVertices Connectivity for the codim-0 entities in
     *                            the mesh.  Each element occupies
     *                            refelem.size(0) consecutive entries.  Each
     *                            entry denotes a vertex number.
     *
     * The remaining parameters are used to store the computed intersection
     * information.  There are seperate arrays for the inside and the outside
     * of interior intersections and for the inside of boundary intersections.
     */
    template<class ReferenceElement, class Index>
    void computeIntersections(const ReferenceElement &refelem,
                              const std::vector<Index> &elementVertices,
                              std::vector<Index> &iInsideElements,
                              std::vector<Index> &iIndexInInside,
                              std::vector<Index> &iOutsideElements,
                              std::vector<Index> &iIndexInOutside,
                              std::vector<Index> &bInsideElements,
                              std::vector<Index> &bIndexInInside)
    {
      iInsideElements.clear();
      iIndexInInside.clear();
      iOutsideElements.clear();
      iIndexInOutside.clear();
      bInsideElements.clear();
      bIndexInInside.clear();

      const auto &gt = refelem.type();
      auto dim = gt.dim();
      auto verticesPerElement = refelem.size(dim);
      assert(elementVertices.size() % verticesPerElement == 0);
      Index elements = elementVertices.size() / verticesPerElement;
      unsigned sides = refelem.size(1);

      typedef std::vector<Index> IntersectionVertices;
      typedef std::pair<Index, Index> IntersectionSide;
      typedef std::map<IntersectionVertices, IntersectionSide,
                       lexicographic_less> IMap;
      IMap intersections;
      assert(gt.isSimplex() || gt.isCube());
      // assume all codim1-subentities have the same number of vertices
      // true for simplices and hypercubes
      IntersectionVertices vertices(refelem.size(0, 1, dim));
      for(auto e : range(elements))
      {
        Index base = e * verticesPerElement;
        for(auto side : range(sides)) {
          for(auto v : range(vertices.size()))
            vertices[v] = elementVertices[base +
                                          refelem.subEntity(side, 1, v, dim)];
          std::sort(vertices.begin(), vertices.end());
          auto val = std::make_pair(vertices, std::make_pair(e, side));
          auto result = intersections.insert(val);
          if(!result.second)
          {
            // key already in container, have both sides of intersection now
            iInsideElements.push_back(result.first->second.first);
            iIndexInInside.push_back(result.first->second.second);
            iOutsideElements.push_back(val.second.first);
            iIndexInOutside.push_back(val.second.second);
            intersections.erase(result.first);
          }
        }
      }

      // remaining intersections are boundary
      bInsideElements.reserve(intersections.size());
      bIndexInInside.reserve(intersections.size());
      for(typename IMap::iterator it = intersections.begin();
          it != intersections.end(); ++it)
      {
        bInsideElements.push_back(it->second.first);
        bIndexInInside.push_back(it->second.second);
      }
    }

    //! Compute entities of one particular codim given an element connectivity
    template<class Refelem, class Index>
    void computeCodimEntities(const Refelem &refelem,
                              const std::vector<Index> &elemVertexConn,
                              unsigned codim,
                              std::vector<Index> &entityVertexConn,
                              std::vector<Index> &elemEntityConn)
    {
      auto dim = refelem.type().dim();
      auto elemVertices = refelem.size(dim);
      auto elements = elemVertexConn.size() / elemVertices;
      auto entityVertices = refelem.size(0, codim, dim);
      auto elemEntities = refelem.size(codim);

      entityVertexConn.clear();
      // reserve some lower limit; we don't know the exact number
      entityVertexConn.reserve(entityVertices * elements);
      elemEntityConn.clear();
      elemEntityConn.reserve(elemEntities * elements);
      std::map<std::vector<Index>, Index, lexicographic_less> seen;
      std::vector<Index> vertices(entityVertices);
      std::pair<std::vector<Index>, Index> entry;

      assert(elemVertexConn.size() % elemVertices == 0);
      for(auto it = elemVertexConn.begin(); it != elemVertexConn.end();
          it += elemVertices)
        for(auto i : range(refelem.size(codim)))
        {
          for(auto v : range(entityVertices))
            vertices[v] = it[refelem.subEntity(i, codim, v, dim)];
          entry.first = vertices;
          std::sort(entry.first.begin(), entry.first.end());
          entry.second = entityVertexConn.size() / entityVertices;
          auto result = seen.insert(entry);
          if(result.second)
          {
            // entity not seen before
            entityVertexConn.insert(entityVertexConn.end(),
                                    vertices.begin(), vertices.end());
            elemEntityConn.push_back(entry.second);
            // TODO: store unity twist here
          }
          else
          {
            // entity already seen
            elemEntityConn.push_back(result.first->second);
            // TODO: compute and store twist here.
          }
        }
      assert(elemEntityConn.size() == elemEntities * elements);
    }

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_COMMON_UTILITY_HH
