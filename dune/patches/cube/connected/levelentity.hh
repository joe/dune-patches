#ifndef DUNE_PATCHES_CUBE_CONNECTED_LEVELENTITY_HH
#define DUNE_PATCHES_CUBE_CONNECTED_LEVELENTITY_HH

#include <array>
#include <cassert>
#include <type_traits>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/axisalignedcubegeometry.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include <dune/patches/cube/geometry.hh>
#include <dune/patches/utility/cmp.hh>
#include <dune/patches/utility/math.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {
      namespace Connected {

        template<class Patch, unsigned cd, class = void>
        class LevelEntity;

        template<class Patch>
        class LevelEntity<Patch, 0>
        {
          const Patch *patch_;
          typedef typename Patch::Index Index;
          Index index_;
          unsigned level_;

        protected:
          Index &iteratorIndex()       { return index_; }
          Index  iteratorIndex() const { return index_; }

        public:
          enum { codimension = 0 };
          enum { dimension = Patch::dimension };
          enum { mydimension = dimension - codimension };

          LevelEntity() = default;
          LevelEntity(const Patch *patch, unsigned level, Index index) :
            patch_(patch), index_(index), level_(level)
          { }

          using Geometry =
            Cube::Geometry<typename Patch::ctype, mydimension, dimension>;
          Geometry geometry() const
          {
            typedef typename Patch::ctype ct;
            static const auto &refelem =
              ReferenceElements<ct, dimension>::cube();

            auto n = 1u << level_;
            auto subentities = ipow(Index(n), Index(dimension));
            auto macroIndex = index_ / subentities;
            auto subIndex = index_ - macroIndex;

            constexpr auto vertices = 1 << dimension;

            std::array<FieldVector<ct, dimension>, vertices> corners;
            for(auto v : range(vertices))
              corners[v] =
                patch_->vertex(patch_->entityVertex(0, macroIndex, v));
            return { corners, n, subIndex };
          }

          static const GeometryType &type()
          {
            return Patch::types(codimension)[0];
          }

          unsigned subEntities(unsigned codim) const
          {
            static const auto &refelem =
              ReferenceElements<typename Patch::ctype,dimension>::cube();
            return refelem.size(codim);
          }
          template<unsigned codim>
          typename std::enable_if<codim==dimension,
                                  LevelEntity<Patch, codim> >::type
          subEntity(Index n) const
          {
            assert(level_ == 0);
            assert(n < subEntities(codim));
            return { patch_, 0,
                patch_->entityVertices_[0][(1<<dimension)*index_+n] };
          }
          template<unsigned codim>
          typename std::enable_if<codim==0, const LevelEntity &>::type
          subEntity(Index n) const
          {
            assert(n == 0);
            return *this;
          }

        private:
          Index subVertex(unsigned v) const
          {
            auto n = 1 << level_;
            auto subelements = ipow(Index(n), Index(dimension));
            auto localIndex = index_ % subelements;
            auto macroElemIndex = index_ / subelements;
            unsigned hostDim = 0;
            FieldVector<typename Patch::ctype, dimension> hostPos;
            decltype(n) indexInHost = 0, baseInHost = 1;
            for(auto d : range(dimension))
            {
              auto epos = localIndex % n;
              localIndex /= n;
              auto offset = (v >> d) & 1;
              auto vpos = epos + offset;

              if     (eq(vpos, 0)) { hostPos[d] = 0;   }
              else if(eq(vpos, n)) { hostPos[d] = 1;   }
              else                 { hostPos[d] = 0.5;
                                     indexInHost += baseInHost * (vpos-1);
                                     baseInHost *= n-1;
                                     ++hostDim;        }
            }
            auto hostCodim = dimension - hostDim;

            static const auto &refelem =
              ReferenceElements<typename Patch::ctype, dimension>::cube();
            // The number of the host entity within the macro element
            decltype(refelem.size(hostCodim)) i = 0;
            for(; (refelem.position(i, hostCodim)-hostPos).two_norm() > 0.1;
                ++i)
              assert(i < refelem.size(hostCodim));
            auto hostIndex = patch_->macroGridView().
              template begin<0>()[macroElemIndex].subIndex(i, hostCodim);
            auto vertexIndex = indexInHost + hostIndex * baseInHost;
            for(unsigned dim = hostDim+1; dim <= dimension; ++dim)
            {
              baseInHost *= n-1;
              vertexIndex += patch_->entityCount(dimension-dim) * baseInHost;
            }
            return vertexIndex;
          }

        public:
          Index index() const { return index_; }
          Index subIndex(unsigned i, unsigned codim) const
          {
            switch(codim) {
            case 0:
              assert(i == 0);
              return index();
            case dimension:
              return subVertex(i);
            default:
              DUNE_THROW(NotImplemented,
                         "subindices for codimension " << codim);
            }
          }
        };

        template<class Patch, unsigned codim>
        class LevelEntity<Patch, codim,
                          typename std::enable_if<Patch::dimension == codim>
                          ::type>
        {
          const Patch *patch_;
          typedef typename Patch::Index Index;
          Index index_;

        protected:
          Index &iteratorIndex()       { return index_; }
          Index  iteratorIndex() const { return index_; }

          friend LevelEntity<Patch, 0>;
          LevelEntity(const Patch *patch, unsigned level, Index index) :
            patch_(patch), index_(index)
          { assert(level==0); }

        public:
          enum { codimension = codim };
          enum { dimension = Patch::dimension };
          enum { mydimension = 0 };

          LevelEntity() = default;

          using Geometry =
            Dune::AxisAlignedCubeGeometry<typename Patch::ctype,
                                          mydimension, dimension>;
          Geometry geometry() const
          {
            return { patch_->vertices_[index_] };
          }
          static const GeometryType &type()
          {
            return Patch::LevelGridView::IndexSet::types(codimension)[0];
          }

          Index index() const { return index_; }
        };

      } // namespace Connected
    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_CONNECTED_LEVELENTITY_HH
