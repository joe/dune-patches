#ifndef DUNE_PATCHES_CUBE_CONNECTED_PATCH_HH
#define DUNE_PATCHES_CUBE_CONNECTED_PATCH_HH

#include <algorithm>
#include <array>
#include <iterator>
#include <limits>
#include <memory>
#include <type_traits>
#include <utility>
#include <vector>

#include <dune/common/fvector.hh>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include <dune/patches/common/utility.hh>
#include <dune/patches/cube/connected/levelentity.hh>
#include <dune/patches/cube/connected/levelview.hh>
#include <dune/patches/cube/indexset.hh>
#include <dune/patches/cube/macroentity.hh>
#include <dune/patches/cube/macroview.hh>
#include <dune/patches/utility/icccompat.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {
      namespace Connected {

        template<class ct, unsigned d, std::size_t align = sizeof(double)*8,
                 std::size_t internalAlign = sizeof(double)*32>
        class Patch {
        public:
          enum { dimension = d };
          enum { dimensionworld = d };
          typedef ct ctype;
          typedef unsigned Index;
          static_assert(std::is_trivial<ctype>::value,
                        "Coordinate type must be trivial");
          static_assert(std::is_trivial<Index>::value,
                        "Index type must be trivial");

          //! placement-new operator
          /**
           * This enables new-expressions of the form
           * \code
           *   new(extraSize) Patch(constructor_args...)
           * \endcode
           * where \c extraSize specifies the additional amount of memory to
           * allocate (not including \c sizeof(Patch) for the header).
           */
          void *operator new(std::size_t size, std::size_t extraSize)
          {
            return ::operator new(size + extraSize);
          }
          //! placement-delete operator
          /**
           * This is used by a placement-new-expression to free already
           * allocated memory in case of exception.
           */
          void operator delete(void *p, std::size_t)
          {
            ::operator delete(p);
          }
          //! regular delete operator
          /**
           * This is needed because the placement-delete operator hides the
           * global operator delete, and we still want the standard deleter to
           * work.
           */
          void operator delete(void *p)
          {
            ::operator delete(p);
          }

          //! disable non-placement new
          void* operator new(std::size_t) = delete;
          //! disable array new
          void* operator new[](std::size_t) = delete;
          //! disable array delete
          void operator delete[](void*) = delete;
        private:
          // entitycounts in the coarse mesh (indexed by codimension of the
          // entity)
          Index sizes_[d+1];
          Index iIntersectionSize_;
          Index bIntersectionSize_;

          Index entityCount(unsigned codim) const { return sizes_[codim]; }

          std::size_t allocatedSize_;
          const char *storage() const
          { return reinterpret_cast<const char*>(this); }
          char *storage() { return reinterpret_cast<char*>(this); }
        public:
          std::size_t allocatedSize() const { return allocatedSize_; }

        private:
          /////////////////////////////////////////////////////////////////////
          //
          // members of type ctype
          //

          // the vertex coordinates
          ptrdiff_t offsetVertices_;
          ptrdiff_t strideVertices_;
          const ctype *vertices(std::size_t i) const
          { return reinterpret_cast<const ctype *>
              (storage()+offsetVertices_ + i * strideVertices_); }
          ctype *vertices(std::size_t i)
          { return reinterpret_cast<ctype *>
              (storage()+offsetVertices_ + i * strideVertices_); }
        iccmicprivate:
          FieldVector<ctype, dimensionworld> vertex(Index i) const
          {
            FieldVector<ctype, dimensionworld> v;
            for(auto dim : range(dimensionworld))
              v[dim] = vertices(dim)[i];
            return v;
          }

        private:
          /////////////////////////////////////////////////////////////////////
          //
          // members of type Index
          //

          // connectivity[0..d-1] contains the indices of mesh entities of
          // codim 1..d corresponding the the elements' subentities.  (For
          // codim 0 this array would be the identity, so we don't store it.)
          //
          // connectivity[d-1..2*d-2] contains the vertex indices of the
          // entities of codim 0..d-1.  (Note the overlap in
          // connectivity[d-1].)
          ptrdiff_t offsetConnectivity_[2*d-1];
          ptrdiff_t strideConnectivity_[2*d-1];
          const Index *
          elementEntities(std::size_t codim, std::size_t comp) const
          { return reinterpret_cast<const Index *>
              (storage() + offsetConnectivity_[codim-1] +
               comp * strideConnectivity_[codim-1]); }
          Index *elementEntities(std::size_t codim, std::size_t comp)
          { return reinterpret_cast<Index *>
              (storage() + offsetConnectivity_[codim-1] +
               comp * strideConnectivity_[codim-1]); }
        iccmicprivate:
          Index elementSubEntity(Index elem, std::size_t codim,
                                 std::size_t s) const
          { return elementEntities(codim, s)[elem]; }

        private:
          const Index *
          entityVertices(std::size_t codim, std::size_t comp) const
          { return reinterpret_cast<const Index *>
              (storage() + offsetConnectivity_[d-1+codim] +
               comp * strideConnectivity_[d-1+codim]);}
          Index *entityVertices(std::size_t codim, std::size_t comp)
          { return reinterpret_cast<Index *>
              (storage() + offsetConnectivity_[d-1+codim] +
               comp * strideConnectivity_[d-1+codim]);}
        iccmicprivate:
          Index entityVertex(std::size_t codim, Index entity,
                             std::size_t v) const
          { return entityVertices(codim, v)[entity]; }

        private:
          // intersections
          std::ptrdiff_t offsetIInsideElements_;
          const Index *iInsideElements() const
          { return reinterpret_cast<const Index *>
              (storage()+offsetIInsideElements_); }
          Index *iInsideElements()
          { return reinterpret_cast<Index *>
              (storage()+offsetIInsideElements_); }

          std::ptrdiff_t offsetIIndexInInside_;
          const Index *iIndexInInside() const
          { return reinterpret_cast<const Index *>
              (storage()+offsetIIndexInInside_); }
          Index *iIndexInInside()
          { return reinterpret_cast<Index *>
              (storage()+offsetIIndexInInside_); }

          std::ptrdiff_t offsetIOutsideElements_;
          const Index *iOutsideElements() const
          { return reinterpret_cast<const Index *>
              (storage()+offsetIOutsideElements_); }
          Index *iOutsideElements()
          { return reinterpret_cast<Index *>
              (storage()+offsetIOutsideElements_); }

          std::ptrdiff_t offsetIIndexInOutside_;
          const Index *iIndexInOutside() const
          { return reinterpret_cast<const Index *>
              (storage()+offsetIIndexInOutside_); }
          Index *iIndexInOutside()
          { return reinterpret_cast<Index *>
              (storage()+offsetIIndexInOutside_); }

          std::ptrdiff_t offsetBInsideElements_;
          const Index *bInsideElements() const
          { return reinterpret_cast<const Index *>
              (storage()+offsetBInsideElements_); }
          Index *bInsideElements()
          { return reinterpret_cast<Index *>
              (storage()+offsetBInsideElements_); }

          std::ptrdiff_t offsetBIndexInInside_;
          const Index *bIndexInInside() const
          { return reinterpret_cast<const Index *>
              (storage()+offsetBIndexInInside_); }
          Index *bIndexInInside()
          { return reinterpret_cast<Index *>
              (storage()+offsetBIndexInInside_); }

        private:
          // patch creation
          Patch() = default;

          // make copying private.  may be deleted in the long run.
          Patch(const Patch &) = default;
          Patch(Patch &&) = delete;

          template<class Container>
          Patch(const Container &entityCounts, Index bIntersectionCount)
          {
            using std::begin;
            static const auto &refelem = ReferenceElements<ct,d>::cube();

            // copy sizes
            std::copy_n(begin(entityCounts), dimension+1, sizes_);
            iIntersectionSize_ = sizes_[1] - bIntersectionCount;
            bIntersectionSize_ = bIntersectionCount;

            // compute offsets and strides
            allocatedSize_ = pad(sizeof(Patch));

            // vertex coordinates
            offsetVertices_ = allocatedSize_;
            strideVertices_ = pad(sizeof(ctype)*sizes_[d]);
            allocatedSize_ += dimensionworld * strideVertices_;

            // subentities of elements of codim c
            for(unsigned c = 1; c < d; ++c)
            {
              offsetConnectivity_[c-1] = allocatedSize_;
              strideConnectivity_[c-1] = pad(sizeof(Index)*sizes_[0]);
              allocatedSize_ += refelem.size(c) * strideConnectivity_[c-1];
            }

            // subentities of elements of codim d
            // a.k.a. vertices of elements
            // a.k.a. vertices of entities of codim 0
            {
              offsetConnectivity_[d-1] = allocatedSize_;
              strideConnectivity_[d-1] = pad(sizeof(Index)*sizes_[0]);
              allocatedSize_ += refelem.size(d) * strideConnectivity_[d-1];
            }

            // vertices of entities of codim c
            for(unsigned c = 1; c < d; ++c)
            {
              offsetConnectivity_[d-1+c] = allocatedSize_;
              strideConnectivity_[d-1+c] = pad(sizeof(Index)*sizes_[c]);
              allocatedSize_ +=
                refelem.size(0, c, d) * strideConnectivity_[d-1+c];
            }

            // intersections
            offsetIInsideElements_ = allocatedSize_;
            allocatedSize_ += pad(sizeof(Index) * iIntersectionSize_);

            offsetIIndexInInside_ = allocatedSize_;
            allocatedSize_ += pad(sizeof(Index) * iIntersectionSize_);

            offsetIOutsideElements_ = allocatedSize_;
            allocatedSize_ += pad(sizeof(Index) * iIntersectionSize_);

            offsetIIndexInOutside_ = allocatedSize_;
            allocatedSize_ += pad(sizeof(Index) * iIntersectionSize_);

            offsetBInsideElements_ = allocatedSize_;
            allocatedSize_ += pad(sizeof(Index) * bIntersectionSize_);

            offsetBIndexInInside_ = allocatedSize_;
            allocatedSize_ += pad(sizeof(Index) * bIntersectionSize_);

            // allocatedSize_ now contains the space needed to store all arrays
            // + header
          }

          static constexpr std::size_t pad(std::size_t size)
          {
            return (size / internalAlign + bool(size % internalAlign))
              * internalAlign;
          }

        public:
          template<class Container>
          static std::unique_ptr<Patch>
          create(const Container &entityCounts, Index bIntersectionCount)
          {
            using std::begin;
            static const auto &refelem = ReferenceElements<ct,d>::cube();

            std::size_t extraSize = pad(sizeof(Patch)) - sizeof(Patch);

            auto entityCountIt = begin(entityCounts); // at codim=0

            // subentities of elements of codim c (including c=d)
            unsigned elementComponentCount = 0;
            for(unsigned c = 1; c <= d; ++c)
              elementComponentCount += refelem.size(c);
            extraSize +=
              elementComponentCount * pad(sizeof(Index) * *entityCountIt);

            ++entityCountIt; // now at codim=1

            // intersections
            // |{Inside,Outside}x{Elements,IndexIn}| == 4
            extraSize += 4 * pad(sizeof(Index)*(*entityCountIt
                                                - bIntersectionCount));
            // |{Inside}x{Elements,IndexIn}| == 2
            extraSize += 2 * pad(sizeof(Index)*bIntersectionCount);

            // vertices of entities of codim c
            for(unsigned c = 1; c < d; ++c, ++entityCountIt)
            {
              // entityCountIt now at codim=c
              extraSize +=
                refelem.size(0, c, d) * pad(sizeof(Index) * *entityCountIt);
            }
            // entityCountIt now at codim=d

            // vertex coordinates
            extraSize += dimensionworld * pad(sizeof(ctype) * *entityCountIt);

            return std::unique_ptr<Patch>
              (new(extraSize) Patch(entityCounts, bIntersectionCount));
          }

          template<class Coord, class I>
          static std::unique_ptr<Patch>
          create(const std::vector<Coord> &vertices,
                 const std::vector<I> &elementVertices)
          {
            using std::begin;
            using std::end;

            static const auto &refelem = ReferenceElements<ct,d>::cube();

            // intersections
            std::vector<Index> iInsideElements;
            std::vector<Index> iIndexInInside;
            std::vector<Index> iOutsideElements;
            std::vector<Index> iIndexInOutside;
            std::vector<Index> bInsideElements;
            std::vector<Index> bIndexInInside;
            computeIntersections(refelem, elementVertices,
                                 iInsideElements, iIndexInInside,
                                 iOutsideElements, iIndexInOutside,
                                 bInsideElements, bIndexInInside);

            // entityVertices[0] is not used
            std::vector<Index> entityVertices[d];
            std::vector<Index> elementEntities[d];

            Index sizes[dimension+1];
            sizes[0] = elementVertices.size() / refelem.size(0, 0, dimension);
            for(unsigned c  = 1; c < dimension; ++c)
            {
              computeCodimEntities(refelem, elementVertices, c,
                                   entityVertices[c], elementEntities[c]);
              sizes[c] =
                entityVertices[c].size() / refelem.size(0, c, dimension);
            }
            sizes[dimension] = vertices.size();

            // there should be exactly as many intersections as codim-1
            // entities
            assert(sizes[1] ==
                   iInsideElements.size() + bInsideElements.size());

            // allocate patch and copy everything over
            auto p = create(sizes, bInsideElements.size());

            // coordinates
            for(auto i : range(sizes[dimension]))
              for(auto dim : range(dimensionworld))
                p->vertices(dim)[i] = vertices[i][dim];

            // subentities of elements of codim c
            for(unsigned c = 1; c < d; ++c)
            {
              unsigned components = refelem.size(c);
              for(auto elem : range(sizes[0]))
                for(auto comp : range(components))
                  p->elementEntities(c, comp)[elem] =
                    elementEntities[c][components * elem + comp];
            }

            // subentities of elements of codim d
            // a.k.a. vertices of elements
            // a.k.a. vertices of entities of codim 0
            {
              unsigned components = refelem.size(d);
              for(auto elem : range(sizes[0]))
                for(auto comp : range(components))
                  p->elementEntities(d, comp)[elem] =
                    elementVertices[components * elem + comp];
            }

            // vertices of entities of codim c
            for(unsigned c = 1; c < d; ++c)
            {
              unsigned components = refelem.size(0, c, d);
              for(auto entity : range(sizes[c]))
                for(auto comp : range(components))
                  p->entityVertices(c, comp)[entity] =
                    entityVertices[c][components * entity + comp];
            }

            std::copy(begin(iInsideElements), end(iInsideElements),
                      p->iInsideElements());

            std::copy(begin(iIndexInInside), end(iIndexInInside),
                      p->iIndexInInside());

            std::copy(begin(iOutsideElements), end(iOutsideElements),
                      p->iOutsideElements());

            std::copy(begin(iIndexInOutside), end(iIndexInOutside),
                      p->iIndexInOutside());

            std::copy(begin(bInsideElements), end(bInsideElements),
                      p->bInsideElements());

            std::copy(begin(bIndexInInside), end(bIndexInInside),
                      p->bIndexInInside());

            return p;
          }

          // general use
          typedef Cube::IndexSet<Patch> IndexSet;
          friend IndexSet;
          IndexSet indexSet() const { return {}; }

          typedef LevelView<Patch> LevelGridView;
          friend LevelGridView;
          template<class, unsigned, class>
          friend class LevelEntity;
          friend typename LevelGridView::BoundaryIntersection;
          LevelGridView levelGridView(unsigned level) const
          { return { this, level }; }

          typedef MacroView<Patch> MacroGridView;
          friend MacroGridView;
          template<class, unsigned, class>
          friend class MacroEntity;
          friend typename MacroGridView::BoundaryIntersection;
          MacroGridView macroGridView() const
          { return { this }; }

        private:
          typedef std::array<GeometryType, 1> Types;
          static const Types &types(int codim) {
            static const std::array<Types, dimension+1> types_ =
              [] () -> std::array<Types, dimension+1> {
                std::array<Types, dimension+1> tmp;
                for(auto cd : range(dimension+1))
                  tmp[cd][0].makeCube(dimension-cd);
                return tmp;
            } ();
            return types_[codim];
          }
        };

      } // namespace Connected
    } // namespace Cube
  } // namespace Patches
} // namespace Dune

// This is a kludge to make mesh generators work that only get the type of
// patch as a template parameter, but use the GridFactory internally.  They
// have no idea which file to include to get the correct factory.  The user of
// the generator must have included this patch.hh file to be able to pass the
// type of the patch to it, so include the factory from here.
#include <dune/patches/cube/connected/factory.hh>

#endif // DUNE_PATCHES_CUBE_CONNECTED_PATCH_HH
