#ifndef DUNE_PATCHES_CUBE_GEOMETRY_HH
#define DUNE_PATCHES_CUBE_GEOMETRY_HH

#include <array>
#include <cassert>
#include <type_traits>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/genericgeometry/topologytypes.hh>
#include <dune/geometry/multilineargeometry.hh>
#include <dune/geometry/type.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {

      template< class ct >
      struct GeometryTraits : MultiLinearGeometryTraits<ct>
      {
        template< int mydim, int cdim >
        struct CornerStorage
        {
          typedef std::array< FieldVector< ct, cdim >, (1 << mydim) > Type;
        };

        template< int dim >
        struct hasSingleGeometryType
        {
          static const bool v = true;
          static const unsigned int topologyId =
            GenericGeometry::CubeTopology< dim >::type::id;
        };
      };

      // SubsampledMultiLinearGeometry
      // -----------------------------

      /** \brief generic geometry implementation based on corner coordinates
       *
       *  Based on the recursive definition of the reference elements, the
       *  SubsampledMultiLinearGeometry provides a generic implementation of a
       *  geometry given the corner coordinates of a macro element, a
       *  refinement specification, and a subelement number for cubes.
       *
       *  \tparam  ct      coordinate type
       *  \tparam  mydim   geometry dimension
       *  \tparam  cdim    coordinate dimension
       *  \tparam  Traits  traits allowing to tweak some implementation details
       *                   (optional)
       *
       *  The requirements on the traits are documented along with their
       *  default, MultiLinearGeometryTraits.
       *
       *  \note This has been copied and adapted from
       *        Dune::MultiLinearGeometry from dune-geometry.  Code for
       *        handling of non-cubes has been eliminated.  Code to handle
       *        subsampling will be added later.  Code for handling
       *        vectorization will be added even later.
       */
      template< class ct, int mydim, int cdim,
                class Traits = GeometryTraits< ct > >
      class SubsampledMultiLinearGeometry
      {
        static_assert(Traits::template hasSingleGeometryType<mydim>::v &&
                      Traits::template hasSingleGeometryType<mydim>::topologyId
                        == GenericGeometry::CubeTopology<mydim>::type::id,
                      "SubsampledMultiLinearGeometry works with cubes only");

      public:
        //! coordinate type
        typedef ct ctype;

        //! geometry dimension
        static const int mydimension= mydim;
        //! coordinate dimension
        static const int coorddimension = cdim;

        //! type of local coordinates
        typedef FieldVector< ctype, mydimension > LocalCoordinate;
        //! type of global coordinates
        typedef FieldVector< ctype, coorddimension > GlobalCoordinate;

        //! type of jacobian transposed
        typedef FieldMatrix< ctype, mydimension, coorddimension >
          JacobianTransposed;

        //! type of jacobian inverse transposed
        class JacobianInverseTransposed;

      protected:
        typedef typename Traits::MatrixHelper MatrixHelper;

      private:
        typedef typename Traits::
          template CornerStorage< mydimension, coorddimension >::Type::
          const_iterator CornerIterator;

      public:
        /** \brief constructor
         *
         *  \param[in]  corners  corners to store internally
         *  \param[in]  div      number of subdivisions to apply in each local
         *                       direction
         *  \param[in]  subindex Index of the subentity to return the
         *                       transformation for
         *
         *  \note The type of corners is actually a template argument.
         *        It is only required that the internal corner storage can be
         *        constructed from this object.
         */
        template< class Corners >
        SubsampledMultiLinearGeometry ( const Corners &corners,
                                        unsigned div = 1,
                                        unsigned subindex = 0) :
          corners_( corners ),
          div_( div ),
          subindex_( subindex )
        {}

        //! set number of subdivisions in each local direction
        /**
         * There are div^mydim subelements.
         */
        void div(unsigned div) { div_ = div; }
        //! get number of subdivisions in each local direction
        unsigned div() const { return div_; }
        //! set the subindex number
        /**
         * This determines the number of the subelement we provide a geometry
         * for.  The position on the local axis[0] varies fastest.
         */
        void subindex(unsigned subindex) { subindex_ = subindex; }
        //! get the subindex number
        unsigned subindex() const { return subindex_; }

        /** \brief is this mapping affine? */
        bool affine () const
        {
          JacobianTransposed jt;
          return affine( jt );
        }

        /** \brief obtain the name of the reference element */
        Dune::GeometryType type () const {
          GeometryType t;
          t.makeCube(mydimension);
          return t;
        }

        /** \brief obtain number of corners of the corresponding reference
                   element */
        int corners () const { return 1 << mydimension; }

        /** \brief obtain coordinates of the i-th corner */
        GlobalCoordinate corner ( int i ) const
        {
          using std::move;
          assert( (i >= 0) && (i < corners()) );

          // local corner position.
          LocalCoordinate localCorner;
          for(auto &c : localCorner)
          {
            c = i % 2;
            i /= 2;
          }
          return global(move(localCorner));
        }

        /** \brief obtain the centroid of the mapping's image */
        GlobalCoordinate center () const {
          return global(LocalCoordinate(0.5));
        }

        /** \brief evaluate the mapping
         *
         *  \param[in]  local  local coordinate to map
         *
         *  \returns corresponding global coordinate
         */
        GlobalCoordinate global ( const LocalCoordinate &local ) const
        {
          return macroToGlobal(localToMacro(local));
        }

        /** \brief evaluate the inverse mapping
         *
         *  \param[in]  global  global coordinate to map
         *
         *  \return corresponding local coordinate
         *
         *  \note For given global coordinate y the returned local coordinate
         *        x that minimizes the following function over the local
         *        coordinate space spanned by the reference element.
         *  \code
         *  (global( x ) - y).two_norm()
         *  \endcode
         */
        LocalCoordinate local ( const GlobalCoordinate &global ) const
        {
          const ctype tolerance = Traits::tolerance();
          LocalCoordinate x(0.5);
          LocalCoordinate dx;
          do
          {
            // Newton's method: DF^n dx^n = F^n, x^{n+1} -= dx^n
            const GlobalCoordinate dglobal = (*this).global( x ) - global;
            MatrixHelper::template xTRightInvA< mydimension, coorddimension >
              ( jacobianTransposed( x ), dglobal, dx );
            x -= dx;
          } while( dx.two_norm2() > tolerance );
          return x;
        }

        /** \brief obtain the integration element
         *
         *  If the Jacobian of the mapping is denoted by $J(x)$, the
         *  integration integration element \f$\mu(x)\f$ is given by
         *  \f[ \mu(x) = \sqrt{|\det (J^T(x) J(x))|}.\f]
         *
         *  \param[in]  local  local coordinate to evaluate the integration
         *                     element in
         *
         *  \returns the integration element \f$\mu(x)\f$.
         *
         *  \note For affine mappings, it is more efficient to call
         *        jacobianInverseTransposed before integrationElement, if both
         *        are required.
         */
        ctype integrationElement ( const LocalCoordinate &local ) const
        {
          return
            MatrixHelper::template sqrtDetAAT< mydimension, coorddimension >
              ( jacobianTransposed( local ) );
        }

        /** \brief obtain the volume of the mapping's image
         *
         *  \note The current implementation just returns
         *  \code
         *  integrationElement( LocalCoordinate(0.5) )
         *  \endcode
         *  which is wrong for n-linear surface maps and other nonlinear maps.
         */
        ctype volume () const
        {
          return integrationElement( LocalCoordinate(0.5) );
        }

        /** \brief obtain the transposed of the Jacobian
         *
         *  \param[in]  local  local coordinate to evaluate Jacobian in
         *
         *  \returns a reference to the transposed of the Jacobian
         *
         *  \note The returned reference is reused on the next call to
         *        JacobianTransposed, destroying the previous value.
         */
        JacobianTransposed
        jacobianTransposed ( const LocalCoordinate &local ) const
        {
          JacobianTransposed jt = macroJacobianTransposed(localToMacro(local));
          // the jacobian of localToMacro is I/div, apply that
          jt /= div_;
          return jt;
        }

        /** \brief obtain the transposed of the Jacobian's inverse
         *
         *  The Jacobian's inverse is defined as a pseudo-inverse. If we denote
         *  the Jacobian by \f$J(x)\f$, the following condition holds:
         *  \f[J^{-1}(x) J(x) = I.\f]
         */
        JacobianInverseTransposed
        jacobianInverseTransposed ( const LocalCoordinate &local ) const;

      protected:
        LocalCoordinate localToMacro ( LocalCoordinate local ) const
        {
          int index = subindex_;
          for(unsigned d = 0; d < mydimension; ++d)
          {
            local[d] += index % div_;
            local[d] /= div_;
            index /= div_;
          }
          return local;
        }

        GlobalCoordinate macroToGlobal ( const LocalCoordinate &macro ) const
        {
          CornerIterator cit = corners_.begin();
          GlobalCoordinate y(0);
          macroToGlobal( std::integral_constant< int, mydimension >(), cit,
                         macro, ctype( 1 ), y );
          return y;
        }

        // The jacobian of macroToGlobal at position \c macro in the macro
        // element
        JacobianTransposed
        macroJacobianTransposed ( const LocalCoordinate &macro ) const
        {
          JacobianTransposed jt(0);
          CornerIterator cit = corners_.begin();
          macroJacobianTransposed
            ( std::integral_constant< int, mydimension >(), cit, macro,
              ctype( 1 ), jt );
          return jt;
        }

        template< int dim >
        static void
        macroToGlobal ( std::integral_constant< int, dim >,
                        CornerIterator &cit, const LocalCoordinate &x,
                        const ctype &rf, GlobalCoordinate &y );
        static void
        macroToGlobal ( std::integral_constant< int, 0 >, CornerIterator &cit,
                        const LocalCoordinate &x, const ctype &rf,
                        GlobalCoordinate &y );

        template< int rows, int dim >
        static void
        macroJacobianTransposed ( std::integral_constant< int, dim >,
                                  CornerIterator &cit,
                                  const LocalCoordinate &x, const ctype &rf,
                                  FieldMatrix< ctype, rows, cdim > &jt );
        template< int rows >
        static void
        macroJacobianTransposed ( std::integral_constant< int, 0 >,
                                  CornerIterator &cit,
                                  const LocalCoordinate &x, const ctype &rf,
                                  FieldMatrix< ctype, rows, cdim > &jt );

        template< int dim >
        static bool affine ( std::integral_constant< int, dim >,
                             CornerIterator &cit, JacobianTransposed &jt );
        static bool affine ( std::integral_constant< int, 0 >,
                             CornerIterator &cit, JacobianTransposed &jt );

        bool affine ( JacobianTransposed &jacobianTransposed ) const
        {
          CornerIterator cit = corners_.begin();
          return affine( std::integral_constant< int, mydimension >(), cit,
                         jacobianTransposed );
        }

      private:
        typename Traits::template CornerStorage< mydimension, coorddimension >
          ::Type corners_;
        //! there are div_^mydim subelements
        unsigned div_;
        //! index within the subelements, axis[0] counter varies fastest
        unsigned subindex_;
      };


      // SubsampledMultiLinearGeometry::JacobianInverseTransposed
      // ----------------------------------------------

      template< class ct, int mydim, int cdim, class Traits >
      class SubsampledMultiLinearGeometry< ct, mydim, cdim, Traits >
          ::JacobianInverseTransposed :
        public FieldMatrix< ctype, coorddimension, mydimension >
      {
        typedef FieldMatrix< ctype, coorddimension, mydimension > Base;

      public:
        void setup ( const JacobianTransposed &jt )
        {
          detInv_ = MatrixHelper::template rightInvA< mydimension,
                     coorddimension >( jt, static_cast< Base & >( *this ) );
        }

        void setupDeterminant ( const JacobianTransposed &jt )
        {
          detInv_ = MatrixHelper::template sqrtDetAAT< mydimension,
                     coorddimension >( jt );
        }

        ctype det () const { return ctype( 1 ) / detInv_; }
        ctype detInv () const { return detInv_; }

      private:
        ctype detInv_;
      };

      // Implementation of SubsampledMultiLinearGeometry
      // -------------------------------------

      template< class ct, int mydim, int cdim, class Traits >
      inline
      typename SubsampledMultiLinearGeometry< ct, mydim, cdim, Traits >
            ::JacobianInverseTransposed
      SubsampledMultiLinearGeometry< ct, mydim, cdim, Traits >
      ::jacobianInverseTransposed ( const LocalCoordinate &local ) const
      {
        JacobianInverseTransposed jit;
        jit.setup( jacobianTransposed( local ) );
        return jit;
      }


      template< class ct, int mydim, int cdim, class Traits >
      template< int dim >
      inline void SubsampledMultiLinearGeometry< ct, mydim, cdim, Traits >
      ::macroToGlobal ( std::integral_constant< int, dim >,
                        CornerIterator &cit, const LocalCoordinate &x,
                        const ctype &rf, GlobalCoordinate &y )
      {
        const ctype xn = x[ dim-1 ];
        const ctype cxn = ctype( 1 ) - xn;

        // apply (1-xn) times mapping for bottom
        macroToGlobal( std::integral_constant< int, dim-1 >(), cit, x, rf*cxn,
                       y );
        // apply xn times mapping for top
        macroToGlobal( std::integral_constant< int, dim-1 >(), cit, x, rf*xn,
                       y );
      }

      template< class ct, int mydim, int cdim, class Traits >
      inline void SubsampledMultiLinearGeometry< ct, mydim, cdim, Traits >
      ::macroToGlobal ( std::integral_constant< int, 0 >,
                        CornerIterator &cit, const LocalCoordinate &x,
                        const ctype &rf, GlobalCoordinate &y )
      {
        y.axpy(rf, *cit);
        ++cit;
      }


      template< class ct, int mydim, int cdim, class Traits >
      template< int rows, int dim >
      inline void SubsampledMultiLinearGeometry< ct, mydim, cdim, Traits >
      ::macroJacobianTransposed ( std::integral_constant< int, dim >,
                                  CornerIterator &cit,
                                  const LocalCoordinate &x, const ctype &rf,
                                  FieldMatrix< ctype, rows, cdim > &jt )
      {
        assert( rows >= dim );

        const ctype xn = x[ dim-1 ];
        const ctype cxn = ctype( 1 ) - xn;

        CornerIterator cit2( cit );
        // apply (1-xn) times Jacobian for bottom
        macroJacobianTransposed( std::integral_constant< int, dim-1 >(), cit2,
                                 x, rf*cxn, jt );
        // apply xn times Jacobian for top
        macroJacobianTransposed( std::integral_constant< int, dim-1 >(), cit2,
                                 x, rf*xn, jt );
        // compute last row as difference between top value and bottom value
        macroToGlobal( std::integral_constant< int, dim-1 >(), cit, x, -rf,
                       jt[ dim-1 ] );
        macroToGlobal( std::integral_constant< int, dim-1 >(), cit, x, rf,
                       jt[ dim-1 ] );
      }

      template< class ct, int mydim, int cdim, class Traits >
      template< int rows >
      inline void SubsampledMultiLinearGeometry< ct, mydim, cdim, Traits >
      ::macroJacobianTransposed ( std::integral_constant< int, 0 >,
                                  CornerIterator &cit,
                                  const LocalCoordinate &x, const ctype &rf,
                                  FieldMatrix< ctype, rows, cdim > &jt )
      {
        ++cit;
      }



      template< class ct, int mydim, int cdim, class Traits >
      template< int dim >
      inline bool SubsampledMultiLinearGeometry< ct, mydim, cdim, Traits >
      ::affine ( std::integral_constant< int, dim >, CornerIterator &cit,
                 JacobianTransposed &jt )
      {
        const GlobalCoordinate &orgBottom = *cit;
        if( !affine( std::integral_constant< int, dim-1 >(), cit, jt ) )
          return false;
        const GlobalCoordinate &orgTop = *cit;

        JacobianTransposed jtTop;
        if( !affine( std::integral_constant< int, dim-1 >(), cit, jtTop ) )
          return false;

        // check whether both jacobians are identical
        ctype norm( 0 );
        for( int i = 0; i < dim-1; ++i )
          norm += (jtTop[ i ] - jt[ i ]).two_norm2();
        if( norm >= Traits::tolerance() )
          return false;

        jt[ dim-1 ] = orgTop - orgBottom;
        return true;
      }

      template< class ct, int mydim, int cdim, class Traits >
      inline bool SubsampledMultiLinearGeometry< ct, mydim, cdim, Traits >
      ::affine ( std::integral_constant< int, 0 >, CornerIterator &cit,
                 JacobianTransposed &jt )
      {
        ++cit;
        return true;
      }

      template<class ct, int mydim, int cdim>
      using Geometry = SubsampledMultiLinearGeometry<ct, mydim, cdim,
                                                     GeometryTraits<ct> >;

    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_GEOMETRY_HH
