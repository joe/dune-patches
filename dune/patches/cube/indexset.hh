#ifndef DUNE_PATCHES_CUBE_INDEXSET_HH
#define DUNE_PATCHES_CUBE_INDEXSET_HH

namespace Dune {
  namespace Patches {
    namespace Cube {

    template<class Patch>
    class IndexSet
    {
    public:
      typedef typename Patch::Index IndexType;

      template<class Entity>
      auto index(const Entity &e) const -> decltype(e.index())
      {
        return e.index();
      }

      template<class Entity>
      IndexType subIndex(const Entity &e, unsigned i, unsigned codim) const
      {
        return e.subIndex(i, codim);
      }

      typedef typename Patch::Types Types;
      const Types &types(int codim) const
      {
        return Patch::types(codim);
      }
    };

    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_INDEXSET_HH
