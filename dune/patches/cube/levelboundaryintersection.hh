#ifndef DUNE_PATCHES_CUBE_LEVELBOUNDARYINTERSECTION_HH
#define DUNE_PATCHES_CUBE_LEVELBOUNDARYINTERSECTION_HH

#include <array>
#include <cassert>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include <dune/patches/cube/geometry.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {

      template<class Patch>
      class LevelBoundaryIntersection
      {
        typedef typename Patch::Index Index;
        const Patch *patch_;
        Index index_;

      protected:
        Index &iteratorIndex()       { return index_; }
        Index  iteratorIndex() const { return index_; }

      public:
        enum { codimension = 1 };
        enum { dimension = Patch::dimension };
        enum { mydimension = dimension - codimension };

        LevelBoundaryIntersection() = default;
        LevelBoundaryIntersection(const Patch *patch, unsigned level, Index index) :
          patch_(patch), index_(index)
        { assert(level==0); }

        const GeometryType &type() const
        {
          return Patch::types(codimension)[0];
        }

        bool neighbor() const { return false; }

        typedef typename Patch::LevelGridView::template Codim<0>::Entity
          Entity;
        Entity inside() const
        {
          return { patch_, 0, patch_->bInsideElements()[index_] };
        }
        Entity outside() const
        {
          DUNE_THROW(NotImplemented, "Attempt to retrieve outside information "
                     "of a boundary intersection");
        }

        Index indexInInside() const
        {
          return patch_->bIndexInInside()[index_];
        }
        Index indexInOutside() const
        {
          DUNE_THROW(NotImplemented, "Attempt to retrieve outside information "
                     "of a boundary intersection");
        }

        typedef Cube::Geometry<typename Patch::ctype, mydimension,
                               Patch::dimensionworld> Geometry;
        Geometry geometry() const
        {
          static const auto &refelem =
            Dune::ReferenceElements<typename Patch::ctype,dimension>::cube();
          constexpr auto myvertices = 1 << mydimension;
          auto eindex = patch_->bInsideElement_[index_];
          auto base = eindex * (1 << dimension);
          auto iie = indexInInside();

          std::array<FieldVector<typename Patch::ctype, Patch::dimensionworld>,
                     myvertices> corners;
          for(auto v : range(myvertices))
            corners[v] = patch_->elementVertices_
              [refelem.subEntity(iie, 1, v, dimension)];
          return { type(), corners };
        }

        typedef typename ReferenceElement<typename Patch::ctype, dimension>::
          template Codim<1>::Geometry LocalGeometry;
        LocalGeometry geometryInInside() const
        {
          static const auto &refelem =
            ReferenceElements<typename Patch::ctype, dimension>::cube();
          return refelem.geometry<1>(indexInInside());
        }
        LocalGeometry geometryInOutside() const
        {
          DUNE_THROW(NotImplemented, "Attempt to retrieve outside information "
                     "of a boundary intersection");
        }
      };

    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_LEVELBOUNDARYINTERSECTION_HH
