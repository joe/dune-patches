#ifndef DUNE_PATCHES_CUBE_MACROENTITY_HH
#define DUNE_PATCHES_CUBE_MACROENTITY_HH

#include <array>
#include <cassert>
#include <type_traits>

#if Vc_FOUND
#include <Vc/Vc>
#endif //Vc_FOUND

#include <dune/common/fvector.hh>

#include <dune/geometry/axisalignedcubegeometry.hh>
#include <dune/geometry/genericgeometry/topologytypes.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include <dune/patches/cube/geometry.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {

      template<class Patch, unsigned cd, class = void>
      class MacroEntity;

      template<class Patch>
      class MacroEntity<Patch, 0>
      {
        const Patch *patch_;
        typedef typename Patch::Index Index;
        Index index_;

      protected:
        Index &iteratorIndex()       { return index_; }
        Index  iteratorIndex() const { return index_; }

      public:
        enum { codimension = 0 };
        enum { dimension = Patch::dimension };
        enum { mydimension = dimension - codimension };

        MacroEntity() = default;
        MacroEntity(const Patch *patch, Index index) :
          patch_(patch), index_(index)
        { }

        typedef Cube::Geometry<typename Patch::ctype, mydimension,
                               dimension> Geometry;
        Geometry geometry() const
        {
          constexpr auto vertices = 1 << dimension;

          std::array<FieldVector<typename Patch::ctype, mydimension>, vertices>
            corners;
          for(auto v : range(vertices))
            corners[v] = patch_->vertex(patch_->entityVertex(0, index_, v));
          return { corners };
        }

        GeometryType type() const
        {
          return GeometryType
            (typename GenericGeometry::CubeTopology<mydimension>::type());
        }

        unsigned subEntities(unsigned codim) const
        {
          static const auto &refelem =
            ReferenceElements<typename Patch::ctype,mydimension>::cube();
          return refelem.size(codim);
        }
        template<unsigned codim>
        MacroEntity<Patch, codim> subEntity(Index n) const
        {
          assert(n < subEntities(codim));
          return { patch_, patch_->elementSubEntity(index_, codim, n) };
        }

        Index index() const { return index_; }

        Index subIndex(Index i, unsigned codim) const
        {
#ifndef NDEBUG
          assert(codim <= dimension);
          static const auto &refelem =
            ReferenceElements<typename Patch::ctype, dimension>::cube();
          auto subentities = refelem.size(codim);
          assert(subentities > 0 && i < Index(subentities));
#endif // !NDEBUG
          if(codim == 0)
            return index();
          else
            return patch_->elementSubEntity(index(), codim, i);
        }

      };

      template<class Patch, unsigned codim>
      class MacroEntity<
        Patch, codim,
        typename std::enable_if<Patch::dimension == codim>::type>
      {
        friend typename Patch::IndexSet;
        const Patch *patch_;
        typedef typename Patch::Index Index;
        Index index_;

      protected:
        Index &iteratorIndex()       { return index_; }
        Index  iteratorIndex() const { return index_; }

      public:
        enum { codimension = codim };
        enum { dimension = Patch::dimension };
        enum { mydimension = 0 };

        MacroEntity() = default;
        MacroEntity(const Patch *patch, Index index) :
          patch_(patch), index_(index)
        { }


        typedef Dune::AxisAlignedCubeGeometry<typename Patch::ctype,
                                              mydimension, dimension> Geometry;
        Geometry geometry() const
        {
          return { patch_->vertices_[index_] };
        }
        const GeometryType type() const
        {
          return { typename GenericGeometry::Point() };
        }

        Index index() const { return index_; }
      };

#if Vc_FOUND
      template<class Patch, unsigned cd, unsigned lanes, class = void>
      class VcMacroEntity;

      template<class Patch, unsigned lanes>
      class VcMacroEntity<Patch, 0, lanes>
      {
        const Patch *patch_;
        typedef typename Patch::Index Index;
        Index chunkIndex_;

      protected:
        Index &iteratorIndex()       { return chunkIndex_; }
        Index  iteratorIndex() const { return chunkIndex_; }

      public:
        enum { codimension = 0 };
        enum { dimension = Patch::dimension };
        enum { mydimension = dimension - codimension };

        VcMacroEntity() = default;
        VcMacroEntity(const Patch *patch, Index chunkIndex) :
          patch_(patch), chunkIndex_(chunkIndex)
        { }

        using Geometry =
          Cube::Geometry<Vc::SimdArray<typename Patch::ctype, lanes>,
                         mydimension, dimension>;
        Geometry geometry() const
        {
          constexpr auto vertices = 1 << dimension;

          std::array<FieldVector<Vc::SimdArray<typename Patch::ctype, lanes>,
                                 mydimension>, vertices> corners;
          for(auto v : range(vertices))
            patch_->loadVertex(corners[v],
                               patch_->template entityVertexChunk<lanes>
                                 (0, chunkIndex_, v));
          return { corners };
        }

        GeometryType type() const
        {
          return GeometryType
            (typename GenericGeometry::CubeTopology<mydimension>::type());
        }

        unsigned subEntities(unsigned codim) const
        {
          static const auto &refelem =
            ReferenceElements<typename Patch::ctype,mydimension>::cube();
          return refelem.size(codim);
        }

        Vc::SimdArray<Index, lanes> index() const
        {
          return Vc::SimdArray<Index, lanes>::IndicesFromZero()
            + chunkIndex_*lanes;
        }
      };
#endif //Vc_FOUND

    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_MACROENTITY_HH
