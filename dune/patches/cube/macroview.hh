#ifndef DUNE_PATCHES_CUBE_MACROVIEW_HH
#define DUNE_PATCHES_CUBE_MACROVIEW_HH

#include <dune/geometry/type.hh>

#include <dune/patches/common/indexediterator.hh>
#include <dune/patches/cube/macroboundaryintersection.hh>
#include <dune/patches/cube/macroentity.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {

      template<class Patch>
      class MacroView
      {
        const Patch *patch_;

      public:
        enum { dimension = Patch::dimension };
        enum { dimensionworld = Patch::dimensionworld };
        typedef typename Patch::ctype ctype;
        typedef typename Patch::Index Index;

        MacroView() = default;
        MacroView(const Patch *patch) : patch_(patch) { }

        typedef Patch Grid;
        const Patch &grid() const
        {
          return *patch_;
        }

        typedef typename Patch::IndexSet IndexSet;
        IndexSet indexSet() const { return patch_->indexSet(); }

        // entities
        template<unsigned codim>
        struct Codim
        {
          typedef MacroEntity<Patch, codim> Entity;
          typedef typename Entity::Geometry Geometry;
          typedef IndexedIterator<Entity> Iterator;
#if Vc_FOUND
          template<unsigned lanes>
          struct Vc {
            using Entity = VcMacroEntity<Patch, codim, lanes>;
            using Iterator = IndexedIterator<Entity>;
          };
#endif // Vc_FOUND
        };

        template<unsigned codim>
        typename Codim<codim>::Iterator begin() const
        {
          return { patch_, 0 };
        }
        template<unsigned codim>
        typename Codim<codim>::Iterator end() const
        {
          return { patch_, size(codim) };
        }
#if Vc_FOUND
        template<unsigned codim, unsigned lanes>
        IteratorRange<typename Codim<codim>::template Vc<lanes>::Iterator>
        vcRange() const
        {
          if(size(codim) % lanes != 0)
            DUNE_THROW(NotImplemented, "Tail handling");
          using Iterator =
            typename Codim<codim>::template Vc<lanes>::Iterator;
          return { Iterator(patch_, 0), Iterator(patch_, size(codim)/lanes) };
        }
#endif // Vc_FOUND

        // boundary intersections
        typedef MacroBoundaryIntersection<Patch> BoundaryIntersection;
        typedef IndexedIterator<BoundaryIntersection>
          BoundaryIntersectionIterator;
        BoundaryIntersectionIterator bBegin() const
        {
          return { patch_, 0 };
        }
        BoundaryIntersectionIterator bEnd() const
        {
          return { patch_, patch_->bInsideElements_.size() };
        }

        // interior intersections
        class InteriorIntersection;
        class InteriorIntersectionIterator;
        InteriorIntersectionIterator iBegin() const;
        InteriorIntersectionIterator iEnd() const;

        // size
        Index size(unsigned codim) const
        {
          if(codim > dimension)
            return 0;
          else
            return patch_->entityCount(codim);
        }
        Index size(const GeometryType &t) const
        {
          if(t.isCube())
            return size(dimension - t.dim());
          else
            return 0;
        }
      };

    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_MACROVIEW_HH
