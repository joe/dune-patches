#install headers
install(FILES
  fwd.hh
  indexset.hh
  levelentity.hh
  levelview.hh
  patch.hh
  DESTINATION include/dune/patches/cube/single)
