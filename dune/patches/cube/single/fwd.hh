#ifndef DUNE_PATCHES_CUBE_SINGLE_FWD_HH
#define DUNE_PATCHES_CUBE_SINGLE_FWD_HH

namespace Dune {
  namespace Patches {
    namespace Cube {
      namespace Single {

        //////////////////////////////////////////////////////////////////////
        //
        // Forward Declarations
        //

        using Index = unsigned;

        template<class ct, unsigned dim, unsigned dimw>
        class Patch;

        template<class ct, unsigned dim, unsigned dimw>
        class LevelView;

        class IndexSet;

        template<class ct, unsigned dim, unsigned dimw, unsigned codim>
        class LevelEntity;


      } // namespace Single
    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_SINGLE_FWD_HH
