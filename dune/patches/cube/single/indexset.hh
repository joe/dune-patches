#ifndef DUNE_PATCHES_CUBE_SINGLE_INDEXSET_HH
#define DUNE_PATCHES_CUBE_SINGLE_INDEXSET_HH

#include <dune/patches/cube/single/fwd.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {
      namespace Single {

        //////////////////////////////////////////////////////////////////////
        //
        // IndexSet
        //
        class IndexSet
        {
        public:
          using Index = Single::Index;

          template<class Entity>
          Index index(const Entity &e) const
          {
            return e.index_;
          }

          template<class Entity>
          Index subIndex(const Entity &e, unsigned i, unsigned codim) const
          {
            return e.subIndex(i, codim);
          }
        };

      } // namespace Single
    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_SINGLE_INDEXSET_HH
