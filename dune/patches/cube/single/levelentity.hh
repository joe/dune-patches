#ifndef DUNE_PATCHES_CUBE_SINGLE_LEVELENTITY_HH
#define DUNE_PATCHES_CUBE_SINGLE_LEVELENTITY_HH

#include <cassert>
#include <type_traits>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/unused.hh>

#include <dune/geometry/axisalignedcubegeometry.hh>
#include <dune/geometry/type.hh>

#include <dune/patches/cube/geometry.hh>
#include <dune/patches/cube/single/fwd.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {
      namespace Single {

        //////////////////////////////////////////////////////////////////////
        //
        // The LevelEntity
        //

        template<class ct, unsigned dim, unsigned dimw>
        class LevelEntity<ct, dim, dimw, 0>
        {
          const Patch<ct, dim, dimw> *patch_;
          Index index_;
          unsigned level_;

        protected:
          // for IndexIterator
          Index &iteratorIndex()       { return index_; }
          Index  iteratorIndex() const { return index_; }

        public:
          enum { codimension = 0 };
          enum { dimension = dim };
          enum { mydimension = dimension - codimension };

          LevelEntity() = default;

        private:
          LevelEntity(const Patch<ct, dim, dimw> *patch, unsigned level,
                      Index index) :
            patch_(patch), index_(index), level_(level)
          { }

        public:
          using Geometry = Cube::Geometry<ct, mydimension, dimension>;
          Geometry geometry() const
          {
            return { patch_->corners, 1u << level_, index_ };
          }

          static GeometryType type()
          {
            GeometryType gt;
            gt.makeCube(dim);
            return gt;
          }

          unsigned subEntities(unsigned codim) const
          {
            switch(codim) {
            case 0:         return 1u;
            case dimension: return 1u << dim;
            default:
              DUNE_THROW(NotImplemented,
                         "subEntities() for codimension " << codim);
            }
          }
          template<unsigned codim>
          typename std::enable_if<codim==dim,
                                  LevelEntity<ct, dim, dimw, codim> >::type
          subEntity(Index n) const
          {
            assert(n < subEntities(codim));
            return { patch_, level_, subVertex(n) };
          }
          template<unsigned codim>
          typename std::enable_if<codim==0, const LevelEntity &>::type
          subEntity(Index n) const
          {
            assert(n == 0);
            return *this;
          }

        private:
          Index subVertex(unsigned v) const
          {
            assert(v < (1u << dimension));

            auto div = 1u << level_;

            Index elemIndex = index_;
            Index vertexIndex = 0;
            Index vertexBase = 1;

            for(auto DUNE_UNUSED d : range(dimension))
            {
              vertexIndex += vertexBase * (elemIndex % div + v % 2);
              elemIndex /= div;
              v /= 2;
              vertexBase *= (div+1);
            }

            return vertexIndex;
          }

          Index subIndex(unsigned i, unsigned codim) const
          {
            switch(codim) {
            case 0:
              assert(i == 0);
              return index_;
            case dimension:
              return subVertex(i);
            default:
              DUNE_THROW(NotImplemented,
                         "subindices for codimension " << codim);
            }
          }

          friend class LevelView<ct, dim, dimw>;
          friend class IndexSet;
        };

        template<class ct, unsigned dim, unsigned dimw>
        class LevelEntity<ct, dim, dimw, dim>
        {
          const Patch<ct, dim, dimw> *patch_;
          Index index_;
          unsigned level_;

        protected:
          // for IndexIterator
          Index &iteratorIndex()       { return index_; }
          Index  iteratorIndex() const { return index_; }

        private:
          friend LevelEntity<ct, dim, dimw, 0>;
          LevelEntity(const Patch<ct, dim, dimw> *patch, unsigned level,
                      Index index) :
            patch_(patch), index_(index), level_(level)
          { }

        public:
          enum { codimension = dim };
          enum { dimension = dim };
          enum { mydimension = 0 };

          LevelEntity() = default;

          using Geometry = Dune::AxisAlignedCubeGeometry<ct, 0, dim>;
          Geometry geometry() const
          {
            auto div = 1u << level_;
            auto tmp = index_;

            Dune::FieldVector<ct, dimension> local;
            for(auto &c : local)
            {
              c = ct(tmp % (div + 1)) / div;
              tmp /= (div + 1);
            }

            return { patch_->geometry().global(local) };
          }
          static GeometryType type()
          {
            GeometryType gt;
            gt.makeCube(0);
            return gt;
          }

          friend class LevelView<ct, dim, dimw>;
          friend class IndexSet;
        };

      } // namespace Single
    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_SINGLE_LEVELENTITY_HH
