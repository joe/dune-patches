#ifndef DUNE_PATCHES_CUBE_SINGLE_LEVELVIEW_HH
#define DUNE_PATCHES_CUBE_SINGLE_LEVELVIEW_HH

#include <dune/geometry/dimension.hh>

#include <dune/grid/common/rangegenerators.hh>

#include <dune/patches/common/indexediterator.hh>
#include <dune/patches/cube/single/fwd.hh>
#include <dune/patches/cube/single/indexset.hh>
#include <dune/patches/cube/single/levelentity.hh>
#include <dune/patches/utility/math.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {
      namespace Single {

        //////////////////////////////////////////////////////////////////////
        //
        // The LevelView
        //
        template<class ct, unsigned dim, unsigned dimw>
        class LevelView
        {
          const Patch<ct, dim, dimw> *patch_;
          unsigned level_;

        public:
          using ctype = ct;
          enum { dimension = dim };
          enum { dimensionworld = dimw };

          LevelView() = default;
          LevelView(const Patch<ct, dim, dimw> *patch, unsigned level) :
            patch_(patch), level_(level)
          { }

          using IndexSet = Single::IndexSet;
          IndexSet indexSet() const { return {}; }

          // entities
          template<unsigned codim>
          struct Codim {
            using Entity = LevelEntity<ct, dim, dimw, codim>;
            using Iterator = IndexedIterator<Entity>;
          };

          template<unsigned codim>
          typename Codim<codim>::Iterator begin() const
          {
            return typename Codim<codim>::Entity(patch_, level_, 0);
          }
          template<unsigned codim>
          typename Codim<codim>::Iterator end() const
          {
            return typename Codim<codim>::Entity(patch_, level_, size(codim));
          }

          // size
          constexpr Index size(unsigned codim) const
          {
            return binomial(unsigned(dimension), codim)
              * ipow(1u << level_, dimension - codim)
              * ipow((1u << level_) + 1u, codim);
          }
          Index size(const GeometryType &t) const
          {
            if(t.isCube())
              return size(dimension - t.dim());
            else
              return 0;
          }
        };

        //////////////////////////////////////////////////////////////////////
        //
        // Ranges
        //
        template<typename ct, unsigned dim, unsigned dimw, int codim>
        IteratorRange<typename LevelView<ct, dim, dimw>::
                      template Codim<codim>::Iterator>
        entities(const LevelView<ct, dim, dimw>& pv, Codim<codim> cd)
        {
          static_assert(0 <= codim && codim <= dim,
                        "invalid codimension for given PatchView");
          return { pv.template begin<codim>(), pv.template end<codim>() };
        }

        using Dune::elements;
        using Dune::facets;
        using Dune::edges;
        using Dune::vertices;

      } // namespace Single
    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_SINGLE_LEVELVIEW_HH
