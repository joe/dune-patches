#ifndef DUNE_PATCHES_CUBE_SINGLE_PATCH_HH
#define DUNE_PATCHES_CUBE_SINGLE_PATCH_HH

#include <array>
#include <functional>

#include <dune/common/fvector.hh>

#include <dune/geometry/genericgeometry/topologytypes.hh>
#include <dune/geometry/multilineargeometry.hh>
#include <dune/geometry/type.hh>

#include <dune/patches/cube/single/fwd.hh>
#include <dune/patches/cube/single/levelview.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {
      namespace Single {

        //////////////////////////////////////////////////////////////////////
        //
        // The Patch
        //

        template<class ct, unsigned dim, unsigned dimw = dim>
        class Patch {
          struct GeometryTraits : MultiLinearGeometryTraits<ct>
          {
            template< int mydim, int cdim >
            struct CornerStorage
            {
              using Type = std::reference_wrapper<
                const std::array< FieldVector< ct, cdim >, (1 << mydim) > >;
            };

            template< int d >
            struct hasSingleGeometryType
            {
              static const bool v = true;
              static const unsigned int topologyId =
                GenericGeometry::CubeTopology< d >::type::id;
            };
          };

        public:
          enum { dimension = dim };
          enum { dimensionworld = dimw };
          typedef ct ctype;
          using Index = Single::Index;
          using Geometry = MultiLinearGeometry<ct, dim, dimw, GeometryTraits>;

        private:
          std::array<FieldVector<ctype, dimensionworld>,
                     (1 << dimension)> corners;

        public:
          const FieldVector<ctype, dimensionworld> &macroVertex(Index i) const
          {
            return corners[i];
          }
          // modification of the returned object modifies the patch, and
          // geometries will no longer be consistent.
          FieldVector<ctype, dimensionworld> &macroVertex(Index i)
          {
            return corners[i];
          }

          Geometry geometry() const
          {
            GeometryType gt;
            gt.makeCube(dim);
            return { gt, corners };
          }

          // general use
          using LevelGridView = LevelView<ctype, dimension, dimensionworld>;
          LevelGridView levelGridView(unsigned level) const
          { return { this, level }; }

          template<class, unsigned, unsigned, unsigned>
          friend struct LevelEntity;
        };

        template<class Geometry>
        Patch<typename Geometry::ctype, Geometry::mydimension,
              Geometry::coorddimension>
        makeFromGeometry(const Geometry &geo)
        {
          Patch<typename Geometry::ctype, Geometry::mydimension,
                Geometry::coorddimension> patch;
          for(auto c : range(geo.corners()))
            patch.macroVertex(c) = geo.corner(c);
          return patch;
        }

        template<class ctype, int dim>
        Patch<ctype, dim> makeCube(const FieldVector<ctype, dim> &lower,
                                   const FieldVector<ctype, dim> &upper)
        {
          Patch<ctype, dim> patch;
          for(auto c : range(1 << dim))
          {
            auto &v = patch.macroVertex(c);
            for(auto d : range(dim))
              if((c >> d) % 2 == 0)
                v[d] = lower[d];
              else
                v[d] = upper[d];
          }
          return patch;
        }

        template<class ctype, unsigned dim>
        const Patch<ctype, dim> &makeUnitCube()
        {
          static const auto patch =
            makeCube(FieldVector<ctype, dim>(0),
                     FieldVector<ctype, dim>(1));
          return patch;
        }

      } // namespace Single
    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_SINGLE_PATCH_HH
