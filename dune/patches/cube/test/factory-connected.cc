#include <config.h>

#include <dune/common/array.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/common/rangegenerators.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/patches/cube/connected/patch.hh>
#include <dune/patches/utility/compare_views.hh>
#include <dune/patches/utility/gridviewpatchfactory.hh>

int main(int argc, char **argv)
{
  Dune::MPIHelper::instance(argc, argv);

  int result = 0;

  constexpr unsigned dim = 2;
  using Grid = Dune::YaspGrid<dim>;
  using ctype = Grid::ctype;
  Grid grid(Dune::FieldVector<ctype, dim>(1),
            Dune::fill_array<int, dim>(10));

  using Patch = Dune::Patches::Cube::Connected::Patch<Grid::ctype, dim>;
  using Factory = Dune::Patches::GridViewPatchFactory<Grid::LeafGridView>;
  Factory factory(grid.leafGridView());
  // I'm creating the leafGridView twice here on purpose, as kind of a small
  // insurance the it really work that way
  auto patchp = factory.create<Patch>(elements(grid.leafGridView()));

  auto &patch = *patchp;

  using Dune::Patches::compare_views;

  compare_views(result,
                "view1=grid.leafGridView(); view2=patch.macroGridView()",
                grid.leafGridView(), patch.macroGridView());

  compare_views(result,
                "view1=grid.leafGridView(); view2=patch.levelGridView(0)",
                grid.leafGridView(), patch.levelGridView(0));

  return result;
}
