#include <config.h>

#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/common/rangegenerators.hh>

#include <dune/patches/cube/connected/patch.hh>
#include <dune/patches/meshes/tensorproduct.hh>
#include <dune/patches/utility/check_range_geometry.hh>
#include <dune/patches/utility/compare_views.hh>

int main(int argc, char **argv)
{
  Dune::MPIHelper::instance(argc, argv);

  int result = 77;

  static const int dim = 2;             /*@\label{fem:dim}@*/
  typedef double ctype;
  typedef Dune::Patches::Cube::Connected::Patch<ctype, dim> Patch;

  auto patchp = Dune::Patches::Meshes::tensorProduct<Patch>
    ( Dune::FieldVector<ctype,dim>(0), Dune::FieldVector<ctype,dim>(1),
      Dune::FieldVector<Patch::Index, dim>(10));

  auto &patch = *patchp;

  using Dune::Patches::check_range_geometry;

  check_range_geometry(result, "Coordinates in bbox on macroGridView()",
                       elements(patch.macroGridView()));
  check_range_geometry(result, "Coordinates in bbox on levelGridView(0)",
                       elements(patch.levelGridView(0)));

  using Dune::Patches::compare_views;

  compare_views(result,
                "view1=patch.macroGridView(); view2=patch.levelGridView(0)",
                patch.macroGridView(), patch.levelGridView(0));

  return result;
}
