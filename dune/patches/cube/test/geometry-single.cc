#include <config.h>

#include <string>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/patches/cube/single/patch.hh>
#include <dune/patches/utility/check_range_geometry.hh>
#include <dune/patches/utility/range.hh>

int main(int argc, char **argv)
{
  Dune::MPIHelper::instance(argc, argv);

  int result = 77;

  static const int dim = 2;             /*@\label{fem:dim}@*/
  typedef double ctype;
  auto patch = Dune::Patches::Cube::Single::makeUnitCube<ctype, dim>();

  using Dune::Patches::check_range_geometry;
  using Dune::Patches::range;
  using std::to_string;

  for(auto level : range(4))
    check_range_geometry(result,
                         "Coordinates in bbox on levelGridView("
                         + to_string(level) + ")",
                         elements(patch.levelGridView(level)));

  return result;
}
