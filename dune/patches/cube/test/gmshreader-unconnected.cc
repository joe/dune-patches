#include <config.h>

#include <iostream>
#include <ostream>
#include <string>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/gmshreader.hh>

#include <dune/patches/cube/unconnected/factory.hh>
#include <dune/patches/cube/unconnected/patch.hh>
#include <dune/patches/utility/compare_views.hh>

int main(int argc, char **argv)
{
  try {
    Dune::MPIHelper::instance(argc, argv);

    int result = 0;

    std::string filename = "gmshreader.msh";

    constexpr unsigned dim = 2;
    using Patch = Dune::Patches::Cube::Unconnected::Patch<double, dim>;

    Dune::GridFactory<Patch> factory;
    Dune::GmshReader<Patch>::read(factory, filename, true, false);
    auto patchp = factory.createGrid();

    auto &patch = *patchp;

    using Dune::Patches::compare_views;

    compare_views(result,
                  "view1=patch.levelGridView(0); view2=patch.macroGridView()",
                  patch.levelGridView(0), patch.macroGridView());

    return result;
  }
  catch(const Dune::Exception &e) {
    std::cerr << "Caught Dune::Exception:" << std::endl;
    std::cerr << e << std::endl;
    throw;
  }
}
