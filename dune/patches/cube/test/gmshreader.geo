cl = 0.1;

Point(1) = {0, 0, 0, cl};
Extrude {1, 0, 0} {
  Point{1};
}
Extrude {0, 1, 0} {
  Line{1};
}

Delete {
  Surface{5};
}

Point(6) = {0.25, 0.5, 0, cl};
Point(7) = {0.5, 0.75, 0, cl};
Point(8) = {0.75, 0.5, 0, cl};
Point(9) = {0.5, 0.25, 0, cl};
Extrude {{0, 0, 1}, {.5, .5, 0}, Pi/2} {
  Point{7, 8, 9, 6};
}
Line Loop(9) = {3, 2, -4, -1};
Line Loop(10) = {5, 8, 7, 6};
Plane Surface(11) = {9, 10};
Recombine Surface {11};
