#include <config.h>

#include <cstring>
#include <iostream>
#include <ostream>
#include <vector>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/patches/cube/single/patch.hh>
#include <dune/patches/utility/compare_views.hh>
#include <dune/patches/utility/range.hh>

int main(int argc, char **argv)
{
  using std::to_string;

  using Dune::Patches::compare_views;
  using Dune::Patches::range;

  Dune::MPIHelper::instance(argc, argv);

  int result = 0;

  static const int dim = 2;             /*@\label{fem:dim}@*/
  typedef double ctype;
  auto orig = Dune::Patches::Cube::Single::makeUnitCube<ctype, dim>();

  std::cout << "copying: orig -> copy..." << std::endl;
  std::vector<char> buf(sizeof(orig));
  std::memcpy(buf.data(), &orig, sizeof(orig));
  auto &copy = *reinterpret_cast<decltype(orig)*>(buf.data());

  for(auto level : range(4))
  {
    auto ls = to_string(level);
    compare_views(result,
                  "view1=level-"+ls+"view(orig); view2=level-"+ls+"-view(copy)",
                  orig.levelGridView(level), copy.levelGridView(level));
  }

  return result;
}
