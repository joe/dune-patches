#include <config.h>

#include <cstring>
#include <iostream>
#include <ostream>
#include <vector>

#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/patches/cube/unconnected/patch.hh>
#include <dune/patches/meshes/tensorproduct.hh>
#include <dune/patches/utility/compare_views.hh>

int main(int argc, char **argv)
{
  Dune::MPIHelper::instance(argc, argv);

  int result = 0;

  static const int dim = 2;             /*@\label{fem:dim}@*/
  typedef double ctype;
  typedef Dune::Patches::Cube::Unconnected::Patch<ctype, dim> Patch;

  auto patchp = Dune::Patches::Meshes::tensorProduct<Patch>
    ( Dune::FieldVector<ctype,dim>(0), Dune::FieldVector<ctype,dim>(1),
      Dune::FieldVector<Patch::Index, dim>(10));
  auto &orig = *patchp;

  using Dune::Patches::compare_views;

  compare_views(result, "view1=macro-view(orig); view2=level-0-view(orig)",
                orig.macroGridView(), orig.levelGridView(0));

  std::cout << "copying: orig -> copy..." << std::endl;
  std::vector<char> buf(patchp->allocatedSize());
  std::memcpy(buf.data(), patchp.get(), patchp->allocatedSize());
  auto &copy = *reinterpret_cast<Patch*>(buf.data());

  compare_views(result, "view1=macro-view(orig); view2=macro-view(copy)",
                orig.macroGridView(), copy.macroGridView());
  compare_views(result, "view1=macro-view(orig); view2=level-0-view(copy)",
                orig.macroGridView(), copy.levelGridView(0));
  compare_views(result, "view1=level-0-view(orig); view2=macro-view(copy)",
                orig.levelGridView(0), copy.macroGridView());
  compare_views(result, "view1=level-0-view(orig); view2=level-0-view(copy)",
                orig.levelGridView(0), copy.levelGridView(0));

  std::cout << "freeing: orig..." << std::endl;
  patchp = nullptr; // orig cannot be used any longer after this

  compare_views(result, "view1=macro-view(copy); view2=level-0-view(copy)",
                copy.macroGridView(), copy.levelGridView(0));

  return result;
}
