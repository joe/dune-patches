#ifndef DUNE_PATCHES_CUBE_TEST_POISSON_COMMON_HH
#define DUNE_PATCHES_CUBE_TEST_POISSON_COMMON_HH

#include <cstddef>

#ifdef HAVE_VC
#include <Vc/Vc>
#endif

#include <dune/common/fvector.hh>

template<class RFF>
class F
{
  template<class Domain>
  static auto inBlob(const Domain &x)
  {
    return x[0]>0.25 && x[0]<0.375 && x[1]>0.25 && x[1]<0.375;
  }

public:
  template<class Domain>
  auto operator()(const Domain &x) const
  {
    using Range = Dune::FieldVector<RFF, 1>;
    return Range(inBlob(x) ? RFF(50) : RFF(0));
  }

#ifdef HAVE_VC
  template<class DF, std::size_t N, class V, std::size_t VectorSize,
           int dim>
  auto operator()
  (const Dune::FieldVector<Vc::SimdArray<DF, N, V, VectorSize>, dim>& x) const
  {
    using RF = Vc::SimdArray<RFF, N>;
    using Range = Dune::FieldVector<RF, 1>;
    typename RF::mask_type b = inBlob(x);
    return Range(iif(b, RF(50), RF(0)));
  }
#endif // HAVE_VC

  template<class Element, class Domain>
  auto operator()(const Element& e, const Domain& x) const
  {
    return (*this)(e.geometry().global(x));
  }
};


#endif // DUNE_PATCHES_CUBE_TEST_POISSON_COMMON_HH
