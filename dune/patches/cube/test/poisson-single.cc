// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <array>
#include <cmath>
#include <iostream>
#include <ostream>
#include <string>
#include <type_traits>

#include <dune/common/array.hh>
#include <dune/pdelab/common/clock.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/common/rangegenerators.hh>
#include <dune/grid/io/file/vtk/common.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/istl/preconditioners.hh>
#include <dune/istl/solver.hh>
#include <dune/istl/solvers.hh>

#include <dune/pdelab/backend/interface.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/geometrywrapper.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/constraints/common/constraintsparameters.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include <dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/localvector.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>

#include <dune/patches/cube/single/patch.hh>
#include <dune/patches/cube/test/poisson-common.hh>
#include <dune/patches/finiteelementmap/refinedq1fem.hh>
#include <dune/patches/localoperator/poisson.hh>
#include <dune/patches/utility/environment.hh>
#include <dune/patches/utility/gridviewpatchfactory.hh>
#include <dune/patches/utility/range.hh>
#include <dune/patches/utility/unittest.hh>


//===============================================================
//===============================================================
// Solve the Poisson equation
//           - \Delta u = f in \Omega,
//                    u = g on \partial\Omega_D
//  -\nabla u \cdot \nu = j on \partial\Omega_N
//===============================================================
//===============================================================

//===============================================================
// Define parameter functions f,g,j and \partial\Omega_D/N
//===============================================================

// f is defined in poisson-common.hh

// boundary grid function selecting boundary conditions
class ConstraintsParameters
  : public Dune::PDELab::DirichletConstraintsParameters
{

public:

  template<typename I>
  bool isDirichlet(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
  {
    return true;
  }

  template<typename I>
  bool isNeumann(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
  {
    return !isDirichlet(ig,x);
  }

};


// function for Dirichlet boundary conditions and initialization
template<typename GV, typename RF>
class G
  : public Dune::PDELab::AnalyticGridFunctionBase<Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
                                                  G<GV,RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,G<GV,RF> > BaseT;

  G (const GV& gv) : BaseT(gv) {}
  inline void evaluateGlobal (const typename Traits::DomainType& x,
                              typename Traits::RangeType& y) const
  {
    typename Traits::DomainType center;
    for (int i=0; i<GV::dimension; i++) center[i] = 0.5;
    center -= x;
    y = exp(-center.two_norm2());
  }
};

// function for defining the flux boundary condition
template<typename GV, typename RF>
class J
  : public Dune::PDELab::AnalyticGridFunctionBase<Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
                                                  J<GV,RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,J<GV,RF> > BaseT;

  J (const GV& gv) : BaseT(gv) {}
  inline void evaluateGlobal (const typename Traits::DomainType& x,
                              typename Traits::RangeType& y) const
  {
    y = 0.0;
  }
};

class Stats {
  unsigned samples_;
  double sum_;
  double sqrSum_;

public:
  Stats() :
    samples_(0), sum_(0), sqrSum_(0)
  { }

  void addSample(double value)
  {
    ++samples_;
    sum_ += value;
    sqrSum_ += value*value;
  }

  unsigned samples() const { return samples_; }
  double mean() const { return sum_/samples_; }
  double sdev() const {
    using std::sqrt;
    return sqrt((sqrSum_ - sum_*sum_/samples_) / (samples_-1));
  }
};

std::ostream &operator<<(std::ostream &str, const Stats &stats)
{
  str << "[ samples = " << stats.samples() << ", mean = " << stats.mean()
      << ", sdev = " << stats.sdev() << " ]";
  return str;
}

template<class PV, class R>
class Q1PatchLFS {
public:
  struct Traits {
    using FiniteElementType =
      Dune::Q1LocalFiniteElement<typename PV::ctype, R, PV::dimension>;
  };

private:
  typename PV::IndexSet is_;
  typename PV::template Codim<0>::Entity e_;
  typename Traits::FiniteElementType fe_;

public:
  Q1PatchLFS(const PV &pv) : is_(pv.indexSet()) { }
  void bind(const typename PV::template Codim<0>::Entity &e) { e_ = e; }
  const typename Traits::FiniteElementType &finiteElement() const
  {
    return fe_;
  }
  auto localIndex(unsigned i) const
    -> decltype(is_.subIndex(e_, i, PV::dimension))
  {
    return is_.subIndex(e_, i, PV::dimension);
  }
};

template<class PV, class R>
Q1PatchLFS<PV, R> makeQ1PatchLFS(const PV &pv, R /*dummy*/)
{
  return { pv };
}

template<class GFS, class LOP, class CG, class F>
class SimpleGridOperator
{
  using V = typename Dune::PDELab::BackendVectorSelector<GFS, F>::Type;

  const GFS &gfs_;
  const CG &cg_;
  const LOP &lop_;
  Stats residualStats_;
  Stats jacobianApplyStats_;
  unsigned verbosity_;
  unsigned subRefines_;

  static double toDouble(const Dune::PDELab::TimeSpec &time)
  {
    return time.tv_sec + 1e-9*time.tv_nsec;
  }

public:
  struct Traits {
    using Domain = V;
    using Range = V;
  };

  SimpleGridOperator(const GFS &gfs, const CG &cg, const LOP &lop,
                     unsigned verbosity = 1) :
    gfs_(gfs),
    cg_(cg),
    lop_(lop),
    verbosity_(verbosity)
  {
    if(verbosity_ >= 1)
      std::cout << "SimpleGridOperator: not vectorizing." << std::endl;
  }

  void setSubRefines(unsigned subRefines) { subRefines_ = subRefines; }

  void residual(const V &x, V &y)
  {
    using Dune::Patches::makeIteratorRange;

    auto start = Dune::PDELab::getWallTime();

    const auto &gv = gfs_.gridView();

    using LFS = Dune::PDELab::LocalFunctionSpace<GFS>;
    LFS lfs(gfs_);

    using LFSCache = Dune::PDELab::LFSIndexCache<LFS>;
    LFSCache lfsCache(lfs);

    using ConstLocalView = typename V::template ConstLocalView<LFSCache>;
    ConstLocalView localViewX(x);
    using LocalView = typename V::template LocalView<LFSCache>;
    LocalView localViewY(y);

    Dune::PDELab::LocalVector<F> xl(lfs.maxSize());

    Dune::PDELab::LocalVector<F> yl(lfs.maxSize());
    typename Dune::PDELab::LocalVector<F>::WeightedAccumulationView
      yl_view(yl, 1.0);

    for(const auto &e : elements(gv))
    {
      auto patch = Dune::Patches::Cube::Single::makeFromGeometry(e.geometry());
      auto pv = patch.levelGridView(subRefines_);
      auto plfs = makeQ1PatchLFS(pv, typename LFS::Traits::FiniteElementType::
                                 Traits::LocalBasisType::Traits::
                                 RangeFieldType(0));

      lfs.bind(e);
      lfsCache.update();
      localViewX.bind(lfsCache);
      localViewY.bind(lfsCache);

      localViewX.read(xl);
      yl = 0;

      for(const auto &pe : elements(pv))
      {
        plfs.bind(pe);
        Dune::PDELab::ElementGeometry<typename std::decay<decltype(pe)>::type>
          eg(pe);
        lop_.alpha_volume(eg, plfs, xl, plfs, yl_view);
        lop_.lambda_volume(eg, plfs, yl_view);
        // skip boundary, we do only Dirichlet
      }

      localViewY.add(yl);
      localViewY.commit();

      localViewX.unbind();
      localViewY.unbind();
    }

    Dune::PDELab::constrain_residual(cg_, y);

    auto stop = Dune::PDELab::getWallTime();
    if(verbosity_ >= 2)
      std::cout << "residual() took " << (stop - start) << "s" << std::endl;
    residualStats_.addSample(toDouble(stop-start));
  }
  void jacobian_apply(const V &x, V &y)
  {
    using Dune::Patches::makeIteratorRange;

    auto start = Dune::PDELab::getWallTime();

    const auto &gv = gfs_.gridView();

    using LFS = Dune::PDELab::LocalFunctionSpace<GFS>;
    LFS lfs(gfs_);

    using LFSCache = Dune::PDELab::LFSIndexCache<LFS>;
    LFSCache lfsCache(lfs);

    using ConstLocalView = typename V::template ConstLocalView<LFSCache>;
    ConstLocalView localViewX(x);
    using LocalView = typename V::template LocalView<LFSCache>;
    LocalView localViewY(y);

    Dune::PDELab::LocalVector<F> xl(lfs.maxSize());

    Dune::PDELab::LocalVector<F> yl(lfs.maxSize());
    typename Dune::PDELab::LocalVector<F>::WeightedAccumulationView
      yl_view(yl, 1.0);

    for(const auto &e : elements(gv))
    {
      auto patch = Dune::Patches::Cube::Single::makeFromGeometry(e.geometry());
      auto pv = patch.levelGridView(subRefines_);
      auto plfs = makeQ1PatchLFS(pv, typename LFS::Traits::FiniteElementType::
                                 Traits::LocalBasisType::Traits::
                                 RangeFieldType(0));

      lfs.bind(e);
      lfsCache.update();
      localViewX.bind(lfsCache);
      localViewY.bind(lfsCache);

      localViewX.read(xl);
      yl = 0;

      for(const auto &pe : elements(pv))
      {
        plfs.bind(pe);
        Dune::PDELab::ElementGeometry<typename std::decay<decltype(pe)>::type>
          eg(pe);
        lop_.jacobian_apply_volume(eg, plfs, xl, plfs, yl_view);
        // skip boundary, we do only Dirichlet
      }

      localViewY.add(yl);
      localViewY.commit();

      localViewX.unbind();
      localViewY.unbind();
    }

    Dune::PDELab::constrain_residual(cg_, y);

    auto stop = Dune::PDELab::getWallTime();
    if(verbosity_ >= 2)
      std::cout << "jacobian_apply() took " << (stop - start) << "s"
                << std::endl;
    jacobianApplyStats_.addSample(toDouble(stop-start));
  }

  ~SimpleGridOperator()
  {
    if(verbosity_ >= 1)
      std::cout << "residual() timings[s]: " << residualStats_ << std::endl
                << "jacobian_apply() timings[s]: " << jacobianApplyStats_
                << std::endl;
  }
};

//===============================================================
// Problem setup and solution
//===============================================================

// generate a P1 function and output it
template<typename GV, typename FEM, typename CON>
bool poisson (const GV& gv, const FEM& fem, std::string filename, int q,
              bool doOutput, unsigned verbosity, unsigned subRefines)
{
  // constants and types
  typedef typename FEM::Traits::FiniteElementType::Traits::
    LocalBasisType::Traits::RangeFieldType R;

  // make function space
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,
    Dune::PDELab::istl::VectorBackend<> > GFS;
  GFS gfs(gv,fem);
  gfs.name("solution");

  // make constraints map and initialize it from a function
  typedef typename GFS::template ConstraintsContainer<R>::Type C;
  C cg;
  cg.clear();
  ConstraintsParameters constraintsparameters;
  Dune::PDELab::constraints(constraintsparameters,gfs,cg);

  // make local operator
  typedef G<GV,R> GType;
  GType g(gv);
  typedef F<R> FType;
  FType f;
  typedef J<GV,R> JType;
  JType j(gv);
  typedef Dune::Patches::Poisson<FType,ConstraintsParameters,JType,
                                 (1 << GV::dimension)> LOP;
  LOP lop(f,constraintsparameters,j, q);

  // make grid operator
  using GridOperator = SimpleGridOperator<GFS, LOP, C, double>;
  GridOperator gridoperator(gfs,cg,lop,verbosity);
  gridoperator.setSubRefines(subRefines);

  // make coefficent Vector and initialize it from a function
  typedef typename GridOperator::Traits::Domain DV;
  DV x0(gfs);
  x0 = 0; //Dune::PDELab::interpolate(g,gfs,x0);
  Dune::PDELab::set_nonconstrained_dofs(cg,0.0,x0);

  // evaluate residual w.r.t initial guess
  typedef typename GridOperator::Traits::Range RV;
  RV r(gfs);
  r = 0.0;
  gridoperator.residual(x0,r);

  // make ISTL solver
  typedef Dune::PDELab::OnTheFlyOperator<DV,RV,
                                         GridOperator> ISTLOnTheFlyOperator;
  ISTLOnTheFlyOperator opb(gridoperator);
  Dune::Richardson<DV,RV> richardsonb(1.0);

  Dune::CGSolver<DV> solverb(opb,richardsonb,1E-10,5000,verbosity);
  Dune::InverseOperatorResult stat;

  std::cout << "Solve with on-the-fly assembly" << std::endl;
  DV xb(gfs,0.0);
  {
    RV rb = r;
    rb *= -1.0; // need -residual
    solverb.apply(xb,rb,stat);
  }
  if(!stat.converged)
  {
    std::cerr << "Error: on-the-fly solver did not converge" << std::endl;
    return false;
  }
  xb += x0;

  // output grid function with VTKWriter
  if(doOutput)
  {
    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,subRefines);
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,xb,
        Dune::PDELab::vtk::DefaultFunctionNameGenerator("on_the_fly"));
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,r,
        Dune::PDELab::vtk::DefaultFunctionNameGenerator("r"));
    vtkwriter.write(filename,Dune::VTK::ascii);
  }

  return true;
}

//===============================================================
// Main program with grid setup
//===============================================================

template<unsigned dim>
bool testYaspGrid(unsigned gridDiv, unsigned subRefines, bool doOutput,
                  unsigned verbosity)
{
  using std::to_string;

  std::cout << "#### Testing YaspGrid Q1 " << dim << "D" << std::endl;

  // make grid
  Dune::FieldVector<double,dim> L(1.0);
  std::array<int,dim> N(Dune::fill_array<int,dim>(gridDiv));
  Dune::YaspGrid<dim> grid(L,N);

  // get view
  typedef typename Dune::YaspGrid<dim>::LeafGridView GV;
  const GV& gv=grid.leafGridView();

  // make finite element map
  typedef typename GV::Grid::ctype DF;
  typedef Dune::Patches::RefinedQ1LocalFiniteElementMap<DF,double,dim> FEM;
  FEM fem(1 << subRefines);

  // solve problem
  return poisson<GV,FEM,Dune::PDELab::ConformingDirichletConstraints>
    (gv,fem,"poisson-single-Q1-"+to_string(dim)+"d",2,doOutput, verbosity,
     subRefines);
}

int main(int argc, char** argv)
{
  using Dune::Patches::fromEnv;
  using Dune::Patches::UnitTest::acc;

  try{
    //Maybe initialize Mpi
    Dune::MPIHelper::instance(argc, argv);

    int result = 77;

    std::cout << "Using plain YaspGrid" << std::endl;
    std::cout << "Using wall clock implementation "
              << Dune::PDELab::getWallTimeImp() << " (advertised resolution: "
              << Dune::PDELab::getWallTimeResolution() << "s)" << std::endl;

    auto gridDiv = fromEnv("GRID_DIV", 4u);
    auto subRefines = fromEnv("REFINES", 3u);
    auto doOutput = fromEnv("DO_OUTPUT", true);
    auto verbosity = fromEnv("VERBOSITY", 1u);

    acc(result, testYaspGrid<2>(gridDiv, subRefines, doOutput, verbosity));
    // acc(result, testYaspGrid<3>(gridDiv, subRefines, doOutput, verbosity));

    // test passed
    return result;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    throw;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    throw;
  }
}
