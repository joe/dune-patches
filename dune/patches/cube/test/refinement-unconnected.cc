#include <config.h>

#include <cmath>
#include <cstddef>
#include <exception>
#include <limits>
#include <iostream>
#include <ostream>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/patches/cube/unconnected/patch.hh>
#include <dune/patches/meshes/tensorproduct.hh>
#include <dune/patches/utility/range.hh>

int main(int argc, char **argv)
{
  try {
    using std::sqrt;

    Dune::MPIHelper::instance(argc, argv);

    int result = 0;

    static const int dim = 2;             /*@\label{fem:dim}@*/
    typedef double ctype;
    typedef Dune::Patches::Cube::Unconnected::Patch<ctype, dim> Patch;

    ctype rho = sqrt(std::numeric_limits<ctype>::epsilon());

    Dune::FieldVector<ctype,dim> lower(0);
    Dune::FieldVector<ctype,dim> upper(1);

    auto patchp = Dune::Patches::Meshes::tensorProduct<Patch>
      (lower, upper, Dune::FieldVector<Patch::Index, dim>(1));

    unsigned level = 3;
    unsigned div = 1 << level;
    ctype h = 1.0 / div;

    const auto &levelView = patchp->levelGridView(level);
    const auto &levelIS = levelView.indexSet();

    std::cout << "Checking element center positions..." << std::endl;

    for(const auto &pe : elements(levelView))
    {
      std::size_t levelIndex = levelIS.index(pe);

      auto expCenter = lower;
      auto tmp = levelIndex;
      for(auto &p : expCenter)
      {
        p += (tmp % div + 0.5) * h;
        tmp /= div;
      }
      auto actCenter = pe.geometry().center();

      if((expCenter-actCenter).two_norm() > rho)
      {
        result = 1;
        std::cerr << "Element center mismatch: levelIndex=" << levelIndex
                  << " expected=" << expCenter << " actual=" << actCenter
                  << std::endl;
      }
    }

    std::cout << "Checking vertex positions..." << std::endl;

    for(const auto &pv : vertices(levelView))
    {
      std::size_t levelIndex = levelIS.index(pv);

      auto expPos = lower;
      auto tmp = levelIndex;
      for(auto &p : expPos)
      {
        p += (tmp % (div + 1)) * h;
        tmp /= (div + 1);
      }
      auto actPos = pv.geometry().corner(0);

      if((expPos-actPos).two_norm() > rho)
      {
        result = 1;
        std::cerr << "Vertex position mismatch: levelIndex=" << levelIndex
                  << " expected=" << expPos << " actual=" << actPos
                  << std::endl;
      }
    }

    std::cout << "Checking subIndex against index(subEntity)..." << std::endl;

    for(const auto &pe : elements(levelView))
    {
      std::size_t levelIndex = levelIS.index(pe);

      using Dune::Patches::range;
      for(auto c : range(1 << dim))
      {
        auto directIndex = levelIS.subIndex(pe, c, dim);
        auto indirectIndex = levelIS.index(pe.template subEntity<dim>(c));
        if(directIndex != indirectIndex)
        {
          result = 1;
          std::cerr << "Subindex mismatch: levelIndex=" << levelIndex
                    << " corner=" << c
                    << " subIndex(pe, corner, dim)=" << directIndex
                    << " index(pe.subEntity<dim>(corner))=" << indirectIndex
                    << std::endl;
        }
      }
    }

    std::cout << "Checking element corner position against subEntity "
              << "position..." << std::endl;

    for(const auto &pe : elements(levelView))
    {
      std::size_t elementIndex = levelIS.index(pe);
      const auto &geo = pe.geometry();

      using Dune::Patches::range;
      for(auto c : range(1 << dim))
      {
        const auto &cornerPos = geo.corner(c);
        const auto &subEntityPos =
          pe.template subEntity<dim>(c).geometry().corner(0);
        if((cornerPos - subEntityPos).two_norm() > rho)
        {
          result = 1;
          std::cerr << "Corner position mismatch: elementIndex="
                    << elementIndex << " corner=" << c << " cornerPos="
                    << cornerPos << " subEntityPos=" << subEntityPos
                    << std::endl;
        }
      }
    }

    std::cout << "Checking subIndex..." << std::endl;

    for(const auto &pe : elements(levelView))
    {
      std::size_t elementIndex = levelIS.index(pe);

      Dune::FieldVector<std::size_t, dim> elemPos;
      auto tmp = elementIndex;
      for(auto &p : elemPos)
      {
        p = tmp % div;
        tmp /= div;
      }

      using Dune::Patches::range;
      for(auto c : range(1 << dim))
      {
        std::size_t expVertexIndex = 0;
        std::size_t base = 1;
        for(auto d : range(dim))
        {
          expVertexIndex += base * (elemPos[d] + (c >> d) % 2);
          base *= (div + 1);
        }

        auto actVertexIndex = levelIS.subIndex(pe, c, dim);
        if(expVertexIndex != actVertexIndex)
        {
          result = 1;
          std::cerr << "Unexpected corner index: elementIndex="
                    << elementIndex << " corner=" << c << " expected="
                    << expVertexIndex << " actual=" << actVertexIndex
                    << std::endl;
        }
      }
    }

    return result;
  }
  catch(const Dune::Exception &e) {
    std::cerr << "Dune::Exception.what()=" << e.what() << std::endl;
    throw;
  }
  catch(const std::exception &e) {
    std::cerr << "std::exception.what()=" << e.what() << std::endl;
    throw;
  }
}
