#include <config.h>

#include <cstddef>
#include <exception>
#include <iostream>
#include <ostream>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/patches/cube/unconnected/patch.hh>
#include <dune/patches/meshes/tensorproduct.hh>
#include <dune/patches/utility/range.hh>

int main(int argc, char **argv)
{
  try {
    Dune::MPIHelper::instance(argc, argv);

    int result = 0;

    static const int dim = 2;             /*@\label{fem:dim}@*/
    typedef double ctype;
    typedef Dune::Patches::Cube::Unconnected::Patch<ctype, dim> Patch;

    auto patchp = Dune::Patches::Meshes::tensorProduct<Patch>
      ( Dune::FieldVector<ctype,dim>(0), Dune::FieldVector<ctype,dim>(1),
        Dune::FieldVector<Patch::Index, dim>(10));

    typedef Patch::MacroGridView MacroGV;
    typedef Patch::LevelGridView LevelGV;
    const MacroGV &macroView = patchp->macroGridView();
    const LevelGV &levelView = patchp->levelGridView(0);

    typedef MacroGV::IndexSet MacroIS;
    typedef LevelGV::IndexSet LevelIS;
    const MacroIS &macroIS = macroView.indexSet();
    const LevelIS &levelIS = levelView.indexSet();

    typedef MacroGV::Codim<0>::Iterator MacroIt;
    typedef LevelGV::Codim<0>::Iterator LevelIt;
    MacroIt macroIt = macroView.begin<0>();
    LevelIt levelIt = levelView.begin<0>();
    MacroIt macroEnd = macroView.end<0>();
    LevelIt levelEnd = levelView.end<0>();

    for(std::size_t runningIndex = 0;
        macroIt != macroEnd && levelIt != levelEnd;
        ++runningIndex, ++macroIt, ++levelIt)
    {
      std::size_t macroIndex = macroIS.index(*macroIt);
      std::size_t levelIndex = levelIS.index(*levelIt);

      if(macroIndex != runningIndex || levelIndex != runningIndex)
      {
        result = 1;
        std::cerr << "Index mismatch: running=" << runningIndex << " macro="
                  << macroIndex << " level=" << levelIndex << std::endl;
      }

      if(macroIt->type() == levelIt->type())
      {
        const auto &ref =
          Dune::ReferenceElements<ctype, dim>::general(macroIt->type());
        for(auto c : Dune::Patches::range(ref.size(dim)))
        {
          std::size_t macroSubIndex = macroIS.subIndex(*macroIt, c, dim);
          std::size_t levelSubIndex = levelIS.subIndex(*levelIt, c, dim);

          if(macroSubIndex != levelSubIndex)
          {
            result = 1;
            std::cerr << "SubIndex mismatch for (e=" << runningIndex << ", c="
                      << c << "): macro=" << macroSubIndex << " level="
                      << levelSubIndex << std::endl;
          }
        }
      }
      else // macroIt->type() 1= levelIt->type()
      {
        result = 1;
        std::cerr << "GeometryType mismatch: macro=" << macroIt->type()
                  << " level=" << levelIt->type() << std::endl;
      }
    }
    return result;
  }
  catch(const Dune::Exception &e) {
    std::cerr << "Dune::Exception.what()=" << e.what() << std::endl;
    throw;
  }
  catch(const std::exception &e) {
    std::cerr << "std::exception.what()=" << e.what() << std::endl;
    throw;
  }
}
