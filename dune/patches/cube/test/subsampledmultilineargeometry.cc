// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <config.h>

#include <array>
#include <cassert>
#include <cmath>   // abs()
#include <cstddef>
#include <cstdlib> // also abs()
#include <iostream>
#include <limits>
#include <ostream>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/unused.hh>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/test/checkgeometry.hh>

#include <dune/patches/cube/geometry.hh>
#include <dune/patches/utility/range.hh>


template< class ctype, int mydim, int cdim >
static Dune::FieldVector< ctype, cdim >
map ( const Dune::FieldMatrix< ctype, mydim, mydim > &A,
      const Dune::FieldMatrix< ctype, cdim, cdim > &B,
      const Dune::FieldVector< ctype, mydim > &x )
{
  // compute Ax
  Dune::FieldVector< ctype, mydim > Ax;
  A.mv( x, Ax );

  // embed Ax into the larger space (eAx)
  Dune::FieldVector< ctype, cdim > eAx( 0 );
  for( int j = 0; j < mydim; ++j )
    eAx[ j ] = Ax[ j ];

  // combute y = B eAx
  Dune::FieldVector< ctype, cdim > y;
  B.mv( eAx, y );
  return y;
}


template< class ctype, int mydim, int cdim >
static bool testSubsampledMultiLinearGeometry
  ( const Dune::FieldMatrix< ctype, mydim, mydim > &A,
    const Dune::FieldMatrix< ctype, cdim, cdim > &B )
{
  using std::abs;
  using Dune::Patches::range;
  static const auto &refElement =
    Dune::ReferenceElements<ctype, mydim>::cube();

  bool pass = true;

  using Geometry =
    Dune::Patches::Cube::SubsampledMultiLinearGeometry< ctype, mydim, cdim >;

  Dune::FieldVector< ctype, mydim > localCenter(0.5);
  const ctype epsilon = ctype( 1e5 )*std::numeric_limits< ctype >::epsilon();

  const ctype detA = A.determinant();
  assert( abs( abs( B.determinant() ) - ctype( 1 ) ) <= epsilon );

  constexpr int numCorners = 1 << mydim;
  assert(numCorners == refElement.size(mydim));

  std::array< Dune::FieldVector< ctype, cdim >, numCorners > corners;
  for( int i = 0; i < numCorners; ++i )
    corners[ i ] = map( A, B, refElement.position( i, mydim ) );

  Geometry geometry( corners );

  if( !geometry.affine() )
  {
    std::cerr << "Error: Affine returns false for an affine geometry."
              << std::endl;
    pass = false;
  }

  const ctype integrationElement = geometry.integrationElement( localCenter );
  if( abs( integrationElement - abs( detA ) ) > epsilon )
  {
    std::cerr << "Error: Wrong integration element (" << integrationElement
              << ", should be " << abs( detA ) << ")." << std::endl;
    pass = false;
  }
  const ctype volume = geometry.volume();
  if( abs( volume - refElement.volume()*abs( detA ) ) > epsilon )
  {
    std::cerr << "Error: Wrong volume (" << volume
              << ", should be " << (refElement.volume()*abs( detA ))
              << ")." << std::endl;
    pass = false;
  }

  const Dune::FieldVector< ctype, cdim > center = geometry.center();
  if( (center - map( A, B, refElement.position( 0, 0 ) )).two_norm() > epsilon )
  {
    std::cerr << "Error: wrong barycenter (" << center << ")." << std::endl;
    pass = false;
  }

  for (int c = 0; c < numCorners; ++c) {
    Dune::FieldVector<ctype, mydim> local(refElement.position(c, mydim));
    Dune::FieldVector<ctype, cdim> global(geometry.global(local));
    Dune::FieldVector<ctype, mydim> local2(geometry.local(global));
    if (local2 != local2) {
      std::cerr << "Error: at corner " << c << " local returned NaN: "
                << local2 << std::endl;
      pass = false;
    }
    if ((local - local2).two_norm() > epsilon) {
      std::cerr << "Error: at corner " << c << " local returned wrong value: "
                << local2 << " (expected: " << local << ")" << std::endl;
      pass = false;
    }
  }

  const Dune::FieldMatrix< ctype, mydim, cdim > JT =
    geometry.jacobianTransposed( localCenter );
  for( int i = 0; i < mydim; ++i )
  {
    Dune::FieldVector< ctype, mydim > e( ctype( 0 ) );
    e[ i ] = ctype( 1 );
    const Dune::FieldVector< ctype, cdim > t = map( A, B, e );
    if (JT[i] != JT[i])
    {
      std::cerr << "Error: jacobianTransposed[" << i << "] (" << JT[i]
                << ") has NaN entry." << std::endl;
      pass = false;
    }
    if( (t - JT[ i ]).two_norm() > epsilon )
    {
      std::cerr << "Error: wrong jacobianTransposed[ " << i << " ] ("
                << JT[ i ] << ", should be " << t << ")." << std::endl;
      pass = false;
    }
  }

  for (int c = 0; c < numCorners; ++c) {
    const Dune::FieldMatrix< ctype, mydim, cdim > JT =
      geometry.jacobianTransposed(refElement.position(c, mydim));
    for( int i = 0; i < mydim; ++i ) {
      Dune::FieldVector< ctype, mydim > e( ctype( 0 ) );
      e[ i ] = ctype( 1 );
      const Dune::FieldVector< ctype, cdim > t = map( A, B, e );
      if (JT[i] != JT[i])
      {
        std::cerr << "Error: at corner " << c << ": jacobianTransposed["
                  << i << "] (" << JT[i] << ") has NaN entry." << std::endl;
        pass = false;
      }
      if( (t - JT[ i ]).two_norm() > epsilon )
      {
        std::cerr << "Error: at corner " << c << ": wrong jacobianTransposed[ "
                  << i << " ] (" << JT[ i ] << ", should be " << t << ")."
                  << std::endl;
        pass = false;
      }
    }
  }

  for(int div = 1; div <= 4; ++div)
  {
    int subentities = 0;
    for(auto DUNE_UNUSED d : range(mydim))
      subentities *= div;
    std::cout << "  Testing with div=" << div << "..." << std::endl;
    geometry.div(div);
    bool divpass = true;
    for(auto subindex : range(subentities))
    {
      geometry.subindex(subindex);
      divpass &= checkGeometry( geometry );
    }
    std::cout << "  Testing with div=" << div << "... "
              << (divpass ? "passed" : "failed") << std::endl;
    pass &= divpass;
  }

  return pass;
}


template< class ctype, int mydim, int cdim >
static bool testSubsampledMultiLinearGeometry ( )
{
  Dune::FieldMatrix< ctype, mydim, mydim > A;
  Dune::FieldMatrix< ctype, cdim, cdim > B;

  std::cout << "Checking geometry (cube, mydim = " << mydim << ", cdim = "
            << cdim << ")" << std::endl;
  std::cout << " reference mapping..." << std::endl;
  A = ctype( 0 );
  for( int i = 0; i < mydim; ++i )
    A[ i ][ i ] = ctype( 1 );
  B = ctype( 0 );
  for( int i = 0; i < cdim; ++i )
    B[ i ][ i ] = ctype( 1 );
  const bool passId = testSubsampledMultiLinearGeometry( A, B );
  std::cout << " reference mapping... " << (passId ? "passed" : "failed")
            << std::endl;

  std::cout << " scaled reference mapping..." << std::endl;
  A = ctype( 0 );
  for( int i = 0; i < mydim; ++i )
    A[ i ][ i ] = ctype( 42 );
  B = ctype( 0 );
  for( int i = 0; i < cdim; ++i )
    B[ i ][ i ] = ctype( 1 );
  const bool passScaledId = testSubsampledMultiLinearGeometry( A, B );
  std::cout << " scaled reference mapping... "
            << (passScaledId ? "passed" : "failed") << std::endl;

  return passId && passScaledId;
}

template<class ctype>
static bool testNonLinearGeometry()
{
  const unsigned dim = 2;
  typedef Dune::ReferenceElement<ctype,dim> ReferenceElement;
  typedef Dune::FieldVector<ctype,dim> Vector;
  using Geometry =
    Dune::Patches::Cube::SubsampledMultiLinearGeometry<ctype,dim,dim>;
  const ctype epsilon(ctype(16) * std::numeric_limits<ctype>::epsilon());

  bool pass(true);
  std::cout << "Checking geometry (non-linear, quadrilateral): ";

  const ReferenceElement& reference(Dune::ReferenceElements<ctype,dim>::cube());

  std::array<Vector, 4> corners;
  corners[0][0] = 0; corners[0][1] = 0;
  corners[1][0] = 2; corners[1][1] = 0;
  corners[2][0] = 0; corners[2][1] = 1;
  corners[3][0] = 1; corners[3][1] = 1;

  const Geometry geometry(corners);

  /* Test global() */
  for (std::size_t c = 0; c < corners.size(); ++c) {
    const Vector& local(reference.position(c, dim));
    const Vector global(geometry.global(local));
    if (global != global) {
      std::cerr << "global failed at corner " << c << ": returned NaN: "
                << global << std::endl;
      pass = false;
    }
    if ((global - corners[c]).two_norm() > epsilon) {
      std::cerr << "global failed at corner " << c << ": got " << global
                << ", but expected " << corners[c] << std::endl;
      pass = false;
    }
  }

  /* Test global() outside reference element */
  {
    Vector local; local[0] = -1; local[1] = 0;
    Vector global; global[0] = -2; global[1] = 0;
    const Vector global2(geometry.global(local));
    if (global2 != global2) {
      std::cerr << "global failed outside reference element: returned NaN: "
                << global2 << std::endl;
      pass = false;
    }
    if ((global - global2).two_norm() > epsilon) {
      std::cerr << "global failed outside reference element: got " << global2
                << ", but expected " << global << std::endl;
      pass = false;
    }
  }

  /* Test local() */
  for (std::size_t c = 0; c < corners.size(); ++c) {
    const Vector& local(reference.position(c, dim));
    const Vector local2(geometry.local(corners[c]));
    if (local2 != local2) {
      std::cerr << "local failed at corner " << c << ": returned NaN: "
                << local2 << std::endl;
      pass = false;
    }
    if ((local - local2).two_norm() > epsilon) {
      std::cerr << "local failed at corner " << c << ": got " << local2
                << ", but expected " << local << std::endl;
      pass = false;
    }
  }

  /* Test local() outside reference element */
  {
    Vector global; global[0] = -2; global[1] = 0;
    Vector local; local[0] = -1; local[1] = 0;
    const Vector local2(geometry.local(global));
    if (local2 != local2) {
      std::cerr << "local failed outside reference element: returned NaN: "
                << local2 << std::endl;
      pass = false;
    }
    if ((local - local2).two_norm() > epsilon) {
      std::cerr << "local failed outside reference element: got " << local2
                << ", but expected " << local << std::endl;
      pass = false;
    }
  }

  std::cout << (pass ? "passed" : "failed") << std::endl;
  return pass;
}

template< class ctype >
static bool testSubsampledMultiLinearGeometry ()
{
  bool pass = true;

  pass &= testSubsampledMultiLinearGeometry< ctype, 0, 0 >( );
  pass &= testSubsampledMultiLinearGeometry< ctype, 0, 1 >( );
  pass &= testSubsampledMultiLinearGeometry< ctype, 0, 2 >( );
  pass &= testSubsampledMultiLinearGeometry< ctype, 0, 3 >( );
  pass &= testSubsampledMultiLinearGeometry< ctype, 0, 4 >( );

  pass &= testSubsampledMultiLinearGeometry< ctype, 1, 3 >( );
  pass &= testSubsampledMultiLinearGeometry< ctype, 1, 1 >( );
  pass &= testSubsampledMultiLinearGeometry< ctype, 1, 2 >( );
  pass &= testSubsampledMultiLinearGeometry< ctype, 1, 4 >( );

  pass &= testSubsampledMultiLinearGeometry< ctype, 2, 2 >( );
  pass &= testSubsampledMultiLinearGeometry< ctype, 2, 3 >( );
  pass &= testSubsampledMultiLinearGeometry< ctype, 2, 4 >( );

  pass &= testSubsampledMultiLinearGeometry< ctype, 3, 3 >( );
  pass &= testSubsampledMultiLinearGeometry< ctype, 3, 4 >( );

  pass &= testSubsampledMultiLinearGeometry< ctype, 4, 4 >( );
  pass &= testSubsampledMultiLinearGeometry< ctype, 4, 5 >( );

  pass &= testNonLinearGeometry<ctype>();

  return pass;
}

int main ( int argc, char **argv )
{
  Dune::MPIHelper::instance(argc, argv);

  bool pass = true;

  std::cout << ">>> Checking ctype = double" << std::endl;
  pass &= testSubsampledMultiLinearGeometry< double >();
  //std::cout << ">>> Checking ctype = float" << std::endl;
  //pass &= testSubsampledMultiLinearGeometry< float >();

  return (pass ? 0 : 1);
}
