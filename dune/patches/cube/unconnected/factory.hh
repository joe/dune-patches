#ifndef DUNE_PATCHES_CUBE_UNCONNECTED_FACTORY_HH
#define DUNE_PATCHES_CUBE_UNCONNECTED_FACTORY_HH

#include <cassert>
#include <cstddef>
#include <memory>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>

#include <dune/grid/common/boundarysegment.hh>
#include <dune/grid/common/gridfactory.hh>

#include <dune/patches/cube/unconnected/patch.hh>

namespace Dune {

  template<class ct, unsigned d, std::size_t align, std::size_t internalAlign>
  class GridFactory<Patches::Cube::Unconnected::
                    Patch<ct, d, align, internalAlign> >
  {
    using Patch =
      Patches::Cube::Unconnected::Patch<ct, d, align, internalAlign>;
    using BoundaryIntersection =
      typename Patch::MacroGridView::BoundaryIntersection;

    std::vector<FieldVector<ct, d> > coordinates_;
    std::vector<FieldVector<ct, d> > flattenedCoordinates_;
    std::vector<typename Patch::Index> vertexIndices_;
    typename Patch::MacroGridView::IndexSet indexSet_;

  public:
    template<unsigned codim>
    using Codim = typename Patch::MacroGridView::template Codim<codim>;

    /** \brief Insert a vertex into the coarse grid */
    void insertVertex(const FieldVector<ct,d>& pos)
    {
      coordinates_.push_back(pos);
    }

    /** \brief Insert an element into the coarse grid
        \param type The GeometryType of the new element
        \param vertices The vertices of the new element, using the DUNE numbering

        Make sure the inserted element is not inverted (this holds even
        for simplices).  There are grids that can't handle inverted elements.
     */
    void insertElement(const GeometryType& type,
                       const std::vector<unsigned int>& vertices)
    {
      assert(type.isCube());
      assert(type.dim() == d);
      assert(vertices.size() == (1 << d));
      for(auto v : vertices)
      {
        vertexIndices_.push_back(v);
        flattenedCoordinates_.push_back(coordinates_[v]);
      }
    }

    /** \brief insert a boundary segment
     *
     *  This method inserts a boundary segment into the coarse grid. Using
     *  this method has two advantages over not using it:
     *  - The boundary segment gets an insertion index.
     *  - The grid factory can verify that this is actually a boundary segment
     *  .
     *
     *  \note You are not forced to insert all boundary segments. The grid
     *        factory will find the remaining boundary segments itself.
     *
     *  \param[in]  vertices  the indices of the vertices of the segment
     */
    void insertBoundarySegment(const std::vector<unsigned int>& vertices)
    {
      DUNE_THROW(GridError,
                 "This patch does not support inserting boundary segments!");
    }

    /** \brief insert an arbitrarily shaped boundary segment
     *
     *  This method inserts a boundary segment into the coarse grid.
     *
     *  \param[in]  vertices         the indices of the vertices of the segment
     *  \param[in]  boundarySegment  user defined implementation of the boundary segment's geometry
     */
    void insertBoundarySegment
    ( const std::vector<unsigned int>& vertices,
      const std::shared_ptr<BoundarySegment<d,d> >& boundarySegment)
    {
      DUNE_THROW(GridError,
                 "This patch does not support inserting boundary segments!");
    }

    /** \brief Finalize grid creation and hand over the grid

       If an exception is thrown, the state of the factory object is
       unspecified (the only valid operation is destruction).
     */
    std::unique_ptr<Patch> createGrid()
    {
      // release memory for coordinates
      std::vector<FieldVector<ct, d> >().swap(coordinates_);
      auto ptr = Patch::create(flattenedCoordinates_);
      if(ptr)
      {
        indexSet_ = ptr->macroGridView().indexSet();
        // release memory for flattenedCoordinates
        std::vector<FieldVector<ct, d> >().swap(flattenedCoordinates_);
      }
      return ptr;
    }

    /** \brief obtain an element's insertion index
     *
     *  Data can be associated to the created macro grid using the insertion
     *  index of each entity that has been inserted during the grid creation
     *  process.
     *
     *  Between grid construction (createGrid) and the first grid
     *  modification, this method allows to obtain this insertion index from
     *  the grid factory. This way, data can be stored using the index maps
     *  provided by the grid.
     *
     *  \param[in]  entity  entity whose insertion index is requested
     *
     *  \returns insertion index of the entity
     */
    unsigned insertionIndex ( const typename Codim< 0 >::Entity &entity ) const
    {
      return indexSet_.index(entity);
    }

    /** \brief obtain a vertex' insertion index
     *
     *  Data can be associated to the created macro grid using the insertion
     *  index of each entity that has been inserted during the grid creation
     *  process.
     *
     *  Between grid construction (createGrid) and the first grid
     *  modification, this method allows to obtain this insertion index from
     *  the grid factory. This way, data can be stored using the index maps
     *  provided by the grid.
     *
     *  \param[in]  entity  entity whose insertion index is requested
     *
     *  \returns insertion index of the entity
     */
    unsigned insertionIndex ( const typename Codim< d >::Entity &entity ) const
    {
      return vertexIndices_[indexSet_.index(entity)];
    }

    /** \brief determine whether an intersection was inserted
     *
     *  This method allows checking wheter an intersection was actually
     *  inserted into the grid factory.
     *
     *  \note Not all boundary segments need to be inserted into the grid
     *        factory.
     *  \note This method returns \b false for all interior intersections
     *
     *  \param[in]  intersection  intersection in question
     *
     *  \returns \b true, if the intersection was inserted
     */
    bool wasInserted ( const BoundaryIntersection &intersection ) const
    {
      return false;
    }

  };

} // namespace Dune

#endif // DUNE_PATCHES_CUBE_UNCONNECTED_FACTORY_HH
