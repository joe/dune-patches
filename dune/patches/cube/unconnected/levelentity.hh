#ifndef DUNE_PATCHES_CUBE_UNCONNECTED_LEVELENTITY_HH
#define DUNE_PATCHES_CUBE_UNCONNECTED_LEVELENTITY_HH

#include <array>
#include <cassert>
#include <iterator>
#include <type_traits>

#if Vc_FOUND
#include <Vc/Vc>
#endif // Vc_FOUND

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/unused.hh>

#include <dune/geometry/axisalignedcubegeometry.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include <dune/patches/cube/geometry.hh>
#include <dune/patches/utility/cmp.hh>
#include <dune/patches/utility/math.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {
      namespace Unconnected {

        template<class Patch, unsigned cd, class = void>
        class LevelEntity;

        template<class Patch>
        class LevelEntity<Patch, 0>
        {
          const Patch *patch_;
          typedef typename Patch::Index Index;
          Index index_;
          unsigned level_;

        protected:
          Index &iteratorIndex()       { return index_; }
          Index  iteratorIndex() const { return index_; }

        public:
          enum { codimension = 0 };
          enum { dimension = Patch::dimension };
          enum { mydimension = dimension - codimension };

          LevelEntity() = default;
          LevelEntity(const Patch *patch, unsigned level, Index index) :
            patch_(patch), index_(index), level_(level)
          { }

          using Geometry =
            Cube::Geometry<typename Patch::ctype, mydimension, dimension>;
          Geometry geometry() const
          {
            typedef typename Patch::ctype ct;

            auto n = 1u << level_;
            auto subentities = ipow(Index(n), Index(dimension));
            auto macroIndex = index_ / subentities;
            auto subIndex = index_ - macroIndex;

            constexpr auto vertices = 1 << dimension;

            std::array<FieldVector<ct, dimension>, vertices> corners;
            for(auto v : range(vertices))
              corners[v] =
                patch_->vertex(patch_->entityVertex(0, macroIndex, v));
            return { corners, n, subIndex };
          }

          static const GeometryType &type()
          {
            return Patch::types(codimension)[0];
          }

          unsigned subEntities(unsigned codim) const
          {
            static const auto &refelem =
              ReferenceElements<typename Patch::ctype,dimension>::cube();
            return refelem.size(codim);
          }
          template<unsigned codim>
          typename std::enable_if<codim==dimension,
                                  LevelEntity<Patch, codim> >::type
          subEntity(Index n) const
          {
            assert(n < subEntities(codim));
            return { patch_, level_, subVertex(n) };
          }
          template<unsigned codim>
          typename std::enable_if<codim==0, const LevelEntity &>::type
          subEntity(Index n) const
          {
            assert(n == 0);
            return *this;
          }

        private:
          Index subVertex(unsigned v) const
          {
            auto n = 1 << level_;
            auto subelements = ipow(Index(n), Index(dimension));
            auto localElemIndex = index_ % subelements;
            auto macroElemIndex = index_ / subelements;

            Index localVertexIndex = 0;
            Index localVertexBase = 1;

            for(auto DUNE_UNUSED d : range(dimension))
            {
              localVertexIndex +=
                localVertexBase * (localElemIndex % n + v % 2);
              localElemIndex /= n;
              v /= 2;
              localVertexBase *= (n+1);
            }

            return macroElemIndex * localVertexBase + localVertexIndex;
          }

        public:
          Index index() const { return index_; }
          Index subIndex(unsigned i, unsigned codim) const
          {
            switch(codim) {
            case 0:
              assert(i == 0);
              return index();
            case dimension:
              return subVertex(i);
            default:
              DUNE_THROW(NotImplemented,
                         "subindices for codimension " << codim);
            }
          }
        };

        template<class Patch, unsigned codim>
        class LevelEntity<Patch, codim,
                          typename std::enable_if<Patch::dimension == codim>
                          ::type>
        {
          const Patch *patch_;
          typedef typename Patch::Index Index;
          Index index_;
          unsigned level_;

        protected:
          Index &iteratorIndex()       { return index_; }
          Index  iteratorIndex() const { return index_; }

          friend LevelEntity<Patch, 0>;
          LevelEntity(const Patch *patch, unsigned level, Index index) :
            patch_(patch), index_(index), level_(level)
          { }

        public:
          enum { codimension = codim };
          enum { dimension = Patch::dimension };
          enum { mydimension = 0 };

          LevelEntity() = default;

          using Geometry =
            Dune::AxisAlignedCubeGeometry<typename Patch::ctype,
                                          mydimension, dimension>;
          Geometry geometry() const
          {
            typedef typename Patch::ctype ct;

            auto n = 1u << level_;
            auto subVertices = ipow(n + 1, unsigned(dimension));
            auto macroElemIndex = index_ / subVertices;
            auto subIndex = index_ - macroElemIndex;

            Dune::FieldVector<ct, dimension> local;
            for(auto &c : local)
            {
              c = ct(subIndex % (n + 1)) / n;
              subIndex /= (n + 1);
            }

            return { patch_->macroGridView().
                template begin<0>()[macroElemIndex].geometry().global(local) };
          }
          static const GeometryType &type()
          {
            return Patch::LevelGridView::IndexSet::types(codimension)[0];
          }

          Index index() const { return index_; }
        };

#if Vc_FOUND
        template<class Patch, unsigned cd, unsigned lanes, class = void>
        class VcLevelEntity;

        template<class Patch, unsigned lanes>
        class VcLevelEntity<Patch, 0, lanes>
        {
          const Patch *patch_;
          typedef typename Patch::Index Index;
          Index chunkIndex_;
          unsigned level_;
          using simdCtype = Vc::SimdArray<typename Patch::ctype, lanes>;

        protected:
          Index &iteratorIndex()       { return chunkIndex_; }
          Index  iteratorIndex() const { return chunkIndex_; }

        public:
          enum { codimension = 0 };
          enum { dimension = Patch::dimension };
          enum { mydimension = dimension - codimension };

          VcLevelEntity() = default;
          VcLevelEntity(const Patch *patch, unsigned level, Index chunkIndex) :
            patch_(patch), chunkIndex_(chunkIndex), level_(level)
          { }

          using Geometry = Cube::Geometry<simdCtype, mydimension, dimension>;
          Geometry geometry() const
          {
            auto n = 1u << level_;
            auto subentities = ipow(Index(n), Index(dimension));
            auto macroChunkIndex = chunkIndex_ / subentities;
            auto subIndex = chunkIndex_ - macroChunkIndex;

            constexpr auto vertices = 1 << dimension;

            std::array<FieldVector<simdCtype, dimension>, vertices> corners;
            for(auto v : range(vertices))
              patch_->loadVertex(corners[v],
                                 patch_->template entityVertexChunk<lanes>
                                   (0, macroChunkIndex, v));
            return { corners, n, subIndex };
          }

          static const GeometryType &type()
          {
            return Patch::types(codimension)[0];
          }

          unsigned subEntities(unsigned codim) const
          {
            static const auto &refelem =
              ReferenceElements<typename Patch::ctype,dimension>::cube();
            return refelem.size(codim);
          }
          template<unsigned codim>
          typename std::enable_if<codim==dimension,
                                  VcLevelEntity<Patch, codim, lanes> >::type
          subEntity(Index n) const = delete;

          template<unsigned codim>
          typename std::enable_if<codim==0, const VcLevelEntity &>::type
          subEntity(Index n) const
          {
            assert(n == 0);
            return *this;
          }

          Index index() const = delete;
          Index subIndex(unsigned i, unsigned codim) const = delete;
        };

        template<class Patch, unsigned codim, unsigned lanes>
        class VcLevelEntity<Patch, codim, lanes,
                          typename std::enable_if<Patch::dimension == codim>
                          ::type>
        {
          const Patch *patch_;
          typedef typename Patch::Index Index;
          Index chunkIndex_;
          unsigned level_;
          using simdCtype = Vc::SimdArray<typename Patch::ctype, lanes>;

        protected:
          Index &iteratorIndex()       { return chunkIndex_; }
          Index  iteratorIndex() const { return chunkIndex_; }

          friend LevelEntity<Patch, 0>;
          VcLevelEntity(const Patch *patch, unsigned level, Index chunkIndex) :
            patch_(patch), chunkIndex_(chunkIndex), level_(level)
          { }

        public:
          enum { codimension = codim };
          enum { dimension = Patch::dimension };
          enum { mydimension = 0 };

          VcLevelEntity() = default;

          using Geometry =
            Dune::AxisAlignedCubeGeometry<simdCtype, mydimension, dimension>;
          Geometry geometry() const
          {
            using std::begin;
            using ctype = typename Patch::ctype;

            auto n = 1u << level_;
            auto subVertices = ipow(n + 1, unsigned(dimension));
            auto macroChunkIndex = chunkIndex_ / subVertices;
            auto subIndex = chunkIndex_ - macroChunkIndex;

            Dune::FieldVector<ctype, dimension> local;
            for(auto &c : local)
            {
              c = ctype(subIndex % (n + 1)) / n;
              subIndex /= (n + 1);
            }

            return
            { begin(patch_->macroGridView().template vcRange<0, lanes>())
                [macroChunkIndex].geometry().global(local) };
          }
          static const GeometryType &type()
          {
            return Patch::LevelGridView::IndexSet::types(codimension)[0];
          }

          Vc::SimdArray<Index, lanes> index() const {
            auto n = 1u << level_;
            auto subVertices = ipow(n + 1, unsigned(dimension));
            auto macroChunkIndex = chunkIndex_ / subVertices;
            auto subIndex = chunkIndex_ - macroChunkIndex;

            auto index = Vc::SimdArray<Index, lanes>::IndexesFromZero();
            index += macroChunkIndex * lanes;
            index *= subVertices;
            index += subIndex;

            return index;
          }
        };
#endif //Vc_FOUND

      } // namespace Unconnected
    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_UNCONNECTED_LEVELENTITY_HH
