#ifndef DUNE_PATCHES_CUBE_UNCONNECTED_LEVELVIEW_HH
#define DUNE_PATCHES_CUBE_UNCONNECTED_LEVELVIEW_HH

#include <dune/common/iteratorrange.hh>
#include <dune/common/unused.hh>

#include <dune/geometry/dimension.hh>
#include <dune/geometry/type.hh>

#include <dune/grid/common/rangegenerators.hh>

#include <dune/patches/common/indexediterator.hh>
#include <dune/patches/cube/levelboundaryintersection.hh>
#include <dune/patches/cube/unconnected/levelentity.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {
      namespace Unconnected {

        template<class Patch>
        class LevelView
        {
          const Patch *patch_;
          unsigned level_;

        public:
          enum { dimension = Patch::dimension };
          enum { dimensionworld = Patch::dimensionworld };
          typedef typename Patch::ctype ctype;
          typedef typename Patch::Index Index;

          LevelView() = default;
          LevelView(const Patch *patch, unsigned level) :
            patch_(patch), level_(level)
          { }

          typedef Patch Grid;
          const Patch &grid() const
          {
            return *patch_;
          }

          typedef typename Patch::IndexSet IndexSet;
          IndexSet indexSet() const { return patch_->indexSet(); }

          // entities
          template<unsigned codim>
          struct Codim
          {
            typedef LevelEntity<Patch, codim> Entity;
            typedef typename Entity::Geometry Geometry;
            typedef IndexedIterator<Entity> Iterator;
#if Vc_FOUND
            template<unsigned lanes>
            struct Vc {
              using Entity = VcLevelEntity<Patch, codim, lanes>;
              using Iterator = IndexedIterator<Entity>;
            };
#endif // Vc_FOUND
          };

          template<unsigned codim>
          typename Codim<codim>::Iterator begin() const
          {
            return { patch_, level_, 0 };
          }
          template<unsigned codim>
          typename Codim<codim>::Iterator end() const
          {
            return { patch_, level_, size(codim) };
          }
#if Vc_FOUND
          template<unsigned codim, unsigned lanes>
          IteratorRange<typename Codim<codim>::template Vc<lanes>::Iterator>
          vcRange() const
          {
            if(size(codim) % lanes != 0)
              DUNE_THROW(NotImplemented, "Tail handling");
            using Iterator =
              typename Codim<codim>::template Vc<lanes>::Iterator;
            return { Iterator(patch_, level_, 0),
                     Iterator(patch_, level_, size(codim)/lanes) };
          }
#endif // Vc_FOUND

          // boundary intersections
          typedef LevelBoundaryIntersection<Patch> BoundaryIntersection;
          typedef IndexedIterator<BoundaryIntersection>
          BoundaryIntersectionIterator;
          BoundaryIntersectionIterator bBegin() const
          {
            return { patch_, level_, 0 };
          }
          BoundaryIntersectionIterator bEnd() const
          {
            return { patch_, level_, patch_->bIntersectionSize_ };
          }

          // interior intersections
          class InteriorIntersection;
          class InteriorIntersectionIterator;
          InteriorIntersectionIterator iBegin() const;
          InteriorIntersectionIterator iEnd() const;

          // size
          Index size(unsigned codim) const
          {
            if(codim > dimension)
              return 0;

            // number of subdivisions per direction
            unsigned n = 1 << level_;
            auto mydim = dimension-codim;

            // compute
            // size = \sum_{dim=mydim}^{dimension}
            //         size_[dimension-dim] * n^mydim * (n-1)^{dim-mydim}
            //              * {dim \choose mydim}
            Index size = 0;
            unsigned n_1_to_dim_mydim = 1; // pow(n-1, dim-mydim)
            unsigned dim_choose_mydim = 1; // choose(dim, mydim)
            for(auto dim : range<unsigned>(mydim,dimension+1))
            {
              size += patch_->entityCount(dimension-dim) * n_1_to_dim_mydim
                * dim_choose_mydim;
              n_1_to_dim_mydim *= n-1;
              dim_choose_mydim *= dim+1;
              dim_choose_mydim /= dim+1-mydim;
            }
            for(auto DUNE_UNUSED i : range(mydim))
              size *= n;
            return size;
          }
          Index size(const GeometryType &t) const
          {
            if(t.isCube())
              return size(dimension - t.dim());
            else
              return 0;
          }
        };

        template<typename Patch, int codim>
        IteratorRange<typename LevelView<Patch>::template Codim<codim>::
                      Iterator>
        entities(const LevelView<Patch>& pv, Codim<codim> cd)
        {
          static_assert(0 <= codim && codim <= Patch::dimension,
                        "invalid codimension for given PatchView");
          return { pv.template begin<codim>(), pv.template end<codim>() };
        }

        using Dune::elements;
        using Dune::vertices;

      } //namespace Unconnected
    } // namespace Cube
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_CUBE_UNCONNECTED_LEVELVIEW_HH
