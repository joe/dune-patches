#ifndef DUNE_PATCHES_CUBE_UNCONNECTED_PATCH_HH
#define DUNE_PATCHES_CUBE_UNCONNECTED_PATCH_HH

#include <array>
#include <cassert>
#include <cstddef>
#include <memory>
#include <type_traits>
#include <vector>

#include <dune/common/fvector.hh>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include <dune/patches/cube/indexset.hh>
#include <dune/patches/cube/macroentity.hh>
#include <dune/patches/cube/macroview.hh>
#include <dune/patches/cube/unconnected/levelentity.hh>
#include <dune/patches/cube/unconnected/levelview.hh>
#include <dune/patches/utility/icccompat.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace Cube {
      namespace Unconnected {

        template<class ct, unsigned d, std::size_t align = sizeof(double)*8,
                 std::size_t internalAlign = sizeof(double)*32>
        class Patch {
        public:
          enum { dimension = d };
          enum { dimensionworld = d };
          typedef ct ctype;
          typedef unsigned Index;
          static_assert(std::is_trivial<ctype>::value,
                        "Coordinate type must be trivial");
          static_assert(std::is_trivial<Index>::value,
                        "Index type must be trivial");

          //! placement-new operator
          /**
           * This enables new-expressions of the form
           * \code
           *   new(extraSize) Patch(constructor_args...)
           * \endcode
           * where \c extraSize specifies the additional amount of memory to
           * allocate (not including \c sizeof(Patch) for the header).
           */
          void *operator new(std::size_t size, std::size_t extraSize)
          {
            return ::operator new(size + extraSize);
          }
          //! placement-delete operator
          /**
           * This is used by a placement-new-expression to free already
           * allocated memory in case of exception.
           */
          void operator delete(void *p, std::size_t)
          {
            ::operator delete(p);
          }
          //! regular delete operator
          /**
           * This is needed because the placement-delete operator hides the
           * global operator delete, and we still want the standard deleter to
           * work.
           */
          void operator delete(void *p)
          {
            ::operator delete(p);
          }

          //! disable non-placement new
          void* operator new(std::size_t) = delete;
          //! disable array new
          void* operator new[](std::size_t) = delete;
          //! disable array delete
          void operator delete[](void*) = delete;

        private:
          // entitycounts in the coarse mesh (indexed by codimension of the
          // entity)
          Index elementCount_;

          Index entityCount(unsigned codim) const {
            static const auto & refelem =
              ReferenceElements<ctype, dimension>::cube();
            return elementCount_ * refelem.size(codim);
          }

          const char *storage() const
          { return reinterpret_cast<const char*>(this); }
          char *storage() { return reinterpret_cast<char*>(this); }
        public:
          std::size_t allocatedSize() const
          {
            return pad(sizeof(Patch)) + pad(sizeof(ctype) * elementCount_ *
                                            (1 << dimension) * dimensionworld);
          }

        private:
          /////////////////////////////////////////////////////////////////////
          //
          // members of type ctype
          //

          // the vertex coordinates

          // use the layout coords[elemVert][dir][elem].  This allows easy
          // vectorization across macro elements.  Beyond that the ordering
          // (dir-major vs. elemVert-major) probably does not matter with
          // regard to memory access.  TODO: In principle we would need
          // padding at the end of each coords[elemVert][dir] array, but we
          // skip that for now (we have to choose the number of elements as a
          // multiple of the number of lanes anyway).
          static constexpr ptrdiff_t offsetVertices()
          { return pad(sizeof(Patch)); }
          const ctype *vertices() const
          {
            return reinterpret_cast<const ctype *>(storage()+offsetVertices());
          }
          ctype *vertices()
          {
            return reinterpret_cast<ctype *>(storage()+offsetVertices());
          }
        iccmicprivate:
          FieldVector<ctype, dimensionworld> vertex(Index i) const
          {
            Index elemVert = i % (1 << dimension);
            Index elem = i / (1 << dimension);
            FieldVector<ctype, dimensionworld> v;
            for(auto dir : range(dimensionworld))
              v[dir] = vertices()[elementCount_*dimensionworld*elemVert
                                  + elementCount_*dir + elem];
            return v;
          }

#ifdef Vc_FOUND
          template<std::size_t lanes>
          void loadVertex
          ( FieldVector<Vc::SimdArray<ctype, lanes>, dimensionworld> &v,
            Index chunkIndex) const
          {
            Index elemVert = chunkIndex % (1 << dimension);
            Index elemChunk = chunkIndex / (1 << dimension);
            for(auto dir : range(dimensionworld))
              v[dir].load(vertices() + elementCount_*dimensionworld*elemVert
                          + elementCount_*dir + elemChunk*lanes);
          }
#endif // Vc_FOUND

        private:
          void setVertex(Index i, const FieldVector<ctype, dimensionworld> &v)
          {
            Index elemVert = i % (1 << dimension);
            Index elem = i / (1 << dimension);
            for(auto dir : range(dimensionworld))
              vertices()[elementCount_*dimensionworld*elemVert
                         + elementCount_*dir + elem] = v[dir];
          }

          /////////////////////////////////////////////////////////////////////
          //
          // members of type Index
          //

        iccmicprivate:
          Index elementSubEntity(Index elem, std::size_t codim,
                                 std::size_t s) const
          {
            static const auto &ref =
              ReferenceElements<ctype, dimension>::cube();
            return elem * ref.size(codim) + s;
          }

          Index entityVertex(std::size_t codim, Index entity,
                             std::size_t v) const
          {
            assert(codim==0);
            return entity * (1 << dimension) + v;
          }

          template<unsigned lanes>
          Index entityVertexChunk(std::size_t codim, Index entityChunk,
                                  std::size_t v) const
          {
            assert(codim==0);
            return entityChunk * (1 << dimension) + v;
          }

        private:
          // patch creation
          Patch() = default;

          // make copying private.  may be deleted in the long run.
          Patch(const Patch &) = default;
          Patch(Patch &&) = delete;

          Patch(Index elementCount) :
            elementCount_(elementCount)
          { }

          static constexpr std::size_t pad(std::size_t size)
          {
            return (size / internalAlign + bool(size % internalAlign))
              * internalAlign;
          }

        public:
          static std::unique_ptr<Patch>
          create(Index elementCount)
          {
            std::size_t extraSize =
              Patch{ elementCount }.allocatedSize() - sizeof(Patch);

            return std::unique_ptr<Patch>(new(extraSize) Patch(elementCount));
          }

          template<class Coord>
          static std::unique_ptr<Patch>
          create(const std::vector<Coord> &vertices)
          {
            auto vertexCount = vertices.size();
            auto verticesPerElem = 1 << dimension;
            assert(vertexCount % verticesPerElem == 0);

            auto pp = create(vertexCount / verticesPerElem);
            for(auto i : range(vertexCount))
              pp->setVertex(i, vertices[i]);

            return pp;
          }

          // general use
          typedef Cube::IndexSet<Patch> IndexSet; // TODO
          friend IndexSet;
          IndexSet indexSet() const { return {}; }

          typedef LevelView<Patch> LevelGridView; // TODO
          friend LevelGridView;
          template<class, unsigned, class>
          friend class LevelEntity;
          friend typename LevelGridView::BoundaryIntersection;
          LevelGridView levelGridView(unsigned level) const
          { return { this, level }; }

          typedef MacroView<Patch> MacroGridView; // TODO
          friend MacroGridView;
          template<class, unsigned, class>
          friend class MacroEntity;
          template<class, unsigned, unsigned, class>
          friend class VcMacroEntity;
          friend typename MacroGridView::BoundaryIntersection;
          MacroGridView macroGridView() const
          { return { this }; }

        private:
          typedef std::array<GeometryType, 1> Types;
          static const Types &types(int codim) {
            static const std::array<Types, dimension+1> types_ =
              [] () -> std::array<Types, dimension+1> {
                std::array<Types, dimension+1> tmp;
                for(auto cd : range(dimension+1))
                  tmp[cd][0].makeCube(dimension-cd);
                return tmp;
            } ();
            return types_[codim];
          }
        };

      } // namespace Unconnected
    } // namespace Cube
  } // namespace Patches
} // namespace Dune

// This is a kludge to make mesh generators work that only get the type of
// patch as a template parameter, but use the GridFactory internally.  They
// have no idea which file to include to get the correct factory.  The user of
// the generator must have included this patch.hh file to be able to pass the
// type of the patch to it, so include the factory from here.
#include <dune/patches/cube/unconnected/factory.hh> // TODO

#endif // DUNE_PATCHES_CUBE_UNCONNECTED_PATCH_HH
