// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_PATCHES_FINITEELEMENTMAP_REFINEDQ1FEM_HH
#define DUNE_PATCHES_FINITEELEMENTMAP_REFINEDQ1FEM_HH

#include <cstddef>

#include <dune/geometry/type.hh>

#include <dune/pdelab/finiteelementmap/finiteelementmap.hh>

#include <dune/patches/localfunction/refinedq1.hh>
#include <dune/patches/utility/math.hh>

namespace Dune {
  namespace Patches {

    template<class D, class R, unsigned dim>
    class RefinedQ1LocalFiniteElementMap
      : public PDELab::SimpleLocalFiniteElementMap<RefinedQ1LocalFiniteElement<
                                                     D,R,dim> >
    {
      using FE = RefinedQ1LocalFiniteElement<D,R,dim>;
      using Base = PDELab::SimpleLocalFiniteElementMap<FE>;

      unsigned subDivisions_;

    public:
      RefinedQ1LocalFiniteElementMap(unsigned subDivisions) :
        Base(FE(subDivisions)), subDivisions_(subDivisions)
      { }

      bool fixedSize() const
      {
        return true;
      }

      std::size_t size(GeometryType gt) const
      {
        if(gt.isCube())
          return ipow(subDivisions_-1, gt.dim());
        else
          return 0;
      }

      bool hasDOFs(int codim) const
      {
        if(subDivisions_ == 1)
          return codim == int(dim);
        return 0 <= codim && codim <= int(dim);
      }

      std::size_t maxLocalSize() const
      {
        return ipow(subDivisions_+1, dim);
      }
    };

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_FINITEELEMENTMAP_REFINEDQ1FEM_HH
