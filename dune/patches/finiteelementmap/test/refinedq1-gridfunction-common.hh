#ifndef DUNE_PATCHES_FINITEELEMENTMAP_TEST_REFINEDQ1_GRIDFUNCTION_COMMON_HH
#define DUNE_PATCHES_FINITEELEMENTMAP_TEST_REFINEDQ1_GRIDFUNCTION_COMMON_HH

#include <numeric>
#include <type_traits>

struct Interpolator
{
  template<class T>
  auto operator()(const T &pos) const
    -> typename std::decay<decltype(*pos.begin())>::type
  {
    using ctype = typename std::decay<decltype(*pos.begin())>::type;
    return std::accumulate(pos.begin(), pos.end(), ctype(0));
  }
};

#endif // DUNE_PATCHES_FINITEELEMENTMAP_TEST_REFINEDQ1_GRIDFUNCTION_COMMON_HH
