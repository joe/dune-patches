#include <config.h>

#include <iostream>
#include <ostream>
#include <string>

#include <dune/common/array.hh>
#include <dune/common/classname.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>

#include <dune/grid/common/rangegenerators.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/pdelab/backend/interface.hh>
// needed to provide the specialization by the backendselector for the default
// gridfunctionspace
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/common/clock.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include <dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/localvector.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>

#include <dune/patches/finiteelementmap/test/refinedq1-gridfunction-common.hh>
#include <dune/patches/utility/environment.hh>
#include <dune/patches/utility/range.hh>
#include <dune/patches/utility/unittest.hh>


template<class GFS, class V, class Func>
void interpolate(const GFS &gfs, V &v, const Func &func)
{
  using Dune::Patches::range;

  const auto &gv = gfs.gridView();

  using LFS = Dune::PDELab::LocalFunctionSpace<GFS>;
  LFS lfs(gfs);

  using LFSCache = Dune::PDELab::LFSIndexCache<LFS>;
  LFSCache lfsCache(lfs);

  using LocalView = typename V::template LocalView<LFSCache>;
  LocalView localView(v);

  Dune::PDELab::LocalVector<typename V::ElementType> vl(lfs.maxSize());
  for(const auto &e : elements(gv))
  {
    lfs.bind(e);
    const auto &geo = e.geometry();
    for(auto v : range(geo.corners()))
      vl(lfs, v) = func(geo.corner(v));

    lfsCache.update();
    localView.bind(lfsCache);

    localView.write(vl);
    localView.commit();
    localView.unbind();
  }
}

template<unsigned dim>
int testQ1GridFunction(unsigned gridDiv, unsigned subRefines, bool doOutput)
{
  using std::to_string;

  std::cout << "Checking Q1LocalFiniteElementMap<" << dim << ">..."
            << std::endl;

  using Grid = Dune::YaspGrid<dim>;
  using ctype = typename Grid::ctype;

  Grid grid(Dune::FieldVector<ctype, dim>(1),
            Dune::fill_array<int, dim>(gridDiv));
  grid.globalRefine(subRefines);

  auto gv = grid.leafGridView();
  using GV = decltype(gv);

  using FEM = Dune::PDELab::QkLocalFiniteElementMap<GV, ctype, double, 1>;
  FEM fem(gv);

  using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM>;
  GFS gfs(gv, fem);

  using V = typename Dune::PDELab::BackendVectorSelector<GFS, double>::Type;

  V v(gfs);
  auto start = Dune::PDELab::getWallTime();
  interpolate(gfs, v, Interpolator());
  auto stop = Dune::PDELab::getWallTime();
  std::cout << "Interpolate took " << (stop - start) << "s" << std::endl;

  if(doOutput)
  {
    Dune::VTKWriter<GV> writer(gv);
    Dune::PDELab::addSolutionToVTKWriter
      (writer, gfs, v,
       Dune::PDELab::vtk::DefaultFunctionNameGenerator("test"));
    writer.write("test-q1-gridfunction-d"+to_string(dim));
  }

  return 0;
}

int main(int argc, char **argv)
{
  using Dune::Patches::fromEnv;
  using Dune::Patches::UnitTest::acc;

  try {
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "Not using any SIMD" << std::endl;
    std::cout << "Using wall clock implementation "
              << Dune::PDELab::getWallTimeImp() << " (advertised resolution: "
              << Dune::PDELab::getWallTimeResolution() << "s)" << std::endl;

    auto gridDiv = fromEnv("GRID_DIV", 10u);
    auto subRefines = fromEnv("REFINES", 2u);
    auto doOutput = fromEnv("DO_OUTPUT", true);

    int result = 77;

    acc(result, testQ1GridFunction<2>(gridDiv, subRefines, doOutput));

    return result;
  }
  catch(const Dune::Exception &e)
  {
    using Dune::className;
    std::cout << className(e) << "(\"" << e << "\")" << std::endl;
    throw;
  }
}
