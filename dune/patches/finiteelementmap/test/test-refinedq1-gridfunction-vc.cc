#include <config.h>

#include <iostream>
#include <ostream>
#include <string>

#include <Vc/Vc>

#include <dune/common/array.hh>
#include <dune/common/classname.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>

#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/pdelab/backend/interface.hh>
// needed to provide the specialization by the backendselector for the default
// gridfunctionspace
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/common/clock.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include <dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/localvector.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>

#include <dune/patches/cube/unconnected/patch.hh>
#include <dune/patches/finiteelementmap/refinedq1fem.hh>
#include <dune/patches/finiteelementmap/test/refinedq1-gridfunction-common.hh>
#include <dune/patches/utility/environment.hh>
#include <dune/patches/utility/gridviewpatchfactory.hh>
#include <dune/patches/utility/range.hh>
#include <dune/patches/utility/unittest.hh>
#include <dune/patches/vccompat/localvector.hh>


template<unsigned lanes, class GFS, class V, class Func>
void interpolate(const GFS &gfs, V &v, const Func &func, unsigned level)
{
  using Dune::Patches::makeIteratorRange;

  using GV = typename GFS::Traits::GridView;
  const auto &gv = gfs.gridView();

  using ctype = typename GV::ctype;
  constexpr auto dim = GV::dimension;

  using Patch = Dune::Patches::Cube::Unconnected::Patch<ctype, dim>;
  using Factory = Dune::Patches::GridViewPatchFactory<GV>;
  Factory factory(gfs.gridView());

  using LFS = Dune::PDELab::LocalFunctionSpace<GFS>;
  LFS lfs(gfs);

  using LFSCache = Dune::PDELab::LFSIndexCache<LFS>;
  LFSCache lfsCache(lfs);

  using LocalView = typename V::template LocalView<LFSCache>;
  LocalView localView(v);

  Dune::PDELab::LocalVector<Vc::SimdArray<typename V::ElementType, lanes> >
    vl(lfs.maxSize());

  auto eit = gv.template begin<0>();
  auto eend = gv.template end<0>();
  while(eit != eend) {
    // determine end of chunk
    auto cend = eit;
    unsigned chunkSize;
    for(chunkSize = 0; chunkSize < lanes && cend != eend;
        ++chunkSize, ++cend) {}

    // create patch
    auto patchp = factory.template create<Patch>(makeIteratorRange(eit, cend));
    auto pv = patchp->levelGridView(level);

    if(chunkSize == lanes)
    {
      unsigned i = 0;
      for(const auto &vertex : pv.template vcRange<dim, lanes>())
      {
        accessBaseContainer(vl)[i] = func(vertex.geometry().center());
        ++i;
      }
    }
    else
      DUNE_THROW(Dune::NotImplemented,
                 "Tail handling (tail of " << chunkSize << ")");

    for(unsigned ci = 0; eit != cend; ++eit, ++ci)
    {
      lfs.bind(*eit);
      lfsCache.update();
      localView.bind(lfsCache);

      using Dune::Patches::VcCompat::laneView;
      localView.write(laneView(vl, ci));
      localView.commit();
      localView.unbind();
    }
  }
}

template<unsigned dim, unsigned lanes>
int testRefinedQ1GridFunction(unsigned gridDiv, unsigned subRefines,
                              bool doOutput)
{
  using std::to_string;

  const unsigned subDiv = 1 << subRefines;

  std::cout << "Checking RefinedQ1LocalFiniteElementMap<" << dim << ">..."
            << std::endl;

  using Grid = Dune::YaspGrid<dim>;
  using ctype = typename Grid::ctype;

  Grid grid(Dune::FieldVector<ctype, dim>(1),
            Dune::fill_array<int, dim>(gridDiv));

  auto gv = grid.leafGridView();
  using GV = decltype(gv);

  using FEM = Dune::Patches::RefinedQ1LocalFiniteElementMap<ctype, double,
                                                            dim>;
  FEM fem(subDiv);

  using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM>;
  GFS gfs(gv, fem);

  using V = typename Dune::PDELab::BackendVectorSelector<GFS, double>::Type;

  V v(gfs);
  auto start = Dune::PDELab::getWallTime();
  interpolate<lanes>(gfs, v, Interpolator(), subRefines);
  auto stop = Dune::PDELab::getWallTime();
  std::cout << "Interpolate took " << (stop - start) << "s" << std::endl;

  if(doOutput)
  {
    Dune::SubsamplingVTKWriter<GV> writer(gv, subRefines);
    Dune::PDELab::addSolutionToVTKWriter
      (writer, gfs, v,
       Dune::PDELab::vtk::DefaultFunctionNameGenerator("test"));
    writer.write("test-refinedq1-gridfunction-vc-d"+to_string(dim));
  }

  return 0;
}

int main(int argc, char **argv)
{
  using Dune::Patches::fromEnv;
  using Dune::Patches::UnitTest::acc;

  try {
    Dune::MPIHelper::instance(argc, argv);

    constexpr unsigned lanes = Vc::Vector<double>::Size;
    std::cout << "Using " << lanes << " SIMD lanes" << std::endl;
    std::cout << "Using wall clock implementation "
              << Dune::PDELab::getWallTimeImp() << " (advertised resolution: "
              << Dune::PDELab::getWallTimeResolution() << "s)" << std::endl;

    auto gridDiv = fromEnv("GRID_DIV", 10u);
    auto subRefines = fromEnv("REFINES", 2u);
    auto doOutput = fromEnv("DO_OUTPUT", true);

    int result = 77;

    acc(result,
        testRefinedQ1GridFunction<2, lanes>(gridDiv, subRefines, doOutput));

    return result;
  }
  catch(const Dune::Exception &e)
  {
    using Dune::className;
    std::cout << className(e) << "(\"" << e << "\")" << std::endl;
    throw;
  }
}
