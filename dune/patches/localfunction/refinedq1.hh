// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_PATCHES_LOCALFUNCTION_REFINEDQ1_HH
#define DUNE_PATCHES_LOCALFUNCTION_REFINEDQ1_HH

#include <dune/common/typetraits.hh>

#include <dune/geometry/type.hh>

#include <dune/localfunctions/common/localfiniteelementtraits.hh>
#include <dune/localfunctions/common/localtoglobaladaptors.hh>
#include <dune/localfunctions/lagrange/q1.hh>

#include <dune/patches/localfunction/refinedq1/localbasis.hh>
#include <dune/patches/localfunction/refinedq1/localcoefficients.hh>
#include <dune/patches/utility/base_from_member.hh>

namespace Dune {
  namespace Patches {

    /** \brief The local Q1 finite element on cubes
        \tparam D Domain data type
        \tparam R Range data type
        \tparam dim Dimension of the simplex
    */
    template<class D, class R, unsigned dim>
    class RefinedQ1LocalFiniteElement
    {
    public:
      /** \todo Please doc me !
       */
      using Traits =
        LocalFiniteElementTraits<RefinedQ1::LocalBasis<D,R,dim>,
                                 RefinedQ1::LocalCoefficients<dim>,
                                 Empty>;

      RefinedQ1LocalFiniteElement(unsigned subDivisions) :
        basis_(subDivisions),
        coefficients_(subDivisions),
        subDivisions_(subDivisions)
      { }

      /** \todo Please doc me !
       */
      const typename Traits::LocalBasisType& localBasis () const
      {
        return basis_;
      }

      /** \todo Please doc me !
       */
      const typename Traits::LocalCoefficientsType& localCoefficients () const
      {
        return coefficients_;
      }

      /** \brief Number of shape functions in this finite element */
      unsigned int size() const
      {
        return basis_.size();
      }

      /** \todo Please doc me !
       */
      GeometryType type () const
      {
        static const auto gt = []{
          GeometryType gt_;
          gt_.makeCube(dim);
          return gt_;
        }();

        return gt;
      }

      RefinedQ1LocalFiniteElement* clone () const
      {
        return new RefinedQ1LocalFiniteElement(*this);
      }

      //! The number of sub-divisions in each direction
      unsigned subDivisions() const
      {
        return subDivisions_;
      }

      //! Type of Sub-Finite-Elements
      using SubElement = Q1LocalFiniteElement<D, R, dim>;

      //! An instance of a SubElement
      /**
       * The returned instance has the correct orientation to be embedded into
       * this refined element.
       *
       * The program should not rely on the fact that a reference is returned,
       * i.e. it is unspecified whether a const reference or a temporary is
       * returned.
       */
      const SubElement &subElement() const
      {
        static const SubElement subElement_;
        return subElement_;
      }

    private:
      RefinedQ1::LocalBasis<D,R,dim> basis_;
      RefinedQ1::LocalCoefficients<dim> coefficients_;
      unsigned subDivisions_;
    };

    namespace RefinedQ1 {
      namespace Impl {

        template<class Geometry, class RF>
        using FactoryLFE = RefinedQ1LocalFiniteElement<
          typename Geometry::ctype, RF, Geometry::mydimension>;

        template<class Geometry, class RF>
        using FactoryBase = ScalarLocalToGlobalFiniteElementAdaptorFactory<
          FactoryLFE<Geometry, RF>, Geometry>;

      } // namespace Impl
    } // namespace RefinedQ1

    //! Factory for global-valued Q1 elements
    /**
     * \tparam Geometry Type of the geometry.  Used to extract the domain field
     *                  type and the dimension.
     * \tparam RF       Range field type.
     */
    template<class Geometry, class RF>
    class RefinedQ1FiniteElementFactory :
      private BaseFromMember<RefinedQ1::Impl::FactoryLFE<Geometry, RF> >,
      public RefinedQ1::Impl::FactoryBase<Geometry, RF>
    {
      using LFEMem =
        BaseFromMember<RefinedQ1::Impl::FactoryLFE<Geometry, RF> >;
      using Base = RefinedQ1::Impl::FactoryBase<Geometry, RF>;

    public:
      //! default constructor
      RefinedQ1FiniteElementFactory(unsigned subDivisions) :
        LFEMem{ subDivisions },
        Base(LFEMem::mem)
      { }

      RefinedQ1FiniteElementFactory
      (const RefinedQ1FiniteElementFactory &other) :
        LFEMem{ other },
        Base(LFEMem::mem)
      { }
    };

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_LOCALFUNCTION_REFINEDQ1_HH
