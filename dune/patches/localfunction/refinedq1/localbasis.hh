// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_PATCHES_LOCALFUNCTION_REFINEDQ1_LOCALBASIS_HH
#define DUNE_PATCHES_LOCALFUNCTION_REFINEDQ1_LOCALBASIS_HH

#include <algorithm>
#include <array>
#include <type_traits>
#include <vector>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/localfunctions/common/localbasis.hh>

#include <dune/patches/utility/math.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace RefinedQ1 {

      /**
       * @brief virtual base class for a local basis
       *
       * Provides the local basis interface with pure virtual methods.
       * This is the base interface with differentiation order 0.
       */
      template<class D, class R, unsigned dim>
      class LocalBasis
      {
        unsigned subDivisions_;

        template<class T1, class T2, class T3>
        static typename std::common_type<T1, T2, T3>::type
        limit_value(T1 lb, T2 ub, T3 value)
        {
          using std::min;
          using std::max;
          using C = typename std::common_type<T1, T2, T3>::type;
          return min(static_cast<C>(ub),
                     max(static_cast<C>(lb), static_cast<C>(value)));
        }

      public:
        using Traits = LocalBasisTraits<D,dim,Dune::FieldVector<D,dim>,
                                        R,1,Dune::FieldVector<R,1>,
                                        Dune::FieldMatrix<R,1,dim> >;

        LocalBasis(unsigned subDivisions) :
          subDivisions_(subDivisions)
        { }

        //! \brief Number of shape functions
        unsigned size() const {
          return ipow(subDivisions_+1, dim);
        }

        //! \brief Polynomial order of the shape functions
        /**
         * \note This does not make any sense for this element; you need very
         *       special quadrature rules to integrate it.  Thus is is marked
         *       as deleted.
         */
        unsigned order() const = delete;

        /** \brief Evaluate all basis function at given position
         *
         * Evaluates all shape functions at the given position and returns
         * these values in a vector.
         */
        void
        evaluateFunction(const typename Traits::DomainType& in,
                         std::vector<typename Traits::RangeType>& out) const
        {
          // Determine local coordinate inside subelement, and the index of
          // the origin of the subelement
          auto subIn = in;
          unsigned origin = 0;
          {
            unsigned base = 1;
            for(auto d : range(dim))
            {
              subIn[d] *= subDivisions_;
              // number of the left corner of the element within direction d
              unsigned dirIndex = limit_value(0, subDivisions_-1, subIn[d]);
              subIn[d] -= dirIndex;
              origin += base * dirIndex;
              base *= (subDivisions_ + 1);
            }
          }

          // evaluate on the subelement
          std::array<R, (1 << dim)> subOut;
          subOut[0] = 1;
          for(auto d : range(dim))
          {
            auto funcs_per_d = 1 << d;
            auto xh = subIn[d];
            auto xl = 1 - xh;
            for(auto i : range(funcs_per_d))
            {
              subOut[funcs_per_d + i] = subOut[i] * xh;
              subOut[i] *= xl;
            }
          }

          // copy into output
          out.clear();
          out.resize(size(), typename Traits::RangeType(0));
          for(unsigned src = 0; src < (1 << dim); ++src)
          {
            unsigned base = 1;
            unsigned dst = origin;
            for(auto d : range(dim))
            {
              dst += base * ((src >> d) & 1u);
              base *= (subDivisions_ + 1);
            }

            out[dst][0] = subOut[src];
          }
        }

        //! \brief Evaluate Jacobian of all shape functions
        /**
         * \note This does not make much sense for this element since the
         *       Jacobian has jumps.  Thus it is marked as deleted.
         */
        void
        evaluateJacobian(const typename Traits::DomainType& in,
                         std::vector<typename Traits::JacobianType>& out) const
          = delete;

      };

    } // namespace RefinedQ1
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_LOCALFUNCTION_REFINEDQ1_LOCALBASIS_HH
