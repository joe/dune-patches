// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_PATCHES_LOCALFUNCTION_REFINEDQ1_LOCALCOEFFICIENTS_HH
#define DUNE_PATCHES_LOCALFUNCTION_REFINEDQ1_LOCALCOEFFICIENTS_HH

#include <array>
#include <cstddef>
#include <vector>

#include <dune/common/unused.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/localfunctions/common/localkey.hh>

#include <dune/patches/utility/math.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace RefinedQ1 {

      namespace Impl {
        constexpr unsigned dimOf(unsigned i)
        {
          return (i == 0) ? 0 : (i % 3 == 1) + dimOf(i / 3);
        }

        template<unsigned dim>
        class SubentityNumbering :
          public std::array<unsigned, ipow(3, dim)>
        {
          SubentityNumbering()
          {
            const auto &base = SubentityNumbering<dim-1>::instance();

            for(auto i : range(base.size()))
            {
              auto baseEntityDim = dimOf(i);

              // interior: an entity of dimension baseEntityDim+1
              (*this)[  base.size()+i] = base[i] ;

              // account for interior entities of dimension baseEntityDim,
              // generated from base entities of dimension baseEntityDim-1
              unsigned offset = 0;
              if(baseEntityDim > 0) offset += base.size(baseEntityDim-1);

              // bottom: an entity of dimension baseEntityDim
              (*this)[              i] = base[i] + offset;

              // top: an entity of dimension baseEntityDim
              // also account for bottom entities of dimension baseEntityDim
              (*this)[2*base.size()+i] = base[i] + offset + base.size(baseEntityDim);
            }
          }

          SubentityNumbering(const SubentityNumbering&) = delete;

        public:
          static const SubentityNumbering &instance()
          {
            static const SubentityNumbering instance_;
            return instance_;
          }

          using std::array<unsigned, ipow(3, dim)>::size;
          // number of sub-entities of dimension d
          static unsigned size(unsigned d)
          {
            static const auto &refelem =
              ReferenceElements<double, dim>::cube();
            return refelem.size(dim - d);
          }
        };

        // end of induction
        template<>
        class SubentityNumbering<0> :
          public std::array<unsigned, 1>
        {
          SubentityNumbering()
          {
            (*this)[0] = 0;
          }

          SubentityNumbering(const SubentityNumbering&) = delete;

        public:
          static const SubentityNumbering &instance()
          {
            static const SubentityNumbering instance_;
            return instance_;
          }

          using std::array<unsigned, 1>::size;
          // number of sub-entities of dimension d
          static unsigned size(unsigned d)
          {
            return unsigned(d == 0);
          }
        };

      } // namespace Impl

      /**@ingroup LocalLayoutImplementation
         \brief Layout map for Q1 elements

         \nosubgrouping
         \implements Dune::LocalCoefficientsVirtualImp
      */
      template <unsigned dim>
      class LocalCoefficients
      {
        unsigned subDivisions_;

        // convert bottom/interior/top numbering into reference element
        // numbering
        unsigned refSubentity(unsigned i) const
        {
          // base-3 bottom/interior/top numbering
          unsigned subentity = 0;
          unsigned base = 1;
          for(auto DUNE_UNUSED d : range(dim))
          {
            auto di = i % (subDivisions_ + 1);
            if(di == 0)
            {
              // nothing to do
            }
            else if(di == subDivisions_)
            {
              subentity += 2 * base;
            }
            else
            {
              subentity += base;
            }
            base *= 3;
            i /= (subDivisions_ + 1);
          }
          // reference element numbering
          return Impl::SubentityNumbering<dim>::instance()[subentity];
        }

        unsigned codim(unsigned i) const
        {
          unsigned cd = 0;
          for(auto DUNE_UNUSED d : range(dim))
          {
            auto di = i % (subDivisions_ + 1);
            if(di == 0 || di == subDivisions_)
              ++cd;
            i /= (subDivisions_ + 1);
          }
          return cd;
        }

        unsigned numberInSubentity(unsigned i) const
        {
          unsigned iInSubentity = 0;
          unsigned baseInSubentity = 1;
          for(auto DUNE_UNUSED d : range(dim))
          {
            auto di = i % (subDivisions_ + 1);
            if(di == 0 || di == subDivisions_)
            {
              // nothing to do
            }
            else
            {
              iInSubentity += baseInSubentity * (di - 1);
              // count only interior points
              baseInSubentity *= (subDivisions_ - 1);
            }
            i /= (subDivisions_ + 1);
          }
          return iInSubentity;
        }
      public:
        //! \brief Standard constructor
        LocalCoefficients(unsigned subDivisions) :
          subDivisions_(subDivisions)
        { }

        //! number of coefficients
        std::size_t size() const
        {
          return ipow(subDivisions_+1, dim);
        }

        //! get i'th index
        const LocalKey localKey (std::size_t i) const
        {
          return { refSubentity(i), codim(i), numberInSubentity(i) };
        }
      };

      template <unsigned dim>
      class CachedLocalCoefficients
      {
        std::vector<LocalKey> keys_;

      public:
        //! \brief Standard constructor
        CachedLocalCoefficients(unsigned subDivisions)
        {
          LocalCoefficients<dim> uncached(subDivisions);
          keys_.reserve(uncached.size());
          for(auto i : range(uncached.size()))
            keys_.push_back(uncached.localKey(i));
        }

        //! number of coefficients
        std::size_t size() const
        {
          return keys_.size();
        }

        //! get i'th index
        const LocalKey &localKey (std::size_t i) const
        {
          return keys_[i];
        }
      };

    } // namespace RefinedQ1
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_LOCALFUNCTION_REFINEDQ1_LOCALCOEFFICIENTS_HH
