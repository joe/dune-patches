#include <config.h>

#include <iostream>
#include <limits>
#include <ostream>
#include <string>
#include <type_traits>
#include <vector>

#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/unused.hh>

#include <dune/geometry/type.hh>

#include <dune/localfunctions/test/geometries.hh>

#include <dune/patches/localfunction/refinedq1.hh>
#include <dune/patches/utility/math.hh>
#include <dune/patches/utility/range.hh>

std::string stringStatus(int status)
{
  using std::to_string;

  switch(status) {
  case  0: return "PASS";
  case  1: return "FAIL";
  case 77: return "SKIP";
  default: return "FAIL(" + to_string(status) + ")";
  }
}

void acc(int &result, bool other)
{
  if(other)
  {
    if(result == 77)
      result = 0;
  }
  else
    result = 1;
}

void acc(int &result, int other)
{
  if(other != 77)
    acc(result, other == 0);
}

template<class Coefficients>
bool testLocalKey(const Coefficients &lc, unsigned i,
                  unsigned subEntity, unsigned codim, unsigned index)
{
  const auto &key = lc.localKey(i);
  bool result = key.subEntity() == subEntity && key.codim() == codim &&
    key.index() == index;
  if(!result)
    std::cerr << "Mismatch for local key " << i
              << ": key is (subEntity, codim, index)=(" << key.subEntity()
              << ", " << key.codim() << ", " << key.index()
              << "), expected: (" << subEntity << ", " << codim << ", "
              << index << ")" << std::endl;
  return result;
}

template<unsigned dim>
int testCoefficients(std::integral_constant<unsigned, dim>)
{
  return 77;
}

int testCoefficients(std::integral_constant<unsigned, 0>)
{
  std::cout << "Testing Coefficients<dim=0>..." << std::endl;

  Dune::Patches::RefinedQ1LocalFiniteElement<double, double, 0> fe(3);
  const auto &lc = fe.localCoefficients();

  int status = 77;

  acc(status, testLocalKey(lc,  0,  0, 0, 0));

  std::cout << "Testing Coefficients<dim=0>... " << stringStatus(status)
            << std::endl;

  return status;
}

int testCoefficients(std::integral_constant<unsigned, 1>)
{
  std::cout << "Testing Coefficients<dim=1>..." << std::endl;

  Dune::Patches::RefinedQ1LocalFiniteElement<double, double, 1> fe(3);
  const auto &lc = fe.localCoefficients();

  int status = 77;

  acc(status, testLocalKey(lc,  0,  0, 1, 0));
  acc(status, testLocalKey(lc,  1,  0, 0, 0));
  acc(status, testLocalKey(lc,  2,  0, 0, 1));
  acc(status, testLocalKey(lc,  3,  1, 1, 0));

  std::cout << "Testing Coefficients<dim=1>... " << stringStatus(status)
            << std::endl;

  return status;
}

int testCoefficients(std::integral_constant<unsigned, 2>)
{
  std::cout << "Testing Coefficients<dim=2>..." << std::endl;

  Dune::Patches::RefinedQ1LocalFiniteElement<double, double, 2> fe(3);
  const auto &lc = fe.localCoefficients();

  int status = 77;

  acc(status, testLocalKey(lc,  0,  0, 2, 0));
  acc(status, testLocalKey(lc,  1,  2, 1, 0));
  acc(status, testLocalKey(lc,  2,  2, 1, 1));
  acc(status, testLocalKey(lc,  3,  1, 2, 0));

  acc(status, testLocalKey(lc,  4,  0, 1, 0));
  acc(status, testLocalKey(lc,  5,  0, 0, 0));
  acc(status, testLocalKey(lc,  6,  0, 0, 1));
  acc(status, testLocalKey(lc,  7,  1, 1, 0));

  acc(status, testLocalKey(lc,  8,  0, 1, 1));
  acc(status, testLocalKey(lc,  9,  0, 0, 2));
  acc(status, testLocalKey(lc, 10,  0, 0, 3));
  acc(status, testLocalKey(lc, 11,  1, 1, 1));

  acc(status, testLocalKey(lc, 12,  2, 2, 0));
  acc(status, testLocalKey(lc, 13,  3, 1, 0));
  acc(status, testLocalKey(lc, 14,  3, 1, 1));
  acc(status, testLocalKey(lc, 15,  3, 2, 0));

  std::cout << "Testing Coefficients<dim=2>... " << stringStatus(status)
            << std::endl;

  return status;
}

int testCoefficients(std::integral_constant<unsigned, 3>)
{
  std::cout << "Testing Coefficients<dim=3>..." << std::endl;

  Dune::Patches::RefinedQ1LocalFiniteElement<double, double, 3> fe(3);
  const auto &lc = fe.localCoefficients();

  int status = 77;

  // plane 0 (bottom)
  acc(status, testLocalKey(lc,  0,  0, 3, 0));
  acc(status, testLocalKey(lc,  1,  6, 2, 0));
  acc(status, testLocalKey(lc,  2,  6, 2, 1));
  acc(status, testLocalKey(lc,  3,  1, 3, 0));

  acc(status, testLocalKey(lc,  4,  4, 2, 0));
  acc(status, testLocalKey(lc,  5,  4, 1, 0));
  acc(status, testLocalKey(lc,  6,  4, 1, 1));
  acc(status, testLocalKey(lc,  7,  5, 2, 0));

  acc(status, testLocalKey(lc,  8,  4, 2, 1));
  acc(status, testLocalKey(lc,  9,  4, 1, 2));
  acc(status, testLocalKey(lc, 10,  4, 1, 3));
  acc(status, testLocalKey(lc, 11,  5, 2, 1));

  acc(status, testLocalKey(lc, 12,  2, 3, 0));
  acc(status, testLocalKey(lc, 13,  7, 2, 0));
  acc(status, testLocalKey(lc, 14,  7, 2, 1));
  acc(status, testLocalKey(lc, 15,  3, 3, 0));

  // plane 1
  acc(status, testLocalKey(lc, 16,  0, 2, 0));
  acc(status, testLocalKey(lc, 17,  2, 1, 0));
  acc(status, testLocalKey(lc, 18,  2, 1, 1));
  acc(status, testLocalKey(lc, 19,  1, 2, 0));

  acc(status, testLocalKey(lc, 20,  0, 1, 0));
  acc(status, testLocalKey(lc, 21,  0, 0, 0));
  acc(status, testLocalKey(lc, 22,  0, 0, 1));
  acc(status, testLocalKey(lc, 23,  1, 1, 0));

  acc(status, testLocalKey(lc, 24,  0, 1, 1));
  acc(status, testLocalKey(lc, 25,  0, 0, 2));
  acc(status, testLocalKey(lc, 26,  0, 0, 3));
  acc(status, testLocalKey(lc, 27,  1, 1, 1));

  acc(status, testLocalKey(lc, 28,  2, 2, 0));
  acc(status, testLocalKey(lc, 29,  3, 1, 0));
  acc(status, testLocalKey(lc, 30,  3, 1, 1));
  acc(status, testLocalKey(lc, 31,  3, 2, 0));

  // plane 2
  acc(status, testLocalKey(lc, 32,  0, 2, 1));
  acc(status, testLocalKey(lc, 33,  2, 1, 2));
  acc(status, testLocalKey(lc, 34,  2, 1, 3));
  acc(status, testLocalKey(lc, 35,  1, 2, 1));

  acc(status, testLocalKey(lc, 36,  0, 1, 2));
  acc(status, testLocalKey(lc, 37,  0, 0, 4));
  acc(status, testLocalKey(lc, 38,  0, 0, 5));
  acc(status, testLocalKey(lc, 39,  1, 1, 2));

  acc(status, testLocalKey(lc, 40,  0, 1, 3));
  acc(status, testLocalKey(lc, 41,  0, 0, 6));
  acc(status, testLocalKey(lc, 42,  0, 0, 7));
  acc(status, testLocalKey(lc, 43,  1, 1, 3));

  acc(status, testLocalKey(lc, 44,  2, 2, 1));
  acc(status, testLocalKey(lc, 45,  3, 1, 2));
  acc(status, testLocalKey(lc, 46,  3, 1, 3));
  acc(status, testLocalKey(lc, 47,  3, 2, 1));

  // plane 3 (top)
  acc(status, testLocalKey(lc, 48,  4, 3, 0));
  acc(status, testLocalKey(lc, 49, 10, 2, 0));
  acc(status, testLocalKey(lc, 50, 10, 2, 1));
  acc(status, testLocalKey(lc, 51,  5, 3, 0));

  acc(status, testLocalKey(lc, 52,  8, 2, 0));
  acc(status, testLocalKey(lc, 53,  5, 1, 0));
  acc(status, testLocalKey(lc, 54,  5, 1, 1));
  acc(status, testLocalKey(lc, 55,  9, 2, 0));

  acc(status, testLocalKey(lc, 56,  8, 2, 1));
  acc(status, testLocalKey(lc, 57,  5, 1, 2));
  acc(status, testLocalKey(lc, 58,  5, 1, 3));
  acc(status, testLocalKey(lc, 59,  9, 2, 1));

  acc(status, testLocalKey(lc, 60,  6, 3, 0));
  acc(status, testLocalKey(lc, 61, 11, 2, 0));
  acc(status, testLocalKey(lc, 62, 11, 2, 1));
  acc(status, testLocalKey(lc, 63,  7, 3, 0));

  std::cout << "Testing Coefficients<dim=3>... " << stringStatus(status)
            << std::endl;

  return status;
}

template<unsigned dim>
int testCoefficients()
{
  return testCoefficients(std::integral_constant<unsigned, dim>());
}

template<unsigned dim>
int testBasis()
{
  using Dune::Patches::range;
  using Dune::Patches::sqr;

  std::cout << "Testing Basis..." << std::endl;

  Dune::Patches::RefinedQ1LocalFiniteElement<double, double, dim> fe(3);
  const auto &lb = fe.localBasis();

  const Dune::FieldVector<double, dim> pos(0.5);
  std::vector<Dune::FieldVector<double, 1> > result;
  lb.evaluateFunction(pos, result);

  double expected = 1;
  for(auto DUNE_UNUSED d : range(dim))
    expected *= 0.5;

  auto rho = std::numeric_limits<double>::epsilon();

  int status = 0;
  for(auto i : range(result.size()))
  {
    bool interior = true;
    {
      auto ii = i;
      for(auto DUNE_UNUSED d : range(dim))
      {
        interior &= (ii % 4) % 3 > 0;
        ii /= 4;
      }
    }
    double myexpected = interior ? expected : 0;
    if(sqr(result[i][0] - myexpected) > rho)
    {
      status = 1;
      std::cerr << "Error: shape function " << i << " has value "
                << result[i][0] << " at position (" << pos
                << "), expected value " << myexpected << std::endl;
    }
  }

  std::cout << "Testing Basis... " << stringStatus(status) << std::endl;

  return status;
}

template<unsigned dim>
int testRefinedQ1()
{
  using Dune::Patches::range;
  using Dune::Patches::sqr;

  std::cout << "Checking RefinedQ1<double, " << dim << ">..." << std::endl;

  Dune::GeometryType gt;
  gt.makeCube(dim);

  typedef TestGeometries<double, dim> TestGeos;
  static const TestGeos testGeos;

  typedef typename TestGeos::Geometry Geometry;
  const Geometry &geo = testGeos.get(gt);

  Dune::Patches::RefinedQ1FiniteElementFactory<Geometry, double> feFactory(3);
  // const auto &fe = feFactory.make(geo);

  int status = 77;

  acc(status, testBasis<dim>());
  acc(status, testCoefficients<dim>());

  return status;
}

int main(int argc, char **argv)
{
  Dune::MPIHelper::instance(argc, argv);

  int result = 77;

  // acc(result, testRefinedQ1<0>());
  acc(result, testRefinedQ1<1>());
  acc(result, testRefinedQ1<2>());
  acc(result, testRefinedQ1<3>());
  // acc(result, testRefinedQ1<4>());

  return result;
}
