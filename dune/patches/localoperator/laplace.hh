// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_PATCHES_LOCALOPERATOR_LAPLACE_HH
#define DUNE_PATCHES_LOCALOPERATOR_LAPLACE_HH

#include <array>
#include <cassert>
#include <cstddef>
#include <type_traits>
#include <utility>

#if HAVE_TBB
#include "tbb/tbb_stddef.h"
#endif

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/type.hh>

#include <dune/localfunctions/common/interfaceswitch.hh>

#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/pattern.hh>

#include <dune/patches/utility/fvectorops.hh>

namespace Dune {
  namespace Patches {

    //! \addtogroup LocalOperator
    //! \ingroup PDELab
    //! \{

    /** a local operator for solving the Laplace equation
     *
     * \f{align*}{
     *           - \Delta u &=& 0 \mbox{ in } \Omega,          \\
     *  -\nabla u \cdot \nu &=& 0 \mbox{ on } \partial\Omega_N \\
     * \f}
     * with conforming finite elements on all types of grids in any dimension.
     *
     * In other words, it only assembles the Laplace matrix.
     *
     * \tparam feSize Number of dofs in the finite element.  This is used for
     *                statically allocating temporary vectors and matrices.
     *
     * \note This is a reimplentation of Dune::PDELab::Laplace.  Main point is
     *       to avoid allocating std::vectors in the inner loops.  But on top
     *       of that this implementation consistently assumes Galerkin
     *       schemes, and implements jacobian_apply_*.
     */
    template<std::size_t feSize>
    class Laplace :
      public PDELab::FullVolumePattern,
      public PDELab::LocalOperatorDefaultFlags
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = true };

      // residual assembly flags
      enum { doAlphaVolume = true };

      /** \brief Constructor
       *
       * \param quadOrder Order of the quadrature rule used for integrating over the element
       */
      Laplace (unsigned int quadOrder)
      : quadOrder_(quadOrder)
      {}

#if HAVE_TBB
      Laplace(Laplace &other, tbb::split) :
        quadOrder_(other.quadOrder_)
      { }

      void join(Laplace &other)
      { }
#endif // HAVE_TBB

      /** \brief Compute Laplace matrix times a given vector for one element
       *
       * This is used for matrix-free algorithms for the Laplace equation
       *
       * \param [in]  eg The grid element we are assembling on
       * \param [in]  lfsu Local ansatz function space basis
       * \param [in]  lfsv Local test function space basis
       * \param [in]  x Input vector
       * \param [out] r The product of the Laplace matrix times x
       */
      template<typename EG, typename LFSU, typename X, typename LFSV,
               typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        using namespace FieldVectorOps;

        // domain and range field type
        typedef FiniteElementInterfaceSwitch<
          typename LFSU::Traits::FiniteElementType
          > FESwitch;
        typedef BasisInterfaceSwitch<typename FESwitch::Basis> BasisSwitch;
        typedef typename BasisSwitch::DomainField DF;
        typedef typename BasisSwitch::DomainLocal Domain;

        // dimensions
        static const int dimLocal = EG::Geometry::mydimension;
        static const int dimGlobal = EG::Geometry::coorddimension;

        // we assume Galerkin method lfsu=lfsv
        const auto &fe = lfsv.finiteElement();
        const auto &basis = FESwitch::basis(fe);

        // make sure the size matches what we expect.
        assert(feSize == basis.size());
        assert(feSize == FESwitch::basis(lfsu.finiteElement()).size());
        using GradPhiField =
          decltype(std::declval<typename std::decay<decltype(basis)>::type::
                                Traits::JacobianType>()[0][0]
                   * eg.geometry().
                     jacobianInverseTransposed(std::declval<Domain>())[0][0]);
        std::array<Dune::FieldMatrix<GradPhiField,1,dimGlobal>, feSize>
          gradphi;

        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        const auto &rule =
          Dune::QuadratureRules<DF,dimLocal>::rule(gt,quadOrder_);

        // loop over quadrature points
        for(const auto &qp : rule)
        {
          // evaluate gradient of shape functions
          BasisSwitch::gradient(basis, eg.geometry(), qp.position(), gradphi);

          // compute gradient of u
          Dune::FieldVector<decltype(x(lfsu,0)*gradphi[0][0][0]),dimGlobal>
            gradu(0.0);
          for (std::size_t i=0; i < feSize; i++)
            gradu.axpy(x(lfsu,i),gradphi[i][0]);

          // integrate grad u * grad phi_i
          auto factor = qp.weight() *
            eg.geometry().integrationElement(qp.position());
          for (std::size_t i=0; i < feSize; i++)
            r.accumulate(lfsv,i,(gradu*gradphi[i][0])*factor);
        }
      }

      /** \brief Compute the Laplace stiffness matrix for the element given in
       *         'eg'
       *
       * \tparam M Type of the element stiffness matrix
       *
       * \param [in]  eg The grid element we are assembling on
       * \param [in]  lfsu Local ansatz function space basis
       * \param [in]  lfsv Local test function space basis
       * \param [in]  x Current configuration; gets ignored for linear problems like this one
       * \param [out] matrix Element stiffness matrix
       */
      template<typename EG, typename LFSU, typename X, typename LFSV,
               typename M>
      void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x,
                           const LFSV& lfsv, M & matrix) const
      {
        // Switches between local and global interface
        typedef FiniteElementInterfaceSwitch<
          typename LFSU::Traits::FiniteElementType
          > FESwitch;
        typedef BasisInterfaceSwitch<typename FESwitch::Basis> BasisSwitch;

        // domain and range field type
        typedef typename BasisSwitch::DomainField DF;
        typedef typename BasisSwitch::RangeField RF;

        // dimensions
        static const int dimLocal = EG::Geometry::mydimension;
        static const int dimGlobal = EG::Geometry::coorddimension;

        // we assume Galerkin method lfsu=lfsv
        const auto &fe = lfsv.finiteElement();
        const auto &basis = FESwitch::basis(fe);

        // make sure the size matches what we expect.
        assert(feSize == basis.size());
        assert(feSize == FESwitch::basis(lfsu.finiteElement()).size());
        std::array<Dune::FieldMatrix<RF,1,dimGlobal>, feSize> gradphi;

        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        const auto &rule =
          Dune::QuadratureRules<DF,dimLocal>::rule(gt,quadOrder_);

        // loop over quadrature points
        for (const auto &qp: rule)
        {
          BasisSwitch::gradient(basis, eg.geometry(), qp.position(), gradphi);

          // geometric weight
          auto factor = qp.weight()
            * eg.geometry().integrationElement(qp.position());

          for (std::size_t i = 0; i < feSize; i++)
            for (std::size_t j = 0; j < feSize; j++)
              // integrate grad u * grad phi
              matrix.accumulate(lfsv,j,lfsu,i,
                                gradphi[i][0] * gradphi[j][0] * factor);
        }
      }

      // just forward to alpha_volume
      template<typename EG, typename LFSU, typename X, typename LFSV,
               typename Y>
      void jacobian_apply_volume(const EG& eg, const LFSU& lfsu, const X& x,
                                 const LFSV& lfsv, Y& y) const
      {
        alpha_volume(eg, lfsu, x, lfsv, y);
      }

    protected:
      // Quadrature rule order
      unsigned int quadOrder_;
    };

     //! \} group LocalOperator
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_LOCALOPERATOR_LAPLACE_HH
