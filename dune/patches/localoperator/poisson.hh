// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_PATCHES_LOCALOPERATOR_POISSON_HH
#define DUNE_PATCHES_LOCALOPERATOR_POISSON_HH

#include <array>
#include <cassert>
#include <cstddef>

#if HAVE_TBB
#include "tbb/tbb_stddef.h"
#endif

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/type.hh>

#include <dune/localfunctions/common/interfaceswitch.hh>

#include <dune/patches/localoperator/laplace.hh>

namespace Dune {
  namespace Patches {
    //! \addtogroup LocalOperator
    //! \ingroup PDELab
    //! \{

    /** a local operator for solving the Poisson equation
     *
     * \f{align*}{
     *           - \Delta u &=& f \mbox{ in } \Omega,          \\
     *                    u &=& g \mbox{ on } \partial\Omega_D \\
     *  -\nabla u \cdot \nu &=& j \mbox{ on } \partial\Omega_N \\
     * \f}
     * with conforming finite elements on all types of grids in any dimension
     * \tparam F grid function type giving f
     * \tparam B grid function type selecting boundary condition
     * \tparam J grid function type giving j
     * \tparam feSize Number of dofs in the finite element.  This is used for
     *                statically allocating temporary vectors and matrices.
     *
     * \note This is a reimplentation of Dune::PDELab::Poisson.  Main point is
     *       to avoid allocating std::vectors in the inner loops.  But on top
     *       of that this implementation consistently assumes Galerkin
     *       schemes, and avoids numerical default implementations.
     */
    template<typename F, typename B, typename J, std::size_t feSize>
    class Poisson :
      public Laplace<feSize>
    {
      using Base = Laplace<feSize>;

    public:
      // residual assembly flags
      enum { doLambdaVolume = true };
      enum { doLambdaBoundary = true };

      /** \brief Constructor
       *
       * \param quadOrder Order of the quadrature rule used for integrating over the element
       *
       * \note For multithreading evaluating f, bctype, and j must be
       *       thread safe.
       */
      Poisson(const F& f, const B& bctype, const J& j,
              unsigned int quadOrder) :
        Base(quadOrder), f_(f), bctype_(bctype), j_(j)
      { }

#if HAVE_TBB
      Poisson(Poisson &other, tbb::split) :
        Base(other, tbb::split()),
        f_(other.f_), bctype_(other.bctype_), j_(other.j_)
      { }
#endif // HAVE_TBB

      // volume integral depending only on test functions
      template<typename EG, typename LFSV, typename R>
      void lambda_volume(const EG& eg, const LFSV& lfsv, R& r) const
      {
        // domain and range field type
        typedef FiniteElementInterfaceSwitch<
          typename LFSV::Traits::FiniteElementType
          > FESwitch;
        typedef BasisInterfaceSwitch<typename FESwitch::Basis> BasisSwitch;
        typedef typename BasisSwitch::DomainField DF;
        typedef typename BasisSwitch::Range Range;

        // dimensions
        static const int dimLocal = EG::Geometry::mydimension;

        const auto &fe = lfsv.finiteElement();
        const auto &basis = FESwitch::basis(fe);

        // make sure the size matches what we expect.
        assert(feSize == basis.size());
        std::array<Range, feSize> phi;

        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        const auto &rule =
          Dune::QuadratureRules<DF,dimLocal>::rule(gt,this->quadOrder_);

        // loop over quadrature points
        for(const auto &qp : rule)
        {
          // evaluate shape functions
          basis.evaluateFunction(qp.position(),phi);

          // evaluate right hand side parameter function
          auto y = f_(eg.entity(), qp.position());

          // integrate f
          auto factor = - qp.weight() *
            eg.geometry().integrationElement(qp.position());
          for(std::size_t i = 0; i < feSize; i++)
            r.accumulate(lfsv,i, y*phi[i]*factor);
        }
      }

      // boundary integral independen of ansatz functions
      template<typename IG, typename LFSV, typename R>
      void lambda_boundary (const IG& ig, const LFSV& lfsv, R& r) const
      {
        // domain and range field type
        typedef FiniteElementInterfaceSwitch<
          typename LFSV::Traits::FiniteElementType
          > FESwitch;
        typedef BasisInterfaceSwitch<typename FESwitch::Basis> BasisSwitch;
        typedef typename BasisSwitch::DomainField DF;
        typedef typename BasisSwitch::Range Range;

        // dimensions
        static const int dimLocal = IG::Geometry::mydimension;

        // we assume Galerkin method lfsu=lfsv
        const auto &fe = lfsv.finiteElement();
        const auto &basis = FESwitch::basis(fe);

        // make sure the size matches what we expect.
        assert(feSize == basis.size());
        std::array<Range, feSize> phi;

        // select quadrature rule
        Dune::GeometryType gtface = ig.geometryInInside().type();
        const auto &rule =
          Dune::QuadratureRules<DF,dimLocal>::rule(gtface,this->quadOrder_);

        // loop over quadrature points and integrate normal flux
        for (const auto &qp : rule)
        {
          // evaluate boundary condition type
          // skip rest if we are on Dirichlet boundary
          if( !bctype_.isNeumann(ig, qp.position()) )
            continue;

          // position of quadrature point in local coordinates of element
          const auto& local = ig.geometryInInside().global(qp.position());

          // evaluate test shape functions
          basis.evaluateFunction(local,phi);

          // evaluate flux boundary condition
          typename J::Traits::RangeType y(0.0);
          j_.evaluate(ig.inside(),local,y);

          // integrate J
          auto factor = qp.weight() *
            ig.geometry().integrationElement(qp.position());
          for (std::size_t i = 0; i < feSize; i++)
            r.accumulate(lfsv,i, y*phi[i]*factor);
        }
      }

    protected:
      const F& f_;
      const B& bctype_;
      const J& j_;

    };

    //! \} group LocalOperator
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_LOCALOPERATOR_POISSON_HH
