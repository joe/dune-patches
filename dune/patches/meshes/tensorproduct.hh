#ifndef DUNE_PATCHES_MESHES_TENSORPRODUCT_HH
#define DUNE_PATCHES_MESHES_TENSORPRODUCT_HH

#include <algorithm>
#include <cassert>
#include <iterator>
#include <memory>
#include <vector>

#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>

#include <dune/grid/common/gridfactory.hh>

#include <dune/patches/utility/blockiteration.hh>
#include <dune/patches/utility/cmp.hh>
#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace Meshes {

      template<class Grid, class Lower, class Upper, class Div>
      void tensorProduct(GridFactory<Grid> &factory, const Lower &lower,
                         const Upper &upper, const Div &division)
      {
        using std::begin;
        using std::end;

        static_assert(eq(Grid::dimension, Grid::dimensionworld),
                      "This function requiers dimension == dimensionworld");
        constexpr unsigned dim = Grid::dimension;
        using ctype = typename Grid::ctype;
        using CF = Dune::FieldVector<ctype, dim>;

        { // vertices
          CF step;
          for(auto d : range(dim))
            step[d] = (upper[d] - lower[d]) / division[d];

          Div counter;
          std::fill(begin(counter), end(counter), 0);

          unsigned vertices = 1;
          Div limit;
          for(auto d : range(dim))
          {
            limit[d] = division[d] + 1;;
            vertices *= limit[d];
          }

          do {
            CF pos = lower;
            for(auto d : range(dim))
              pos[d] += counter[d] * step[d];
            factory.insertVertex(pos);
          } while(iterateBlock(limit, counter));
        }

        { // elements
          Div origin;
          std::fill(begin(origin), end(origin), 0);

          Dune::GeometryType gt;
          gt.makeCube(dim);

          constexpr unsigned verticesPerElement = 1 << dim;
          std::vector<unsigned> elementVertices;
          elementVertices.reserve(verticesPerElement);

          unsigned elements = 1;
          for(auto div : division)
            elements *= div;

          do {
            elementVertices.clear();

            Div limit = origin;
            for(auto d : range(dim))
              limit[d] += 2;

            Div cornerpos = origin;
            // iterate over corners
            do {
              // compute index of corner
              unsigned index = 0;
              unsigned stride = 1;
              for(auto d : range(dim))
              {
                index += cornerpos[d] * stride;
                stride *= (division[d] + 1);
              }
              elementVertices.push_back(index);
            } while(iterateBlock(origin, limit, cornerpos));
            assert(elementVertices.size() == verticesPerElement);

            factory.insertElement(gt, elementVertices);
          } while(iterateBlock(division, origin));
        }
      }

      template<class Grid, class Lower, class Upper, class Div>
      std::unique_ptr<Grid>
      tensorProduct(const Lower &lower, const Upper &upper,
                    const Div &division)
      {
        GridFactory<Grid> factory;
        tensorProduct(factory, lower, upper, division);
        return { factory.createGrid() };
      }

    } // namespace Dune
  } // namespace Patches
} // namespace Meshes

#endif // DUNE_PATCHES_MESHES_TENSORPRODUCT_HH
