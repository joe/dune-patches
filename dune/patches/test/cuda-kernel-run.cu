#include <config.h>

#include <iostream>
#include <ostream>
#include <vector>

__global__ void kernel(int size, int *fill)
{
  int index = blockDim.x * blockIdx.x + threadIdx.x;
  if(index < size)
    fill[index] = index;
}

int main()
{
  int bufsize = 1024;
  int dimblock = 64;
  cudaError error;
  int *devbuf = 0;

  // allocate buffer
  error = cudaMalloc(&devbuf, bufsize * sizeof (int));
  if(error == cudaErrorNoDevice) {
    std::cerr << "No Cuda-devices found, skipping" << std::endl;
    return 77;
  }
  if(error != cudaSuccess) {
    std::cout << "Error: allocating memory: " << cudaGetErrorString(error)
              << std::endl;
    return 1;
  }

  // run kernel
  kernel<<<(bufsize-1)/dimblock+1, dimblock>>>(bufsize, devbuf);
  error = cudaGetLastError();
  if(error != cudaSuccess) {
    std::cout << "Error: running kernel: " << cudaGetErrorString(error)
              << std::endl;
    return 1;
  }

  // copy data
  std::vector<int> hostbuf(bufsize);
  error = cudaMemcpy(hostbuf.data(), devbuf, bufsize * sizeof (int),
                     cudaMemcpyDeviceToHost);
  if(error != cudaSuccess) {
    std::cout << "Error: copying buffer: " << cudaGetErrorString(error)
              << std::endl;
    return 1;
  }

  // free device buffer
  error = cudaFree(devbuf);
  if(error != cudaSuccess) {
    std::cout << "Error: freeing buffer: " << cudaGetErrorString(error)
              << std::endl;
    return 1;
  }

  // verify data
  int result = 0;
  for(int i = 0; i < bufsize; ++i)
    if(hostbuf[i] != i)
    {
      result = 1;
      std::cerr << "Error: expected " << i << " got " << hostbuf[i]
                << std::endl;
    }
  return result;
}
