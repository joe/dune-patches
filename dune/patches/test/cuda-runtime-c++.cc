#include <config.h>

#include <iostream>
#include <ostream>

#include <cuda.h>
#include <cuda_runtime.h>

int main()
{
  cudaError error;
  int deviceCount = 0;
  error = cudaGetDeviceCount(&deviceCount);
  if(error != cudaSuccess && error != cudaErrorNoDevice) {
    std::cout << " [" << cudaGetErrorString(error) << "]";
    return 1;
  }
  std::cout << "Cuda Devices: " << deviceCount;
}
