#include <config.h>

// make sure assert() actually does something
#ifdef NDEBUG
#undef NDEBUG
#endif

#include <cassert>
#include <iostream>
#include <limits>
#include <ostream>

#include <stdio.h>

#include <dune/common/classname.hh>
#include <dune/common/cudacompat/fvector.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/exceptions.hh>

template<class ft, class rt, int d>
bool FieldVectorMainTest() {
  using namespace Dune::CudaCompat;

  bool result = true;

  FieldVector<ft,d> v(1);
  FieldVector<ft,d> w(2);
  FieldVector<ft,d> z(2);
  ft a = 1;
  bool b DUNE_UNUSED;
  rt n DUNE_UNUSED;

  // test element access
  if(v[0] != ft(1))
    result = false;
  v[0] = 2;
  if(v[0] != ft(2))
    result = false;
  v[0] = 1;
  if(v[0] != ft(1))
    result = false;

  //test op(vec,vec)
  z = v + w;
  z = v - w;

  // test op(vec,scalar)
  w += a;
  w -= a;
  w *= a;
  v = w * a;
  v = a * w;
  //w /= a;

  // Test whether the norm methods compile
  n = (w+v).two_norm();
  n = (w+v).two_norm2();
  n = (w+v).one_norm();
  n = (w+v).infinity_norm();

  // test scalar product, axpy
  a = v * w;

  a = v.dot(w);
  z = v.axpy(a,w);

  // test comparison
  b = (w != v);
  b = (w == v);

  return result;
}

template<class ft, class rt, int d>
bool test_on_host()
{
  return FieldVectorMainTest<ft,rt,d>();
}

template<class ft, int d>
struct FieldVectorTest
{
  FieldVectorTest()
  {
    // the second level of () is needed since there are , in the template
    // arguments and assert is a macro that is handled by the preprocessor and
    // the preprocessor does not know about template brackets so interprets
    // the , a macro argument delimeters
    assert( (test_on_host<ft,ft,d>()) );
  }
};

int main()
{
  std::cout << "Test CUDA Field Vector" << std::endl;
  try {
    FieldVectorTest<int, 3>();
    FieldVectorTest<double, 3>();

    return 0;
  } catch (Dune::Exception& e) {
    std::cerr << e << std::endl;
    return 1;
  } catch (...) {
    std::cerr << "Generic exception!" << std::endl;
    return 2;
  }
}
