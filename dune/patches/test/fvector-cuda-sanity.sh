#!/bin/sh

echo "Checking that cuda-test fails with all devices hidden"
if ( set -x
     CUDA_VISIBLE_DEVICES=-1 ./fvector-cuda
   )
then echo "fvector-cuda passed unexpectedly"
     exit 1
else echo "fvector-cuda failed as expected"
     exit 0
fi
