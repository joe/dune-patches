#ifndef DUNE_PATCHES_UTILITY_BASE_FROM_MEMBER_HH
#define DUNE_PATCHES_UTILITY_BASE_FROM_MEMBER_HH

namespace Dune {
  namespace Patches {

    //! Base-from-Member idiom
    /**
     * Sometimes it is necessary to initilaize a base class using a member.
     * This is not possible, but it is often possible to move the member into
     * its own base class and use multiple inheritance to achive the same
     * result.  Usage:
     *
     * \code
     *   struct Base {
     *     Base(int &data);
     *   };
     *   struct Derived :
     *     private BaseFromMember<int>,
     *     public Base
     *   {
     *     Derived() :
     *       BaseFromMember<int>{ 0 },
     *       Base(BaseFromMember<int>::mem)
     *     { }
     *   };
     * \endcode
     *
     * The optional unsigned template parameter is to distinguish between
     * multiple \c BaseFromMember bases with the same \c T.
     *
     * \note The idea was stolen from Boost.
     */
    template<class T, unsigned = 0>
    struct BaseFromMember {
      T mem;
    };

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_BASE_FROM_MEMBER_HH
