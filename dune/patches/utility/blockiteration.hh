#ifndef DUNE_PATCHES_UTILITY_BLOCKITERATION_HH
#define DUNE_PATCHES_UTILITY_BLOCKITERATION_HH

#include <iterator>

namespace Dune {
  namespace Patches {

    //! Iterate over a block of points (with the lower corner of the origin)
    /**
     * Equivalent to the three-argument version of \c iterateBlock() with \c
     * lower=0 and \c upper=limit.
     *
     * \code
     * auto lower = limit;
     * std::fill(begin(lower), end(lower), 0);
     * return iterateBlock(lower, limit, counter);
     * \endcode
     */
    template<class T>
    bool iterateBlock(const T &limit, T &counter)
    {
      using std::begin;
      using std::end;

      auto li = begin(limit);
      for(auto ci = begin(counter); ci != end(counter); ++li, ++ci)
      {
        ++*ci;
        if(*ci < *li)
          return true;
        else
          *ci = 0;
      }
      return false;
    }

    //! Iterate over a block of points
    /**
     * \param lower   Lower (inclusive) end of the block
     * \param upper   Upper (exclusive) end of the block
     * \param counter position inside the block to move.
     *
     * \tparam T Some sequence of integers.  For some object \c t of type \c
     *           T, \c begin(t) and \c end(t) should work as expected.
     *
     * \return \c true unless a wrap around ocurred.
     *
     * Move counter to the next point in the block, varying \c *begin(counter)
     * fastest.  Iterating the last point wraps around to \c lower and returns
     * \c false, otherwise true is returned.
     *
     * Typical usage:
     * \code
     * // set up lower and upper
     * auto counter = lower;
     * do {
     *   // do something with counter
     * } while(iterateBlock(lower, upper, counter));
     * \endcode
     *
     * \note The block must contain a positive number of points.
     */
    template<class T>
    bool iterateBlock(const T &lower, const T &upper, T &counter)
    {
      using std::begin;
      using std::end;

      auto li = begin(lower);
      auto ui = begin(upper);
      for(auto ci = begin(counter); ci != end(counter); ++li, ++ui, ++ci)
      {
        ++*ci;
        if(*ci < *ui)
          return true;
        else
          *ci = *li;
      }
      return false;
    }

    //! \brief Iterate over a block of points (with the lower corner of the
    //!        origin) in reverse
    /**
     * Equivalent to the three-argument version of \c reverseIterateBlock()
     * with \c lower=0 and \c upper=limit.
     *
     * \code
     * auto lower = limit;
     * std::fill(begin(lower), end(lower), 0);
     * return reverseIterateBlock(lower, limit, counter);
     * \endcode
     */
    template<class T>
    bool reverseIterateBlock(const T &limit, T &counter)
    {
      using std::begin;
      using std::end;

      auto li = begin(limit);
      for(auto ci = begin(counter); ci != end(counter); ++li, ++ci)
      {
        if(*ci == 0)
          *ci = *li - 1;
        else
        {
          --*ci;
          return true;
        }
      }
      return false;
    }

    //! Iterate over a block of points in reverse
    /**
     * \param lower   Lower (inclusive) end of the block
     * \param upper   Upper (exclusive) end of the block
     * \param counter position inside the block to move.
     *
     * \tparam T Some sequence of integers.  For some object \c t of type \c
     *           T, \c begin(t) and \c end(t) should work as expected.
     *
     * \return \c true unless a wrap around ocurred.
     *
     * Move counter to the previous point in the block, varying \c
     * *begin(counter) fastest.  Iterating the first point wraps around to the
     * last point and returns \c false, otherwise true is returned.
     *
     * Typical usage:
     * \code
     * // set up lower and upper
     * // initialize counter to last point in block:
     * auto counter = lower;
     * reverseIterateBlock(lower, upper, counter);
     * // do the loop
     * do {
     *   // do something with counter
     * } while(reverseIterateBlock(lower, upper, counter));
     * \endcode
     *
     * \note The block must contain a positive number of points.
     */
    template<class T>
    bool reverseIterateBlock(const T &lower, const T &upper, T &counter)
    {
      using std::begin;
      using std::end;

      auto li = begin(lower);
      auto ui = begin(upper);
      for(auto ci = begin(counter); ci != end(counter); ++li, ++ui, ++ci)
      {
        if(*ci == *li)
          *ci = *ui - 1;
        else
        {
          --*ci;
          return true;
        }
      }
      return false;
    }

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_BLOCKITERATION_HH
