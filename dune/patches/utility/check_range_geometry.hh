#ifndef DUNE_PATCHES_UTILITY_CHECK_RANGE_GEOMETRY_HH
#define DUNE_PATCHES_UTILITY_CHECK_RANGE_GEOMETRY_HH

#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iterator>
#include <limits>
#include <ostream>
#include <string>
#include <type_traits>

#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {

    //! \brief iterate over a range of entities and check that all corners and
    //!        the center of the geometries are in the given bounding box
    template<class Range, class Coord>
    bool check_range_geometry(const Range &view_range, const Coord &lower,
                              const Coord &upper)
    {
      auto isInside = [&lower,&upper](const Coord &pos) -> bool {
        assert(lower.size() == pos.size() && upper.size() == pos.size());
        for(auto d : range(pos.size()))
          if(lower[d] > pos[d] || pos[d] > upper[d])
            return false;
        return true;
      };
      bool result = true;

      unsigned runningIndex = 0;
      for(const auto &e : view_range)
      {
        const auto &geo = e.geometry();
        for(auto corner : range(geo.corners()))
          if(!isInside(geo.corner(corner)))
          {
            result = false;
            std::cerr << "Corner outside for (e=" << runningIndex
                      << ", corner=" << corner << "): pos=("
                      << geo.corner(corner) << ") bbox=(" << lower << "; "
                      << upper << ")" << std::endl;
          }
        if(!isInside(geo.center()))
        {
          result = false;
          std::cerr << "Center outside for (e=" << runningIndex << "): pos=("
                    << geo.center() << ") bbox=(" << lower << "; " << upper
                    << ")" << std::endl;
        }
        ++runningIndex;
      }

      return result;
    }

    //! \brief iterate over a range of entities and check that all corners and
    //!        the center of the geometries are in the given bounding box
    /**
     * \param result Result accumulator for a unit test.  Set to 0 (for
     *               success) or 1 (for fail), but do not change a fail to a
     *               pass.  77 counts as pass, but we never set that.
     * \param msg    Message to print to identify the test.
     */
    template<class Range, class Coord>
    void check_range_geometry(int &result, const std::string &msg,
                              const Range &range, const Coord &lower,
                              const Coord &upper)
    {
      std::cout << "checking " << msg << "..." << std::endl;
      if(check_range_geometry(range, lower, upper))
      {
        std::cout << "checking " << msg << "... ok" << std::endl;
        if(result == 77)
          result = 0;
      }
      else
      {
        std::cout << "checking " << msg << "... error" << std::endl;
        result = 1;
      }
    }

    //! \brief iterate over a range of entities and check that all corners and
    //!        the center of the geometries are in the given bounding box
    /**
     * \param result Result accumulator for a unit test.  Set to 0 (for
     *               success) or 1 (for fail), but do not change a fail to a
     *               pass.  77 counts as pass, but we never set that.
     * \param msg    Message to print to identify the test.
     *
     * \note Uses a default bounding box [0, 1]^dim, grown slightly to make
     *       sure numeric accuracy errors do not cause false check failures.
     */
    template<class Range>
    void check_range_geometry(int &result, const std::string &msg,
                              const Range &range)
    {
      using std::begin;
      using std::end;
      using std::sqrt;

      using Coord =
        typename std::decay<decltype(begin(range)->geometry().center())>::type;
      Coord lower, upper;

      using ctype = typename std::decay<decltype(lower[0])>::type;
      ctype rho = sqrt(std::numeric_limits<ctype>::epsilon());

      std::fill(begin(lower), end(lower), -rho);
      std::fill(begin(upper), end(upper), ctype(1)+rho);

      check_range_geometry(result, msg, range, lower, upper);
    }

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_CHECK_RANGE_GEOMETRY_HH
