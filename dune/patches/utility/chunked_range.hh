#ifndef DUNE_PATCHES_UTILITY_CHUNKED_RANGE_HH
#define DUNE_PATCHES_UTILITY_CHUNKED_RANGE_HH

#include <cassert>
#include <cstddef>
#include <iterator>
#include <type_traits>
#include <utility>

#include <dune/common/iteratorrange.hh>

namespace Dune {
  namespace Patches {

    //! Iterator over chunks of some other range
    /**
     * Dereferencing yields an iterator range over the chunk.  The value
     * obtained by dereferencing may not by assigned to.
     *
     * This iterator claims the same category as the iterator T.  However, it
     * is not possible for us to satisfy the ForwardIterator requirement that
     * reference is const value_type &.
     */
    template<class T,
             typename std::iterator_traits<T>::difference_type chunkSize>
    class ChunkIterator
    {
      T t;

    public:
      using value_type = Dune::IteratorRange<T>;
      using difference_type =
        typename std::iterator_traits<T>::difference_type;
      class pointer;
      using reference = const value_type;
      using iterator_category =
        typename std::iterator_traits<T>::iterator_category;

      ChunkIterator() = default;
      template<class U>
      ChunkIterator(U&& u) : t(std::forward<U>(u)) { }

      // 24.2.2 Iterator
      reference operator*() const { return { t, std::next(t, chunkSize) }; }
      ChunkIterator &operator++() { std::advance(t, chunkSize); return *this; }

      // 24.2.3 InputIterator
      bool operator==(const ChunkIterator &o) const { return t == o.t; }
      bool operator!=(const ChunkIterator &o) const { return !(*this == o); }
      pointer operator->() const { return { **this }; }
      ChunkIterator operator++(int) { auto tmp = *this; ++*this; return tmp; }

      // 24.2.6 BidirectionalIterator
      ChunkIterator &operator--()
      { std::advance(t, -chunkSize); return *this; }
      ChunkIterator operator--(int) { auto tmp = *this; --*this; return tmp; }

      // 24.2.7 RandomAccessIterator
      ChunkIterator &operator+=(difference_type n) {
        std::advance(t, n * chunkSize);
        return *this;
      }
      ChunkIterator operator+(difference_type n) const {
        auto tmp = *this;
        tmp += n;
        return tmp;
      }
      friend ChunkIterator operator+(difference_type n, const ChunkIterator &o)
      { return o + n; }
      ChunkIterator &operator-=(difference_type n) {
        std::advance(t, -n * chunkSize);
        return *this;
      }
      ChunkIterator operator-(difference_type n) const {
        auto tmp = *this;
        tmp -= n;
        return tmp;
      }
      difference_type operator-(const ChunkIterator &o) const {
        return std::distance(t, o.t)/chunkSize;
      }
      reference operator[](difference_type n) const { return *(*this + n); }
      bool operator<(const ChunkIterator &o) const { return t < o.t; }
      bool operator>(const ChunkIterator &o) const { return t > o.t; }
      bool operator<=(const ChunkIterator &o) const { return t <= o.t; }
      bool operator>=(const ChunkIterator &o) const { return t >= o.t; }
    };

    template<class T,
             typename std::iterator_traits<T>::difference_type chunkSize>
    class ChunkIterator<T, chunkSize>::pointer
    {
      const value_type range_;

      // hide all constructors and make ChunkIterator a friend.  This should
      // increase the likelyhood that objects of type pointer to not escape
      // from expressions.  Anyone who tries ci.operator->() without
      // immediately dereferencing the result should get an error in most
      // cases.
      friend class ChunkIterator;
      pointer() = default;
      pointer(const pointer &) = default;
      pointer(pointer &&) = default;
      pointer& operator=(const pointer &) = delete;
      pointer& operator=(pointer &&) = delete;
      pointer(value_type &&range) : range_(std::move(range)) { }

    public:
      const value_type *operator->() const { return &range_; }
    };

    template<std::size_t chunkSize, class Iterator>
    ChunkIterator<typename std::decay<Iterator>::type, chunkSize>
    chunkIterator(Iterator &&i)
    { return std::forward<Iterator>(i); }

    namespace chunkedImpl {
      using std::begin;
      using std::end;

      template<std::size_t chunkSize, class Range>
      auto chunked(const Range &range)
        -> Dune::IteratorRange<decltype(chunkIterator<chunkSize>(begin(range)))>
      {
        auto first = begin(range);
        auto last = end(range);
        // can't do the assert unless the iterators give multi-pass guarantee
        if(std::is_convertible<
             typename std::iterator_traits<decltype(first)>::iterator_category,
             std::forward_iterator_tag>::value)
          assert(std::distance(first, last) % chunkSize == 0);
        return {
          chunkIterator<chunkSize>(std::move(first)),
          chunkIterator<chunkSize>(std::move(last))
        };
      }
    } // namespace chunkedImpl

    using chunkedImpl::chunked;

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_CHUNKED_RANGE_HH
