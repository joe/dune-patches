#ifndef DUNE_PATCHES_UTILITY_CMP_HH
#define DUNE_PATCHES_UTILITY_CMP_HH

#include <type_traits>

namespace Dune {
  namespace Patches {

    //! equality, avoiding signed/unsigned-warnings
    /**
     * \warning By using this function you declare you know what you are doing
     *          and are effectively shutting up signed/unsigned warnings.
     */
    template<class L, class R>
    constexpr bool eq(L l, R r)
    {
      // compare, avoiding signed/unsigned-warnings
      typedef typename std::common_type<L, R>::type T;
      return T(l) == T(r);
    }

    //! equality, avoiding signed/unsigned-warnings
    /**
     * \warning By using this function you declare you know what you are doing
     *          and are effectively shutting up signed/unsigned warnings.
     */
    template<class L, class R>
    constexpr bool ne(L l, R r)
    {
      return !eq(l, r);
    }

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_CMP_HH
