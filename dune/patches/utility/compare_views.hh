#ifndef DUNE_PATCHES_UTILITY_COMPARE_VIEWS_HH
#define DUNE_PATCHES_UTILITY_COMPARE_VIEWS_HH

#include <cmath>
#include <cstddef>
#include <iostream>
#include <limits>
#include <ostream>
#include <string>
#include <type_traits>

#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {

    //! \brief iterate over the elements of two views in parallel and check
    //!        that all corner positions match
    /**
     * \note This relies on the iteration order of the grid range beeing
     *       imposed on the patch, which is something that we don't actually
     *       guarantee.  However, it is the case for now, so we use it in this
     *       test.
     *
     * Coordinates are considered matching if their distance is smaller than
     * rho = sqrt(epsilon(ctype)), where epsilon is obtained from \c
     * std::numeric_limits.
     */
    template<class View1, class View2>
    bool compare_views(const View1 &view1, const View2 &view2)
    {
      using std::sqrt;
      using ctype1 = typename View1::ctype;
      using ctype2 = typename View2::ctype;
      static_assert(std::is_same<ctype1, ctype2>::value,
                    "Both views must have the same ctype, at least");
      const auto rho = sqrt(std::numeric_limits<ctype1>::epsilon());

      bool result = true;

      auto it1 = view1.template begin<0>();
      auto it2 = view2.template begin<0>();
      auto end1 = view1.template end<0>();
      auto end2 = view2.template end<0>();

      for(std::size_t runningIndex = 0;
          it1 != end1 && it2 != end2;
          ++runningIndex, ++it1, ++it2)
      {
        const auto &geo1 = it1->geometry();
        const auto &geo2 = it2->geometry();

        if(geo1.corners() == geo2.corners())
        {
          for(auto c : range(geo1.corners()))
          {
            const auto &corner1 = geo1.corner(c);
            const auto &corner2 = geo2.corner(c);

            if((corner1 - corner2).two_norm() > rho)
            {
              result = false;
              std::cerr << "Corner mismatch for (e=" << runningIndex << ", c="
                        << c << "): view1=(" << corner1 << ") view2=("
                        << corner2 << ")" << std::endl;
            }
          }
        }
        else // it1->type() != it2->type()
        {
          result = false;
          std::cerr << "Geometry #corners mismatch: view1=" << geo1.corners()
                    << " view2=" << geo2.corners() << std::endl;
        }
      }

      if(it1 != end1) {
        result = false;
        std::cerr << "View1 iteration did not reach end." << std::endl;
      }

      if(it2 != end2) {
        result = false;
        std::cerr << "View2 iteration did not reach end." << std::endl;
      }

      return result;
    }

    //! \brief iterate over the elements of two views in parallel and check
    //!        that all corner positions match
    /**
     * \note This relies on the iteration order of the grid range beeing
     *       imposed on the patch, which is something that we don't actually
     *       guarantee.  However, it is the case for now, so we use it in this
     *       test.
     *
     * \param result Result accumulator for a unit test.  Set to 0 (for
     *               success) or 1 (for fail), but do not change a fail to a
     *               pass.  77 counts as pass, but we never set that.
     * \param msg    Message to print to identify the test.
     */
    template<class View1, class View2>
    void compare_views(int &result, const std::string &msg, const View1 &view1,
                       const View2 &view2)
    {
      std::cout << "checking " << msg << "..." << std::endl;
      if(compare_views(view1, view2))
      {
        std::cout << "checking " << msg << "... ok" << std::endl;
        if(result == 77)
          result = 0;
      }
      else
      {
        std::cout << "checking " << msg << "... error" << std::endl;
        result = 1;
      }
    }

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_COMPARE_VIEWS_HH
