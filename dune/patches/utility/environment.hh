#ifndef DUNE_PATCHES_UTILITY_ENVIRONMENT_HH
#define DUNE_PATCHES_UTILITY_ENVIRONMENT_HH

#include <cstdlib>
#include <iostream>
#include <locale>
#include <map>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include <dune/common/classname.hh>
#include <dune/common/exceptions.hh>

namespace Dune {
  namespace Patches {

    template<class T>
    T fromEnv(const std::string &name, T def)
    {
      const char * val = std::getenv(name.c_str());
      if(val == nullptr || val[0] == '\0')
      {
        std::clog << "Using default value ENV{" << name << "}=" << def
                  << std::endl;
        return def;
      }

      std::istringstream str(val);
      T result;
      str >> result;
      if(str)
      {
        char dummy;
        str >> dummy;
        if(!str)
        {
          std::clog << "Using value ENV{" << name << "}=" << result
                    << std::endl;
          return result;
        }
      }
      DUNE_THROW(Dune::Exception, "Error: cannot convert ENV{" << name << "}='"
                 << val << "' to " << Dune::className<T>());
    }

    bool fromEnv(const std::string &name, bool def)
    {
      static const std::string display[] = { "no", "yes" };
      const char * val = std::getenv(name.c_str());
      if(val == nullptr || val[0] == '\0')
      {
        std::clog << "Using default value ENV{" << name << "}=" << display[def]
                  << std::endl;
        return def;
      }

      std::string key = val;
      std::use_facet<std::ctype<char> >(std::locale::classic()).
        tolower(&*key.begin(), &*key.begin()+key.size());
      static const std::map<std::string, bool> strings{
        { "true",  true  }, { "false", false },
        { "yes",   true  }, { "no",    false },
        { "y",     true  }, { "n",     false },
        { "on",    true  }, { "off",   false }
      };
      try {
        bool result = strings.at(key);
        std::clog << "Using value ENV{" << name << "}=" << display[result]
                  << std::endl;
        return result;
      }
      catch(std::out_of_range) { }

      std::istringstream str(val);
      int intresult;
      str >> intresult;
      if(str)
      {
        char dummy;
        str >> dummy;
        if(!str)
        {
          bool result = intresult;
          std::clog << "Using value ENV{" << name << "}=" << display[result]
                    << std::endl;
          return result;
        }
      }
      DUNE_THROW(Dune::Exception, "Error: cannot convert ENV{" << name << "}='"
                 << val << "' to bool");
    }

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_ENVIRONMENT_HH
