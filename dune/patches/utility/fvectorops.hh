#ifndef DUNE_PATCHES_UTILITY_FVECTOROPS_HH
#define DUNE_PATCHES_UTILITY_FVECTOROPS_HH

#include <dune/common/fvector.hh>

#include <dune/patches/utility/range.hh>

namespace Dune {
  namespace Patches {
    namespace FieldVectorOps {

      template<class T1, class T2, int SIZE>
      auto operator*(const FieldVector<T1, SIZE> &a,
                     const FieldVector<T2, SIZE> &b)
        -> decltype(a[0]*b[0])
      {
        decltype(a[0]*b[0]) acc = 0;
        for(auto i : range(SIZE))
          acc += a[i]*b[i];
        return acc;
      }

    } // namespace FieldVectorOps
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_FVECTOROPS_HH
