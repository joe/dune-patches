#ifndef DUNE_PATCHES_UTILITY_GRIDVIEWPATCHFACTORY_HH
#define DUNE_PATCHES_UTILITY_GRIDVIEWPATCHFACTORY_HH

#include <algorithm>
#include <cassert>
#include <iterator>
#include <map>
#include <memory>
#include <utility>
#include <vector>

#include <dune/geometry/type.hh>

#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/common/mcmgmapper.hh>
#include <dune/grid/common/rangegenerators.hh>

#include <dune/patches/utility/cmp.hh>

namespace Dune {
  namespace Patches {

    template<class View>
    class GridViewPatchFactory
    {
      using Mapper =
        MultipleCodimMultipleGeomTypeMapper<View, MCMGVertexLayout>;

      Mapper mapper_;

      /**
       * \param nodeMap Map from grid index according to mapper to insertion
       *                index in patch
       */
      template<class Patch, class Range>
      void fill(Dune::GridFactory<Patch> &factory, const Range &range,
                std::map<unsigned, typename Patch::Index> &nodeMap)
      {
        static_assert(eq(Patch::dimension, View::dimension) &&
                      eq(Patch::dimensionworld, View::dimensionworld),
                      "Dimension of View and Patch do not match");

        using std::begin;
        constexpr unsigned dim = View::dimension;

        GeometryType gt;
        gt.makeCube(dim);

        nodeMap.clear();
        std::vector<unsigned> vertices(1 << dim);
        for(const auto &e : range)
        {
          assert(e.type() == gt);
          const auto &geo = e.geometry();
          for(unsigned i = 0; i < (1 << dim); ++i)
          {
            auto gridIndex = mapper_.subIndex(e, i, dim);
            auto result =
              nodeMap.insert(std::make_pair(gridIndex, nodeMap.size()));
            if(result.second)
              factory.insertVertex(geo.corner(i));
            vertices[i] = result.first->second;
          }
          factory.insertElement(gt, vertices);
        }
      }

    public:
      GridViewPatchFactory(const View &view) :
        mapper_(view)
      { }

      template<class Patch, class Range>
      void fill(Dune::GridFactory<Patch> &factory, const Range &range)
      {
        std::map<unsigned, typename Patch::Index> nodeMap;
        fill(factory, range, nodeMap);
      }

      template<class Patch, class Range>
      std::unique_ptr<Patch>
      create(const Range &range)
      {
        static_assert(eq(Patch::dimension, View::dimension) &&
                      eq(Patch::dimensionworld, View::dimensionworld),
                      "Dimension of View and Patch do not match");

        GridFactory<Patch> factory;
        fill(factory, range);
        return factory.createGrid();
      }

      template<class Patch, class Range>
      std::unique_ptr<Patch>
      createAndMapVertices(const Range &range,
                           std::vector<unsigned> &vertexMap)
      {
        using std::begin;
        using std::end;

        static_assert(eq(Patch::dimension, View::dimension) &&
                      eq(Patch::dimensionworld, View::dimensionworld),
                      "Dimension of View and Patch do not match");

        GridFactory<Patch> factory;
        // Map from insertion index in patch to grid index according to mapper
        std::vector<unsigned> inverseMap;
        {
          // Map from grid index according to mapper to insertion index in
          // patch
          std::map<unsigned, typename Patch::Index> nodeMap;
          fill(factory, range, nodeMap);

#ifdef NDEBUG
          inverseMap.resize(nodeMap.size());
#else
          // debug mode: fill with marker value
          inverseMap.resize(nodeMap.size(), unsigned(-1));
#endif
          for(const auto &rel : nodeMap)
          {
            assert(rel.second < inverseMap.size());
            inverseMap[rel.second] = rel.first;
          }
          // debug mode: check that there are no marker values left
          assert(std::none_of(begin(inverseMap), end(inverseMap),
                              [](unsigned i) { return i == unsigned(-1); }));
          // release nodeMap
        }

        std::unique_ptr<Patch> patchp(factory.createGrid());
        auto pv = patchp->macroGridView();
        auto is = pv.indexSet();
#ifdef NDEBUG
        vertexMap.clear();
        vertexMap.resize(pv.size(Patch::dimension));
#else
        // debug mode: fill with marker value
        vertexMap.assign(pv.size(Patch::dimension), unsigned(-1));
#endif
        for(const auto &v : vertices(pv))
          vertexMap[is.index(v)] = inverseMap[factory.insertionIndex(v)];
        // debug mode: check that there are no marker values left
        assert(std::none_of(begin(vertexMap), end(vertexMap),
                            [](unsigned i) { return i == unsigned(-1); }));

        return patchp;
      }
    };

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_GRIDVIEWPATCHFACTORY_HH
