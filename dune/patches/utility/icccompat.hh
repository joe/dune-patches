#ifndef DUNE_PATCHES_UTILITY_ICCCOMPAT_HH
#define DUNE_PATCHES_UTILITY_ICCCOMPAT_HH

#ifdef __MIC__
#define iccmicprivate public
#else
#define iccmicprivate private
#endif

#endif // DUNE_PATCHES_UTILITY_ICCCOMPAT_HH
