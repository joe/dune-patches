#ifndef DUNE_PATCHES_UTILITY_INTEGRAL_ITERATOR_HH
#define DUNE_PATCHES_UTILITY_INTEGRAL_ITERATOR_HH

#include <type_traits>

#include <dune/common/iteratorfacades.hh>

namespace Dune {
  namespace Patches {

    //! Iterator over an integral range
    /**
     * With unsigned types, this may be used for both forward and backward
     * iteration.  This is because unsigned arithmetic does not overflow
     * according to the standard, rather, it is modulo to the range of the
     * type.  It is impossible to iterate through the whole range of an
     * unsigned integer with the standard idioms using this iterator, because
     * the begin and the passed-the-end iterator for such a range are
     * identical.
     *
     * With signed types overflows and underflows do occur.  So advancing the
     * iterator beyond the largest or smallest integer value must be avoided.
     * Not even passed-the-end iterators may lie outside this range.
     *
     * Taking the distance between two iterators results in
     * implementation-defined behaviour if the result falls outside the range
     * of values of the difference_type.
     */
    template<class T, class = void>
    class IntegralIterator;
    template<class T>
    class IntegralIterator<
        T, typename std::enable_if<std::is_integral<T>::value>::type> :
      public Dune::RandomAccessIteratorFacade<
        IntegralIterator<T>, const T, const T,
        typename std::make_signed<T>::type>
    {
      typedef typename IntegralIterator::RandomAccessIteratorFacade Facade;

      T value_;
    public:
      IntegralIterator() = default;
      IntegralIterator(T value) : value_(value) {}

      T dereference() const
      {
        return value_;
      }
      T elementAt(T n) const
      {
        return value_ + n;
      }
      template<class Iterator>
      bool equals(const Iterator &other) const
      {
        return value_ == *other;
      }
      void increment()
      {
        ++value_;
      }
      void decrement()
      {
        --value_;
      }
      void advance(typename Facade::DifferenceType n)
      {
        value_ += n;
      }
      template<class Iterator>
      typename Facade::DifferenceType distanceTo(const Iterator &other) const
      {
        // be careful here -- T may be signed or unsigned, but DifferenceType
        // is always signed.  We can't directly convert a distance computed in
        // unsigned arithmetic into a signed type, because the result may be
        // implementation-defined.
        auto ovalue = *other;
        if(ovalue >= value_)
          return ovalue - value_;
        else
          return -Facade::DifferenceType(value_- ovalue);
      }
    };

    template<class T>
    class IntegralIterator<
        T, typename std::enable_if<std::is_enum<T>::value>::type> :
      public IntegralIterator<typename std::underlying_type<T>::type>
    {
      typedef IntegralIterator<typename std::underlying_type<T>::type> Base;
    public:
      IntegralIterator() = default;
      IntegralIterator(T value) : Base(value) {}
    };

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_INTEGRAL_ITERATOR_HH
