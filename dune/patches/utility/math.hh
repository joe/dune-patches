#ifndef DUNE_PATCHES_UTILITY_MATH_HH
#define DUNE_PATCHES_UTILITY_MATH_HH

#include <stdexcept>
#include <string>
#include <type_traits>

namespace Dune {
  namespace Patches {

    //! Square
    template<class T>
    constexpr T sqr(T x)
    {
      return x*x;
    }

    //! integral power
    /**
     * \tparam B        Must allow multiplication and construction of the
     *                  multiplicative neutral value by \c B(1).
     * \tparam E        Must be an integral type.
     * \param  exponent Must be non-negative.
     *
     * \note Returns 1 for 0^0.
     * \note This is implemented as a recursion.  In C++14 it can be
     *       implemented with a loop and still be constexpr.  The recusion
     *       depth is \c O(ld(exponent)).
     *
     * \throws \c std::domain_error if \c exponent<0.
     */
    template<class B, class E>
    constexpr B ipow(B base, E exponent)
    {
      static_assert(std::is_integral<E>::value,
                    "The exponent must be an integral type for ipow()");
      using std::to_string;

      // TODO: for C++14 this can be implemented without recursion
      return
        exponent <  0 ?
          throw std::domain_error("ipow(): exponent must not be negative (got "
                                  + to_string(exponent) + ")") :
        exponent == 0 ?
          B(1) :
          sqr(ipow(base, exponent/2)) * ( exponent % 2 ? base : B(1) );
    }

    template<class T>
    constexpr T falling_factorial(T x, T m)
    {
      static_assert(std::is_integral<T>::value,
                    "The arguments must be of integral type for falling_factorial()");
      using std::to_string;

      return
        m < 0 ?
            throw std::domain_error("falling_factorial(x, m): m must not be "
                                    "negative (got x=" + to_string(x) + ", m="
                                    + to_string(m) + ")") :
        x < 0 ?
            throw std::domain_error("falling_factorial(x, m): x must not be "
                                    "negative (got x=" + to_string(x) + ", m="
                                    + to_string(m) + ")") :
        m > x ?
            throw std::domain_error("falling_factorial(x, m): m must not be "
                                    "larger than x (got x=" + to_string(x) +
                                    ", m=" + to_string(m) + ")") :
        m == 0 ? T(1) :
        m == 1 ? x :
        falling_factorial(x, m/2) * falling_factorial(x - m/2, m - m/2);
    }

    template<class T>
    constexpr T factorial(T n)
    {
      return falling_factorial(n, n);
    }

    template<class T>
    constexpr T binomial(T n, T k)
    {
      return
        k < (n+1)/2 ?
            falling_factorial(n, k) / factorial(k) :
            falling_factorial(n, n-k) / factorial(n-k);
    }

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_MATH_HH
