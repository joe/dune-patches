#ifndef DUNE_PATCHES_UTILITY_RANGE_HH
#define DUNE_PATCHES_UTILITY_RANGE_HH

#include <dune/patches/utility/integral_iterator.hh>

namespace Dune {
  namespace Patches {

    //! should be replaced by Dune::IteratorRange once that is available.
    template<typename Iterator>
    class IteratorRange
    {
    public:
      typedef Iterator iterator;

      IteratorRange(const Iterator& begin, const Iterator& end)
        : _begin(begin)
        , _end(end)
      {}

      iterator begin() const
      {
        return _begin;
      }

      iterator end() const
      {
        return _end;
      }

    private:

      Iterator _begin;
      Iterator _end;
    };

    template<class Iterator>
    IteratorRange<Iterator>
    makeIteratorRange(const Iterator &begin, const Iterator &end)
    {
      return { begin, end };
    }

    template<class T>
    IteratorRange<IntegralIterator<T> > range(T begin, T end)
    {
      return { begin, end };
    }

    template<class T>
    IteratorRange<IntegralIterator<T> > range(T end)
    {
      return range(T(0), T(end));
    }

  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_RANGE_HH
