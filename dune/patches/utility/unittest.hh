#ifndef DUNE_PATCHES_UTILITY_UNITTEST_HH
#define DUNE_PATCHES_UTILITY_UNITTEST_HH

namespace Dune {
  namespace Patches {
    namespace UnitTest {

      void acc(int &result, bool other)
      {
        if(other)
        {
          if(result == 77)
            result = 0;
        }
        else
          result = 1;
      }

      void acc(int &result, int other)
      {
        if(other != 77)
          acc(result, other == 0);
      }

    } // namespace UnitTest
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_UTILITY_UNITTEST_HH
