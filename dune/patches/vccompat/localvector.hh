#ifndef DUNE_PATCHES_VCCOMPAT_LOCALVECTOR_HH
#define DUNE_PATCHES_VCCOMPAT_LOCALVECTOR_HH

#include <Vc/Vc>

#include <dune/pdelab/gridfunctionspace/localvector.hh>

namespace Dune {
  namespace Patches {
    namespace VcCompat {

      template<class LocalVector>
      class LocalVectorLane
      {
        LocalVector &lv_;
        unsigned lane_;

      public:
        LocalVectorLane(LocalVector &lv, unsigned lane) :
          lv_(lv), lane_(lane)
        { }

        auto operator[](std::size_t i) const
          -> decltype(accessBaseContainer(lv_)[i][lane_])
        { return accessBaseContainer(lv_)[i][lane_]; }
        auto operator[](std::size_t i)
          -> decltype(accessBaseContainer(lv_)[i][lane_])
        { return accessBaseContainer(lv_)[i][lane_]; }
      };

      template<class T, long unsigned lanes, class LFSFlavorTag, class W>
      LocalVectorLane<PDELab::LocalVector<Vc::SimdArray<T, lanes>,
                                          LFSFlavorTag, W> >
      laneView(PDELab::LocalVector<Vc::SimdArray<T, lanes>,
                                   LFSFlavorTag, W> &lv,
               unsigned lane)
      {
        return { lv, lane };
      }

    } // namespace VcCompat
  } // namespace Patches
} // namespace Dune

#endif // DUNE_PATCHES_VCCOMPAT_LOCALVECTOR_HH
