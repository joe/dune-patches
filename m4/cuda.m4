# -*- Autoconf -*-
# Completely stolen from autoconf's c.m4 and adapted from C++ to Cuda
# This is the original notice:
# ======================================================================
# Copyright (C) 2001-2012 Free Software Foundation, Inc.

# This file is part of Autoconf.  This program is free
# software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# Under Section 7 of GPL version 3, you are granted additional
# permissions described in the Autoconf Configure Script Exception,
# version 3.0, as published by the Free Software Foundation.
#
# You should have received a copy of the GNU General Public License
# and a copy of the Autoconf Configure Script Exception along with
# this program; see the files COPYINGv3 and COPYING.EXCEPTION
# respectively.  If not, see <http://www.gnu.org/licenses/>.

# Written by David MacKenzie, with help from
# Akim Demaille, Paul Eggert,
# Franc,ois Pinard, Karl Berry, Richard Pixley, Ian Lance Taylor,
# Roland McGrath, Noah Friedman, david d zuhn, and many others.
# ======================================================================

# Cuda adaptations are copyright (C) 2015 Jö Fahlke

## ------------------- ##
## The Cuda language.  ##
## ------------------- ##


# AC_LANG(Cuda)
# ------------
# NVCCFLAGS is not in ac_cpp because -g, -O, etc. are not valid cpp options.
AC_LANG_DEFINE([Cuda], [cuda], [NVCC], [NVCC], [C++],
[ac_ext=cu
ac_cpp='$NVCPP $CPPFLAGS'
ac_compile='$NVCC -c $NVCCFLAGS $CPPFLAGS conftest.$ac_ext >&AS_MESSAGE_LOG_FD'
ac_link='$NVCC -o conftest$ac_exeext $NVCCFLAGS $CPPFLAGS $LDFLAGS conftest.$ac_ext $LIBS >&AS_MESSAGE_LOG_FD'
ac_compiler_gnu=$ac_cv_cuda_compiler_gnu
])


# AC_LANG_CUDA
# -----------------
AU_DEFUN([AC_LANG_CUDA], [AC_LANG(Cuda)])


# ------------------- #
# The Cuda compiler.  #
# ------------------- #


# AC_LANG_PREPROC(Cuda)
# --------------------
# Find the Cuda preprocessor.  Must be AC_DEFUN'd to be AC_REQUIRE'able.
AC_DEFUN([AC_LANG_PREPROC(Cuda)],
[AC_REQUIRE([AC_PROG_NVCPP])])


# AC_PROG_NVCPP
# --------------
# Find a working Cuda preprocessor.
# We shouldn't have to require AC_PROG_CC, but this is due to the concurrency
# between the AC_LANG_COMPILER_REQUIRE family and that of AC_PROG_NVCC.
AC_DEFUN([AC_PROG_NVCPP],
[AC_REQUIRE([AC_PROG_NVCC])dnl
AC_ARG_VAR([NVCPP],   [Cuda preprocessor])dnl
_AC_ARG_VAR_CPPFLAGS()dnl
AC_LANG_PUSH(Cuda)dnl
AC_MSG_CHECKING([how to run the Cuda preprocessor])
if test -z "$NVCPP"; then
  AC_CACHE_VAL(ac_cv_prog_NVCPP,
  [dnl
    # Double quotes because NVCPP needs to be expanded
    for NVCPP in "$NVCC -E"
    do
      _AC_PROG_PREPROC_WORKS_IFELSE([break])
    done
    ac_cv_prog_NVCPP=$NVCPP
  ])dnl
  NVCPP=$ac_cv_prog_NVCPP
else
  ac_cv_prog_NVCPP=$NVCPP
fi
AC_MSG_RESULT([$NVCPP])
_AC_PROG_PREPROC_WORKS_IFELSE([],
	  [AC_MSG_FAILURE([Cuda preprocessor "$NVCPP" fails sanity check])])
AC_SUBST(NVCPP)dnl
AC_LANG_POP(Cuda)dnl
])# AC_PROG_NVCPP


# AC_LANG_COMPILER(Cuda)
# ---------------------
# Find the Cuda compiler.  Must be AC_DEFUN'd to be AC_REQUIRE'able.
AC_DEFUN([AC_LANG_COMPILER(Cuda)],
[AC_REQUIRE([AC_PROG_NVCC])])


# AC_PROG_NVCC([LIST-OF-COMPILERS])
# --------------------------------
# LIST-OF-COMPILERS is a space separated list of Cuda compilers to search
# for (if not specified, a default list is used).  This just gives the
# user an opportunity to specify an alternative search list for the Cuda
# compiler.
# nvcc  NVidia Cuda compiler
AN_MAKEVAR([NVCC], [AC_PROG_NVCC])
AN_PROGRAM([nvcc], [AC_PROG_NVCC])
AC_DEFUN([AC_PROG_NVCC],
[AC_REQUIRE([AC_PROG_CC])dnl needed to OBJEXT etc is set.
AC_LANG_PUSH(Cuda)dnl
AC_ARG_VAR([NVCC],      [Cuda compiler command])dnl
AC_ARG_VAR([NVCCFLAGS], [Cuda compiler flags])dnl
_AC_ARG_VAR_LDFLAGS()dnl
_AC_ARG_VAR_LIBS()dnl
_AC_ARG_VAR_CPPFLAGS()dnl
if test -z "$NVCC"; then
  AC_CHECK_TOOLS(NVCC,
      	   [m4_default([$1],
      		       [nvcc])],
      	   no)
fi
if test no = "$NVCC"; then
  NVCC_FOUND=0
  NVCC=nvcc-NOTFOUND
else
  NVCC_FOUND=1
fi
# Provide some information about the compiler.
_AS_ECHO_LOG([checking for _AC_LANG compiler version])
set X $ac_compile
ac_compiler=$[2]
for ac_option in --version -v -V -qversion; do
  _AC_DO_LIMIT([$ac_compiler $ac_option >&AS_MESSAGE_LOG_FD])
done

# Assume NVCC_FOUND=1 means nvcc is working
if test 1 = "$NVCC_FOUND"; then
  AC_DEFINE([HAVE_NVCC], [1], [Whether nvcc was found])
fi
AM_CONDITIONAL([NVCC], [test 1 = "$NVCC_FOUND"])

dnl Don't call these: they should be called with a C compiler (i.e. from
dnl AC_PROG_CC).  If we would call them here that would make nvcc mandatory,
dnl but we want to have the option to continue even if it is missing
dnl
dnl m4_expand_once([_AC_COMPILER_EXEEXT])[]dnl
dnl m4_expand_once([_AC_COMPILER_OBJEXT])[]dnl
_AC_PROG_NVCC_G
AC_LANG_POP(Cuda)dnl
m4_ifdef([AM_INIT_AUTOMAKE],
  [dnl
    AC_PROVIDE_IFELSE([AM_INIT_AUTOMAKE],
      [_AM_IF_OPTION([no-dependencies],,[_AM_DEPENDENCIES([NVCC])])])
    m4_define([AM_INIT_AUTOMAKE], [
      m4_fatal([AM_INIT_AUTOMAKE and AC_PROG_NVCC must be called in that order if they are both used])
    ])
  ])dnl
])# AC_PROG_NVCC
# append libtool handling.  This avoids a circular require
m4_define([AC_PROG_NVCC], m4_defn([AC_PROG_NVCC])[
  m4_ifdef([LT_INIT], [dnl
    AC_PROVIDE_IFELSE([LT_INIT], [dnl
      AM_CONDITIONAL([LIBTOOL], [true])
      LT_LANG([Cuda])
    ], [dnl
      m4_define([LT_INIT], [dnl
        m4_fatal([LT_INIT and AC_PROG_NVCC must be called in that order if they are both used])
      ])
    ])
  ], [dnl
    AM_CONDITIONAL([LIBTOOL], [false])
  ])
])

# _AC_PROG_NVCC_G
# ---------------
# Check whether -g works, even if NVCCFLAGS is set, in case the package
# plays around with NVCCFLAGS (such as to build both debugging and
# normal versions of a library), tasteless as that idea is.
# Don't consider -g to work if it generates warnings when plain compiles don't.
m4_define([_AC_PROG_NVCC_G],
[ac_test_NVCCFLAGS=${NVCCFLAGS+set}
ac_save_NVCCFLAGS=$NVCCFLAGS
AC_CACHE_CHECK(whether $NVCC accepts -g, ac_cv_prog_nvcc_g,
  [ac_save_nvcc_werror_flag=$ac_nvcc_werror_flag
   ac_nvcc_werror_flag=yes
   ac_cv_prog_nvcc_g=no
   NVCCFLAGS="-g"
   _AC_COMPILE_IFELSE([AC_LANG_PROGRAM()],
     [ac_cv_prog_nvcc_g=yes],
     [NVCCFLAGS=""
      _AC_COMPILE_IFELSE([AC_LANG_PROGRAM()],
	[],
	[ac_nvcc_werror_flag=$ac_save_nvcc_werror_flag
	 NVCCFLAGS="-g"
	 _AC_COMPILE_IFELSE([AC_LANG_PROGRAM()],
	   [ac_cv_prog_nvcc_g=yes])])])
   ac_nvcc_werror_flag=$ac_save_nvcc_werror_flag])
if test "$ac_test_NVCCFLAGS" = set; then
  NVCCFLAGS=$ac_save_NVCCFLAGS
elif test $ac_cv_prog_nvcc_g = yes; then
  NVCCFLAGS="-g -O2"
else
  NVCCFLAGS="-O2"
fi[]dnl
])# _AC_PROG_NVCC_G


# AC_PROG_NVCC_C_O
# ---------------
# Test if the Cuda compiler accepts the options `-c' and `-o'
# simultaneously, and define `NVCC_NO_MINUS_C_MINUS_O' if it does not.
AC_DEFUN([AC_PROG_NVCC_C_O],
[AC_REQUIRE([AC_PROG_NVCC])dnl
AC_LANG_PUSH([Cuda])dnl
AC_CACHE_CHECK([whether $NVCC understands -c and -o together],
	       [ac_cv_prog_nvcc_c_o],
[AC_LANG_CONFTEST([AC_LANG_PROGRAM([])])
# We test twice because some compilers refuse to overwrite an existing
# `.o' file with `-o', although they will create one.
ac_try='$NVCC $NVCCFLAGS -c conftest.$ac_ext -o conftest2.$ac_objext >&AS_MESSAGE_LOG_FD'
rm -f conftest2.*
if _AC_DO_VAR(ac_try) &&
     test -f conftest2.$ac_objext &&
     _AC_DO_VAR(ac_try); then
  ac_cv_prog_nvcc_c_o=yes
else
  ac_cv_prog_nvcc_c_o=no
fi
rm -f conftest*])
if test $ac_cv_prog_nvcc_c_o = no; then
  AC_DEFINE(NVCC_NO_MINUS_C_MINUS_O, 1,
	    [Define to 1 if your Cuda compiler doesn't accept
	     -c and -o together.])
fi
AC_LANG_POP([Cuda])dnl
])# AC_PROG_NVCC_C_O

m4_ifdef([AM_CONDITIONAL], [], [
  AC_DEFUN([AM_CONDITIONAL], [
    if $2; then
      AC_SUBST([$1_TRUE], [])
      AC_SUBST([$1_FALSE], [#])
    else
      AC_SUBST([$1_TRUE], [#])
      AC_SUBST([$1_FALSE], [])
    fi
  ])
])

AC_DEFUN([JF_LIBCUDART], [
  AC_LANG_PUSH([C])
  AC_CHECK_LIB([cudart], [cudaGetDeviceCount],
    [AC_SUBST([CUDART_LIBS], [-lcudart])
     AC_DEFINE([HAVE_LIBCUDART], [1], [Define if -lcudart works])],
    [AC_SUBST([CUDART_LIBS], [""])])
  AC_LANG_POP([C])
])
AC_DEFUN([JF_CUDART], [
  AC_REQUIRE([JF_LIBCUDART])
  AC_CACHE_CHECK([for cudart], [jf_cv_have_cudart], [
    AC_LANG_PUSH([C])
    jf_save_LIBS=$LIBS
    LIBS="$CUDART_LIBS $LIBS"
    AC_LINK_IFELSE([AC_LANG_PROGRAM(
      [#include <cuda_runtime.h>],
      [int main()
{
  int deviceCount;
  cudaGetDeviceCount(&deviceCount);
  return 0;
}])],
      [jf_cv_have_cudart=yes],
      [jf_cv_have_cudart=no])
    LIBS=$jf_save_LIBS
  ])
  if test "x$jf_cv_have_cudart" = xyes; then
    AC_DEFINE([HAVE_CUDART], [1], [Define if the Cuda runtime is available])
  fi
  AM_CONDITIONAL([CUDART], [test "x$jf_cv_have_cudart" = xyes])
  AC_LANG_POP([C])
])

# -*- Autoconf -*-
# Completely stolen from libtool's libtool.m4 and adapted from Go to Cuda
# This is the original notice:
# ======================================================================
# libtool.m4 - Configure libtool for the host system. -*-Autoconf-*-
#
#   Copyright (C) 1996, 1997, 1998, 1999, 2000, 2001, 2003, 2004, 2005,
#                 2006, 2007, 2008, 2009, 2010, 2011 Free Software
#                 Foundation, Inc.
#   Written by Gordon Matzigkeit, 1996
#
# This file is free software; the Free Software Foundation gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
# ======================================================================

# Cuda adaptations are copyright (C) 2015 Jö Fahlke

# _LT_LANG_Cuda_CONFIG([TAG])
# --------------------------
# Ensure that the configuration variables for the GNU Go compiler
# are suitably defined.  These variables are subsequently used by _LT_CONFIG
# to write the compiler configuration to `libtool'.
m4_defun([_LT_LANG_Cuda_CONFIG],
[AC_LANG_PUSH([Cuda])

# Object file extension for compiled Go test sources.
_LT_TAGVAR(objext, $1)=$objext

# Code to be used in simple compile tests
lt_simple_compile_test_code="int main() { }"

# Code to be used in simple link tests
lt_simple_link_test_code='int main() { }'

# ltmain only uses $CC for tagged configurations so make sure $CC is set.
_LT_TAG_COMPILER

# save warnings/boilerplate of simple test code
_LT_COMPILER_BOILERPLATE
_LT_LINKER_BOILERPLATE

# Allow CC to be a program name with arguments.
lt_save_CC=$CC
lt_save_CFLAGS=$CFLAGS
lt_save_GCC=$GCC
GCC=yes
CC=${NVCC-"nvcc"}
CFLAGS=$NVCCFLAGS
compiler=$CC
_LT_TAGVAR(compiler, $1)=$CC
_LT_TAGVAR(LD, $1)="$LD"
_LT_CC_BASENAME([$compiler])

# Go did not exist at the time GCC didn't implicitly link libc in.
_LT_TAGVAR(archive_cmds_need_lc, $1)=no

_LT_TAGVAR(old_archive_cmds, $1)=$old_archive_cmds
_LT_TAGVAR(reload_flag, $1)=$reload_flag
_LT_TAGVAR(reload_cmds, $1)=$reload_cmds

## CAVEAT EMPTOR:
## There is no encapsulation within the following macros, do not change
## the running order or otherwise move them around unless you know exactly
## what you are doing...
if test -n "$compiler"; then
  _LT_COMPILER_NO_RTTI($1)
  _LT_COMPILER_PIC($1)
  _LT_COMPILER_C_O($1)
  _LT_COMPILER_FILE_LOCKS($1)
  _LT_LINKER_SHLIBS($1)
  _LT_LINKER_HARDCODE_LIBPATH($1)

  _LT_CONFIG($1)
fi

AC_LANG_POP([Cuda])

GCC=$lt_save_GCC
CC=$lt_save_CC
CFLAGS=$lt_save_CFLAGS
])# _LT_LANG_Cuda_CONFIG
