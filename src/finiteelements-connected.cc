// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <config.h>

#include <iostream>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/referenceelements.hh>

#if HAVE_DUNE_ISTL
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/ilu.hh>
#include <dune/istl/io.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>
#else
#include <dune/common/dynvector.hh>
#include <dune/common/dynmatrix.hh>
#endif // HAVE_DUNE_ISTL

#include <dune/patches/cube/connected/patch.hh>
#include <dune/patches/meshes/tensorproduct.hh>
#include <dune/patches/utility/range.hh>
#if HAVE_DUNE_GRID
#include <dune/patches/vtkwriter.hh>
#endif // HAVE_DUNE_GRID

#include "matrix.hh"
#include "p1q1localoperator.hh"
#include "pattern.hh"
#include "shapefunctions.hh"
#include "vector.hh"

using Dune::Patches::range;

// P1/Q1Elements:
// a P1/Q1 finite element discretization for elliptic problems Dirichlet
// boundary conditions on simplicial conforming grids
template<class GV, class LOP>
class P1Q1Elements
{
public:
  static const int dim = GV::dimension;

  typedef typename GV::ctype ctype;
#if HAVE_DUNE_ISTL
  typedef Dune::BCRSMatrix<Dune::FieldMatrix<ctype,1,1> > Matrix;
  typedef Dune::BlockVector<Dune::FieldVector<ctype,1> > ScalarField;
#else
  typedef Dune::DynamicMatrix<ctype> Matrix;
  typedef Dune::DynamicVector<ctype> ScalarField;
#endif // HAVE_DUNE_ISTL

private:
  typedef typename GV::template Codim<0>::Iterator LeafIterator;
  typedef typename GV::template Codim<0>::Geometry::JacobianInverseTransposed JacobianInverseTransposed;

  GV gv;
  const LOP &lop;

public:
  Matrix A;
  ScalarField b;
  ScalarField u;
  VertexPattern<GV> adjacencyPattern;

  P1Q1Elements(const GV& gv_, const LOP &lop_) :
    gv(gv_), lop(lop_), adjacencyPattern(gv)
  {}

  // store adjacency information in a vector of sets
  void determineAdjacencyPattern();

  // assemble stiffness matrix A and right side b
  void assemble()
  {
    if(gv.template begin<0>()->type().isSimplex())
      // get a set of P1 shape functions
      assemble(P1ShapeFunctionSet<ctype,ctype,dim>::instance());
    else
      // get a set of Q1 shape functions
      assemble(Q1ShapeFunctionSet<ctype,ctype,dim>::instance());
  }

  // finally solve Au = b for u
  void solve();

private:
  template<class ShapeFunctionSet>
  void assemble(const ShapeFunctionSet &basis);
};

template<class GV, class LOP> /*@\label{fem:adjpat1}@*/
void P1Q1Elements<GV, LOP>::determineAdjacencyPattern()
{
  for(auto it = gv.template begin<0>(); it != gv.template end<0>(); ++it)
  {
    auto &&e = *it;
    lop.pattern_volume(e, makeLocalPattern(adjacencyPattern, e, e));
  }
} /*@\label{fem:adjpat2}@*/

template<class GV, class LOP>
template<class ShapeFunctionSet>
void P1Q1Elements<GV, LOP>::assemble(const ShapeFunctionSet &basis)
{
  const auto & indexSet = gv.indexSet();
  const int N = gv.size(dim);

  const LeafIterator itend = gv.template end<0>();

  // set sizes of A and b
#if HAVE_DUNE_ISTL
  setPattern(A, adjacencyPattern);
  b.resize(N, false);
#else
  A.resize(N, N);
  b.resize(N);
#endif // HAVE_DUNE_ISTL

  // initialize A and b
  A = 0.0;
  b = 0.0;

  for (LeafIterator it = gv.template begin<0>(); it != itend; ++it)   /*@\label{fem:loop1}@*/
  {
    const auto &e = *it;
    lop.jacobian_volume(e, basis, makeLocalMatrix(A, indexSet, e, e));
    lop.lambda_volume(e, basis, makeLocalVector(b, indexSet, e));
  }   /*@\label{fem:loop2}@*/

  // Dirichlet boundary conditions:
  // replace lines in A related to Dirichlet vertices by trivial lines
  for(auto is = gv.bBegin(); is != gv.bEnd(); ++ is)
  {
    // determine geometry type of the current element and get the matching reference element
    Dune::GeometryType gt = is->inside().type();
    const auto &ref = Dune::ReferenceElements<ctype,dim>::general(gt);

    // traverse all vertices the intersection consists of
    for (auto i : range(ref.size(is->indexInInside(),1,dim)))
    {
      // and replace the associated line of A and b with a trivial one
      auto indexi = indexSet.subIndex
        (is->inside(), ref.subEntity(is->indexInInside(),1,i,dim), dim);

      A[indexi] = 0.0;           /*@\label{fem:trivialline}@*/
      A[indexi][indexi] = 1.0;
      b[indexi] = 0.0;
    }
  }   /*@\label{fem:boundary2}@*/
}

#if HAVE_DUNE_ISTL
template<class GV, class LOP>
void P1Q1Elements<GV, LOP>::solve()
{
  // make linear operator from A
  Dune::MatrixAdapter<Matrix,ScalarField,ScalarField> op(A);

  // initialize preconditioner
  Dune::SeqILUn<Matrix,ScalarField,ScalarField> ilu1(A, 1, 0.92);

  // the inverse operator
  Dune::BiCGSTABSolver<ScalarField> bcgs(op, ilu1, 1e-15, 5000, 2);
  Dune::InverseOperatorResult r;

  // initialize u to some arbitrary value to avoid u being the exact
  // solution
  u.resize(b.N(), false);
  u = 2.0;

  // finally solve the system
  bcgs.apply(u, b, r);
}
#endif // HAVE_DUNE_ISTL

// an example right hand side function
template<class ctype, int dim>
class Bump {
public:
  ctype operator() (Dune::FieldVector<ctype,dim> x) const
  {
    ctype result = 0;
    for (int i=0 ; i < dim ; i++)
      result += 2.0 * x[i]* (1-x[i]);
    return result;
  }
};

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  static const int dim = 2;             /*@\label{fem:dim}@*/
  typedef double ctype;
  typedef Dune::Patches::Cube::Connected::Patch<ctype, dim> Patch;

  auto patchp = Dune::Patches::Meshes::tensorProduct<Patch>
    ( Dune::FieldVector<ctype,dim>(0), Dune::FieldVector<ctype,dim>(1),
      Dune::FieldVector<Patch::Index, dim>(10));
  // typedef Patch::MacroGridView GV;
  // const GV &view = patch.macroGridView();
  typedef Patch::LevelGridView GV;
  const GV &view = patchp->levelGridView(0);

  typedef Bump<ctype,dim> Func;
  typedef P1Q1LocalOperator<Func> LOP;
  LOP lop { Func() };

  P1Q1Elements<GV,LOP> p1q1(view, lop);

  std::cout << "-----------------------------------" << "\n";
  std::cout << "number of unknowns: " << view.size(dim) << "\n";

  std::cout << "determine adjacency pattern..." << "\n";
  p1q1.determineAdjacencyPattern();

  std::cout << "assembling..." << "\n";
  p1q1.assemble();

#if HAVE_DUNE_ISTL
  std::cout << "solving..." << "\n";
  p1q1.solve();

#if HAVE_DUNE_GRID
  std::cout << "visualizing..." << "\n";
  Dune::Patches::VTKWriter<GV> vtkwriter(view);
  vtkwriter.addVertexData(p1q1.u, "u");
  vtkwriter.write("finiteelements-connected", Dune::VTK::ascii);
#else
  std::cout << "for visualizing dune-grid is necessary." << "\n";
#endif //HAVE_DUNE_GRID
#else
  std::cout << "for solving and visualizing dune-istl is necessary." << "\n";
#endif // HAVE_DUNE_ISTL
}
