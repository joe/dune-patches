// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <config.h>

#include <memory>
#include <iostream>
#include <vector>

#include <dune/common/array.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/referenceelements.hh>

#if !HAVE_DUNE_GRID
#error We need dune-grid
#endif
#include <dune/grid/common/rangegenerators.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/yaspgrid.hh>

#if !HAVE_DUNE_ISTL
#error we need dune-istl
#endif
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/ilu.hh>
#include <dune/istl/io.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/patches/cube/unconnected/patch.hh>
#include <dune/patches/utility/gridviewpatchfactory.hh>
#include <dune/patches/utility/range.hh>

#include "matrix.hh"
#include "p1q1localoperator.hh"
#include "pattern.hh"
#include "shapefunctions.hh"
#include "vector.hh"

using Dune::Patches::range;

// P1/Q1Elements:
// a P1/Q1 finite element discretization for elliptic problems Dirichlet
// boundary conditions on simplicial conforming grids
template<class GV, class LOP>
class P1Q1Elements
{
public:
  static const int dim = GV::dimension;
  typedef typename GV::ctype ctype;
  using Patch = Dune::Patches::Cube::Unconnected::Patch<ctype, dim>;
  using PV = typename Patch::MacroGridView;

  typedef Dune::BCRSMatrix<Dune::FieldMatrix<ctype,1,1> > Matrix;
  typedef Dune::BlockVector<Dune::FieldVector<ctype,1> > ScalarField;

private:
  typedef typename GV::template Codim<0>::Iterator LeafIterator;
  typedef typename GV::template Codim<0>::Geometry::JacobianInverseTransposed JacobianInverseTransposed;

  GV gv;
  std::vector<unsigned> vertexMap;
  std::unique_ptr<const Patch> patchp;
  PV pv;
  const LOP &lop;

public:
  Matrix A;
  ScalarField b;
  ScalarField u;
  VertexPattern<GV> adjacencyPattern;

  P1Q1Elements(const GV& gv_, const LOP &lop_) :
    gv(gv_), vertexMap(),
    patchp(Dune::Patches::GridViewPatchFactory<GV>(gv)
           .template createAndMapVertices<Patch>(elements(gv), vertexMap)),
    pv(patchp->macroGridView()), lop(lop_), adjacencyPattern(gv)
  {}

  // store adjacency information in a vector of sets
  void determineAdjacencyPattern();

  // assemble stiffness matrix A and right side b
  void assemble()
  {
    if(gv.template begin<0>()->type().isSimplex())
      // get a set of P1 shape functions
      assemble(P1ShapeFunctionSet<ctype,ctype,dim>::instance());
    else
      // get a set of Q1 shape functions
      assemble(Q1ShapeFunctionSet<ctype,ctype,dim>::instance());
  }

  // finally solve Au = b for u
  void solve();

private:
  template<class ShapeFunctionSet>
  void assemble(const ShapeFunctionSet &basis);
};

template<class GV, class LOP> /*@\label{fem:adjpat1}@*/
void P1Q1Elements<GV, LOP>::determineAdjacencyPattern()
{
  for(auto it = gv.template begin<0>(); it != gv.template end<0>(); ++it)
  {
    auto &&e = *it;
    lop.pattern_volume(e, makeLocalPattern(adjacencyPattern, e, e));
  }
} /*@\label{fem:adjpat2}@*/

template<class GV, class LOP>
template<class ShapeFunctionSet>
void P1Q1Elements<GV, LOP>::assemble(const ShapeFunctionSet &basis)
{
  const int N = gv.size(dim);

  // set sizes of A and b
  setPattern(A, adjacencyPattern);
  b.resize(N, false);

  // initialize A and b
  A = 0.0;
  b = 0.0;

  auto pis = pv.indexSet();
  for(const auto pe : elements(pv))
  {
    Dune::FieldMatrix<ctype, (1 << dim), (1 << dim)> localA(0.0);
    Dune::FieldVector<ctype, (1 << dim)> localb(0.0);

    lop.jacobian_volume(pe, basis,
                        [&localA] (unsigned i, unsigned j) -> ctype &
                        { return localA[i][j]; });
    lop.lambda_volume(pe, basis, localb);

    for(auto i : range(1 << dim))
    {
      auto gi = vertexMap[pis.subIndex(pe, i, dim)];
      b[gi] += localb[i];
      auto &row = A[gi];
      for(auto j : range(1 << dim))
        row[vertexMap[pis.subIndex(pe, j, dim)]] += localA[i][j];
    }
  }

  const auto & indexSet = gv.indexSet();

  // Dirichlet boundary conditions:
  // replace lines in A related to Dirichlet vertices by trivial lines
  for(const auto &e : elements(gv))
    for(auto is = gv.ibegin(e); is != gv.iend(e); ++is)
    {
      if(!is->boundary())
        continue;

      // determine geometry type of the current element and get the matching
      // reference element
      Dune::GeometryType gt = e.type();
      const auto &ref = Dune::ReferenceElements<ctype,dim>::general(gt);

      // traverse all vertices the intersection consists of
      for (auto i : range(ref.size(is->indexInInside(),1,dim)))
      {
        // and replace the associated line of A and b with a trivial one
        auto indexi = indexSet.subIndex
          (e, ref.subEntity(is->indexInInside(),1,i,dim), dim);

        A[indexi] = 0.0;           /*@\label{fem:trivialline}@*/
        A[indexi][indexi] = 1.0;
        b[indexi] = 0.0;
      }
    }   /*@\label{fem:boundary2}@*/
}

template<class GV, class LOP>
void P1Q1Elements<GV, LOP>::solve()
{
  // make linear operator from A
  Dune::MatrixAdapter<Matrix,ScalarField,ScalarField> op(A);

  // initialize preconditioner
  Dune::SeqILUn<Matrix,ScalarField,ScalarField> ilu1(A, 1, 0.92);

  // the inverse operator
  Dune::BiCGSTABSolver<ScalarField> bcgs(op, ilu1, 1e-15, 5000, 2);
  Dune::InverseOperatorResult r;

  // initialize u to some arbitrary value to avoid u being the exact
  // solution
  u.resize(b.N(), false);
  u = 2.0;

  // finally solve the system
  bcgs.apply(u, b, r);
}

// an example right hand side function
template<class ctype, int dim>
class Bump {
public:
  ctype operator() (Dune::FieldVector<ctype,dim> x) const
  {
    ctype result = 0;
    for (int i=0 ; i < dim ; i++)
      result += 2.0 * x[i]* (1-x[i]);
    return result;
  }
};

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  static constexpr int dim = 2;             /*@\label{fem:dim}@*/

  using Grid = Dune::YaspGrid<dim>;
  Grid grid(Dune::FieldVector<Grid::ctype, dim>(1),
            Dune::fill_array<int, dim>(10));

  typedef Grid::LevelGridView GV;
  const GV &gv = grid.levelGridView(0);

  typedef Bump<Grid::ctype,dim> Func;
  typedef P1Q1LocalOperator<Func> LOP;
  LOP lop { Func() };

  P1Q1Elements<GV,LOP> p1q1(gv, lop);

  std::cout << "-----------------------------------" << "\n";
  std::cout << "number of unknowns: " << gv.size(dim) << "\n";

  std::cout << "determine adjacency pattern..." << "\n";
  p1q1.determineAdjacencyPattern();

  std::cout << "assembling..." << "\n";
  p1q1.assemble();

  std::cout << "solving..." << "\n";
  p1q1.solve();

  std::cout << "visualizing..." << "\n";
  Dune::VTKWriter<GV> vtkwriter(gv);
  vtkwriter.addVertexData(p1q1.u, "u");
  vtkwriter.write("finiteelements-unconnected", Dune::VTK::ascii);
}
