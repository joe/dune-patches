#ifndef DUNE_PATCHES_SRC_MATRIX_HH
#define DUNE_PATCHES_SRC_MATRIX_HH

#include <cstddef>

// local pattern, directly inserts into global pattern
template<class Matrix, class IndexSet, class Element>
class LocalMatrix {
  enum { dim_ = Element::dimension };

  Matrix &matrix_;
  const IndexSet &indexSet_;
  const Element &ei_;
  const Element &ej_;

public:
  LocalMatrix(Matrix &matrix, const IndexSet &indexSet,
              const Element &ei, const Element &ej) :
    matrix_(matrix), indexSet_(indexSet),
    ei_(ei), ej_(ej)
  { }

  auto operator()(std::size_t li, std::size_t lj) const
    -> decltype(matrix_[li][lj])&
  {
    return matrix_[indexSet_.subIndex(ei_, li, dim_)]
                  [indexSet_.subIndex(ej_, lj, dim_)];
  }
};

template<class Matrix, class IndexSet, class Element>
LocalMatrix<Matrix, IndexSet, Element>
makeLocalMatrix(Matrix &matrix, const IndexSet &indexSet,
                const Element &ei, const Element &ej)
{
  return { matrix, indexSet, ei, ej };
}

#endif // DUNE_PATCHES_SRC_MATRIX_HH
