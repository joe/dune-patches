#ifndef DUNE_PATCHES_SRC_P1Q1LOCALOPERATOR_HH
#define DUNE_PATCHES_SRC_P1Q1LOCALOPERATOR_HH

#include <utility>

#include <dune/common/fvector.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/referenceelements.hh>

#include <dune/patches/utility/range.hh>

//! LocalOperator using P1/Q1 continuous finite elements.
template<class F>
class P1Q1LocalOperator {
  F f_;

public:
  template<class F_>
  P1Q1LocalOperator(F_ &&f) : f_(std::forward<F_>(f)) { }

  // compute adjacency pattern on each element
  template<class Element, class LocalPattern>
  void pattern_volume(const Element &e, LocalPattern &&p) const
  {
    using Dune::Patches::range;
    typedef typename Element::Geometry::ctype ctype;
    constexpr auto dim = Element::dimension;

    const auto &ref = Dune::ReferenceElements<ctype,dim>::general(e.type());
    for(auto i : range(ref.size(dim)))
      for(auto j : range(ref.size(dim)))
        p.addLink(i, j);
  }

  template<class Element, class LocalBasis, class LocalMatrix>
  void
  jacobian_volume(const Element &e, LocalBasis &&lb, LocalMatrix &&m) const
  {
    using Dune::Patches::range;
    typedef typename Element::Geometry::ctype ctype;
    constexpr auto dim = Element::dimension;

    // determine geometry type of the current element and get the matching
    // reference element
    auto gt = e.type();
    const auto &geo = e.geometry();
    const auto &ref = Dune::ReferenceElements<ctype,dim>::general(gt);
    int vertexsize = ref.size(dim);

    // get a quadrature rule of order one for the given geometry type
    for (const auto &r : Dune::QuadratureRules<ctype,dim>::rule(gt,1))
    {
      // compute the jacobian inverse transposed to transform the gradients
      const auto &jacInvTra = geo.jacobianInverseTransposed(r.position());

      // get the weight at the current quadrature point
      ctype weight = r.weight();

      // compute Jacobian determinant for the transformation formula
      ctype detjac = geo.integrationElement(r.position());
      for (auto i : range(vertexsize))
      {
        // compute transformed gradients
        Dune::FieldVector<ctype,dim> grad1;
        jacInvTra.mv(lb[i].evaluateGradient(r.position()),grad1);
        for (auto j : range(vertexsize))
        {
          Dune::FieldVector<ctype,dim> grad2;
          jacInvTra.mv(lb[j].evaluateGradient(r.position()),grad2);

          // update matrix entry
          m(i,j) += (grad1*grad2) * weight * detjac;
        }
      }
    }

  }

  template<class Element, class LocalBasis, class LocalVector>
  void lambda_volume(const Element &e, LocalBasis &&lb, LocalVector &&v) const
  {
    using Dune::Patches::range;
    typedef typename Element::Geometry::ctype ctype;
    constexpr auto dim = Element::dimension;

    // determine geometry type of the current element and get the matching
    // reference element
    auto gt = e.type();
    const auto &geo = e.geometry();
    const auto &ref = Dune::ReferenceElements<ctype,dim>::general(gt);
    int vertexsize = ref.size(dim);

    // get a quadrature rule of order two for the given geometry type
    for (const auto &r : Dune::QuadratureRules<ctype,dim>::rule(gt,2))
    {
      ctype weight = r.weight();
      ctype detjac = geo.integrationElement(r.position());
      for (auto i : range(vertexsize))
      {
        // evaluate the integrand of the right side
        ctype fval = lb[i].evaluateFunction(r.position())
                     * f_(geo.global(r.position())) ;
        v[i] += fval * weight * detjac;         /*@\label{fem:calcb}@*/
      }
    }
  }

};

#endif // DUNE_PATCHES_SRC_P1Q1LOCALOPERATOR_HH
