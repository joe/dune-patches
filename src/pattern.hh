#ifndef DUNE_PATCHES_SRC_PATTERN_HH
#define DUNE_PATCHES_SRC_PATTERN_HH

#include <cstddef>
#include <set>
#include <type_traits>
#include <vector>

#include <dune/common/fmatrix.hh>

#include <dune/grid/common/indexidset.hh>

#if HAVE_DUNE_ISTL
#include <dune/istl/bcrsmatrix.hh>
#endif //HAVE_DUNE_ISTL

#include <dune/patches/cube/indexset.hh>
#include <dune/patches/utility/range.hh>

// local pattern, directly inserts into global pattern
template<class Pattern, class Element>
class LocalPattern {
  Pattern &pattern_;
  const Element &ei_;
  const Element &ej_;

public:
  LocalPattern(Pattern &pattern, const Element &ei, const Element &ej) :
    pattern_(pattern),
    ei_(ei), ej_(ej)
  { }

  void addLink(std::size_t i, std::size_t j) const
  {
    pattern_.addLink(ei_, i, ej_, j);
  }
};

template<class Pattern, class Element>
LocalPattern<Pattern, Element>
makeLocalPattern(Pattern &pattern, const Element &ei, const Element &ej)
{
  return { pattern, ei, ej };
}

template<class T, class = void>
struct CanCopy {};

template<class Patch>
struct CanCopy<Dune::Patches::Cube::IndexSet<Patch> > : std::true_type {};

template<class GridImp, class IndexSetImp, class IndexTypeImp, class TypesImp>
struct CanCopy<Dune::IndexSet<GridImp, IndexSetImp, IndexTypeImp, TypesImp> > :
  std::false_type {};

//! Pattern for vertex-based spaces
template<class GV>
class VertexPattern :
  public std::vector<std::set<std::size_t> >
{
  enum { dim_ = GV::dimension };
  typedef typename GV::IndexSet IndexSet;
  typedef typename GV::template Codim<0>::Entity Element;

  // Hack to make sure both by-value and by-reference index sets work.
  typename std::conditional<CanCopy<IndexSet>::value,
                            IndexSet, const IndexSet &>::type indexSet_;

public:
  VertexPattern() = default;

  VertexPattern(const GV &gv) : indexSet_(gv.indexSet())
  {
    resize(gv.size(dim_));
  }

  void addLink(std::size_t i, std::size_t j)
  {
    (*this)[i].insert(j);
  }

  void addLink(const Element &ei, std::size_t li,
               const Element &ej, std::size_t lj)
  {
    addLink(indexSet_.subIndex(ei, li, dim_),
            indexSet_.subIndex(ej, lj, dim_));
  }
};

#if HAVE_DUNE_ISTL
template<class T, class GV>
void setPattern(Dune::BCRSMatrix<Dune::FieldMatrix<T, 1, 1> > &matrix,
                const VertexPattern<GV> &pattern)
{
  using Dune::Patches::range;

  const unsigned N = pattern.size();

  std::size_t nnz = 0;
  for(auto&& r : pattern)
    nnz += r.size();

  matrix.setSize(N, N, nnz);
  matrix.setBuildMode(matrix.random);

  for(auto i : range(N))
    matrix.setrowsize(i,pattern[i].size());
  matrix.endrowsizes();

  for(auto i : range(N))
    for (auto j : pattern[i])
      matrix.addindex(i,j);

  matrix.endindices();
}
#endif //HAVE_DUNE_ISTL

#endif // DUNE_PATCHES_SRC_PATTERN_HH
