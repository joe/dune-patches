#ifndef DUNE_PATCHES_SRC_VECTOR_HH
#define DUNE_PATCHES_SRC_VECTOR_HH

#include <cstddef>

// local pattern, directly inserts into global pattern
template<class Vector, class IndexSet, class Element>
class LocalVector {
  enum { dim_ = Element::dimension };

  Vector &vector_;
  const IndexSet &indexSet_;
  const Element &e_;

public:
  LocalVector(Vector &vector, const IndexSet &indexSet, const Element &e) :
    vector_(vector), indexSet_(indexSet), e_(e)
  { }

  auto operator[](std::size_t li) const -> decltype(vector_[li])&
  {
    return vector_[indexSet_.subIndex(e_, li, dim_)];
  }
};

template<class Vector, class IndexSet, class Element>
LocalVector<Vector, IndexSet, Element>
makeLocalVector(Vector &vector, const IndexSet &indexSet, const Element &e)
{
  return { vector, indexSet, e };
}

#endif // DUNE_PATCHES_SRC_VECTOR_HH
